// @ts-check
import "../../engine/WebGPU/compatibility-layer.js";
import Renderer from "../../engine/WebGPU/Render3D/Renderer.js";
import SceneGraph from "../../engine/Data/Geometric/SceneGraph.js";

import Camera from "../../engine/Data/Geometric/Camera.js";

import RenderContext from "../../engine/WebGPU/RenderContext.js";
import SceneGlobals from "../../engine/WebGPU/Render3D/SceneGlobals.js";

const canvas = document.getElementById("main-canvas");
if (!(canvas instanceof HTMLCanvasElement)) throw "Failed to get canvas";

const renderContext = new RenderContext(canvas);
const renderer = new Renderer();
const sceneGlobals = SceneGlobals.create();
const camera = new Camera();

/**
 * @param {DOMHighResTimeStamp} timestamp 
*/
function startRendering (timestamp) {
  let previousTimestamp = timestamp;
  /**
   * 
   * @param {DOMHighResTimeStamp} timestamp 
   */
  const update = (timestamp) => {
    renderer.render(renderContext, camera, sceneGlobals);
    requestAnimationFrame(update);
  }

  requestAnimationFrame(update);
}

requestAnimationFrame(startRendering);