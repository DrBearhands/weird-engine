// @ts-check

import InputHandler from "../engine/IO/InputHandler.js";
import { Vec3, Mat4 } from "../engine/Data/LinearAlgebra.js";
import Camera from "../engine/Data/Geometric/Camera.js";
import { CMD_BACK, CMD_DOWN, CMD_FORWARD, CMD_LEFT, CMD_RIGHT, CMD_UP } from "./keys.js";


const m = Mat4.createZeroes();
const movement = new Vec3();

export class PlayerController {
  cameraPitch = 0.0;
  cameraYaw = 0.0;
  cameraPosition = Vec3.create(0, 0, 2.5);
  motion = new Vec3();
  /** @type {Camera} */
  camera;
  /** @type {InputHandler} */
  inputHandler;

  dragging = false;
  lastMouseX = 0;
  lastMouseY = 0;


  /**
   * 
   * @param {Camera} camera 
   * @param {InputHandler} inputHandler
   */
  constructor(camera, inputHandler) {
    this.camera = camera;
    this.inputHandler = inputHandler;
  }

  update(dt) {
    const moveSpeed = 5.0;
    const motion = this.motion;
    const inputHandler = this.inputHandler;
    Vec3.set(motion, 0, 0, 0);
    motion[2] -= inputHandler.isPressed(CMD_FORWARD);
    motion[2] += inputHandler.isPressed(CMD_BACK);
    motion[0] += inputHandler.isPressed(CMD_RIGHT);
    motion[0] -= inputHandler.isPressed(CMD_LEFT);
    motion[1] += inputHandler.isPressed(CMD_UP);
    motion[1] -= inputHandler.isPressed(CMD_DOWN);

    if (inputHandler.mouseDown) {
      const mx = inputHandler.mousePositionX;
      const my = inputHandler.mousePositionY;
      if (this.dragging) {
        this.cameraYaw += -(mx - this.lastMouseX)*Math.PI*2;
        this.cameraPitch += (my - this.lastMouseY)*Math.PI
      }
      this.lastMouseX = mx;
      this.lastMouseY = my; 
      this.dragging = true;
    } else {
      this.dragging = false;
    }

    Mat4.fromTranslationPitchYaw(m, this.cameraPosition, this.cameraPitch, this.cameraYaw);
        
    const camera = this.camera;

    camera.setTransform(m);

    Vec3.scale(movement, motion, moveSpeed*dt);
    Mat4.multiplyV3(movement, m, movement);

    this.cameraPosition[0] = movement[0];
    this.cameraPosition[1] = movement[1] + this.motion[1]*moveSpeed*dt;
    this.cameraPosition[2] = movement[2];
  }
}