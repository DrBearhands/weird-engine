// @ts-check

import Canvas2DRenderer from "../../engine/Data/DataflowGraphs/Canvas2DRenderer.js";
import { DataflowNode } from "../../engine/Data/DataflowGraphs/DataflowGraph.js";
import GraphEditor from "../../engine/Data/DataflowGraphs/GraphEditor.js";
import * as MaterialGraph from "../../engine/Data/Geometric/Materials/MaterialGraph.js"
import GraphEditorDescriptor from "../../engine/Data/DataflowGraphs/GraphEditorDescriptor.js";
import RenderContext from "../../engine/Graphics/WebGPU/RenderContext.js";
import Renderer from "../../engine/Graphics/WebGPU/Render3D/Renderer.js";
import Material from "../../engine/Graphics/WebGPU/Render3D/Material.js";
import SceneGlobals from "../../engine/Graphics/WebGPU/Render3D/SceneGlobals.js";
import Camera from "../../engine/Data/Geometric/Camera.js";
import { GraphDrawer } from "../../engine/Data/DataflowGraphs/GraphDrawer.js";
import StaticMesh from "../../engine/Graphics/WebGPU/Render3D/Meshes/StaticMesh.js";
import { createSphereGeometry } from "../../engine/Data/Geometric/shapes.js";
import { Vec3 } from "../../engine/Data/LinearAlgebra.js";
import SceneNode from "../../engine/Data/Geometric/SceneGraph.js";

/**
 * 
 * @param {string} id 
 */
function getElementByIdOrThrow(id) {
  const result = document.getElementById(id);
  if (!result) throw `Failed to get element with id ${id}`
  return result;
}

const canvas = getElementByIdOrThrow("canvas");
if (!(canvas instanceof HTMLCanvasElement)) throw "Failed to get canvas";
const materialCanvas = getElementByIdOrThrow("material-canvas");
if (!(materialCanvas instanceof HTMLCanvasElement)) throw "Failed to get materialCanvas";

const editorDescriptor = new GraphEditorDescriptor(MaterialGraph.nodeGenerators);


const canvasRenderer = new Canvas2DRenderer(canvas);
const editor = new GraphEditor(canvasRenderer, editorDescriptor, MaterialGraph.tryConnect);

const newNodeElement = getElementByIdOrThrow("new-node");
for (let ii = 0; ii < MaterialGraph.nodeGenerators.length; ++ii) {
  const generateNode = MaterialGraph.nodeGenerators[ii];
  newNodeElement.innerHTML += `<option value="${ii}">${generateNode({}).label}</option>`;
}
newNodeElement.addEventListener("change", (evt) => {
  const idx = Number.parseInt(evt.target?.value);
  editor.addNode(idx);
});


const renderContext = new RenderContext(materialCanvas);
const renderer = new Renderer();
const sceneGlobals = SceneGlobals.create();
const camera = new Camera();
const sceneGraph = new SceneNode();

camera.lookAt(Vec3.create(0, 0, -3), Vec3.create(0,0,0));
const sphereGeometry = createSphereGeometry(20, 20);
const sphere = StaticMesh.create(sphereGeometry);
const sphereInstance = sphere.createInstance();
sceneGraph.meshes.push(sphereInstance);

function onGraphChange() {
  const graph = editor.graph;
  const materialBuilder = new MaterialGraph.MaterialBuilder();
  const accumulate = MaterialGraph.generateAccumulate(materialBuilder);
  DataflowNode.traverse(accumulate, graph[0]);
  const { material, instance } = Material.fromMaterialData(materialBuilder.data);
  sphereInstance.materialInstance = instance;
}
onGraphChange();


function update() {
  editor.redraw();
  const canvasRect = GraphDrawer.nodeInnerRect(editor.screenPosition, editor.graph[0]);

  materialCanvas.style.left = `${canvasRect.start.x}px`;
  materialCanvas.style.top = `${canvasRect.start.y}px`;
  materialCanvas.style.width = `${canvasRect.size.x}px`;
  materialCanvas.style.height = `${canvasRect.size.y}px`;

  renderer.render(renderContext, camera, sceneGlobals, sceneGraph);
  
  requestAnimationFrame(update);
}

editor.onGraphChange = onGraphChange;

update();

