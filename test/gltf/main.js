// @ts-check
import "../../engine/Graphics/WebGPU/compatibility-layer.js";
import InputHandler from "../../engine/IO/InputHandler.js";
import Renderer from "../../engine/Graphics/WebGPU/Render3D/Renderer.js";
import { GLTFMaterial } from "../../engine/Graphics/WebGPU/Render3D/Materials/GLTFMaterial.js";
import SceneGraph from "../../engine/Data/Geometric/SceneGraph.js";

import StaticMesh from "../../engine/Graphics/WebGPU/Render3D/Meshes/StaticMesh.js"
import * as GLTF from "../../engine/IO/Importers/gltf/gltf.js";
import { GLTFMaterialData } from "../../engine/Data/Geometric/Materials/GLTFMaterialData.js";
import MaterialInstance from "../../engine/Graphics/WebGPU/Render3D/MaterialInstance.js";
import { importHDR } from "../../engine/IO/Importers/hdr/hdr.js";
import Transform from "../../engine/Graphics/WebGPU/Render3D/Transform.js";
import Camera from "../../engine/Data/Geometric/Camera.js";

import { keyBindings } from "../keys.js";
import { PlayerController } from "../PlayerController.js";
import RenderContext from "../../engine/Graphics/WebGPU/RenderContext.js";
import SceneGlobals from "../../engine/Graphics/WebGPU/Render3D/SceneGlobals.js";
import EnvironmentMap from "../../engine/Graphics/WebGPU/Render3D/Materials/EnvironmentMap.js";
import StaticMeshInstance from "../../engine/Graphics/WebGPU/Render3D/Meshes/StaticMeshInstance.js";

const canvas = document.getElementById("main-canvas");
if (!(canvas instanceof HTMLCanvasElement)) throw "Failed to get canvas";

const renderContext = new RenderContext(canvas);
const renderer = new Renderer();
/**
 * @type {SceneGraph<StaticMeshInstance>}
 */
const sceneGraph = new SceneGraph();

const hdrTextureData = await importHDR("./assets/phalzer_forest_01_1k.hdr");
const environmentMap = EnvironmentMap.fromEquirectangularTextureData(hdrTextureData);
const sceneGlobals = SceneGlobals.create(environmentMap);

const inputHandler = new InputHandler(canvas, keyBindings);
const camera = new Camera();
const playerController = new PlayerController(camera, inputHandler);

const gltf_material = new GLTFMaterial();


/**
 * 
 * @param {string} path 
 */
async function makeMeshes(path) {
  const { sceneGraph : importedSceneGraph } = await GLTF.importGLB(path);
  /**
   * @type {Map<GLTF.MeshData, StaticMesh>}
   */
  const cachedMeshes = new Map();
  const cachedMaterials = new Map();

  /**
   * 
   * @param {GLTF.MeshData} meshData 
   * @returns {StaticMesh}
   */
  function getRenderMesh(meshData) {
    const cachedRenderMesh = cachedMeshes.get(meshData);
    if (cachedRenderMesh) {
      return cachedRenderMesh;
    } else {
      const newRenderMesh = StaticMesh.create(meshData.mesh);
      cachedMeshes.set(meshData, newRenderMesh);
      return newRenderMesh;
    }
  }

  /**
   * 
   * @param {GLTFMaterialData} materialData 
   * @returns {MaterialInstance}
   */
  function getMaterialInstance(materialData) {
    const cachedInstance = cachedMaterials.get(materialData);
    if (cachedInstance) {
      return cachedInstance;
    } else {
      const newInstance = gltf_material.createInstance(materialData);
      cachedMaterials.set(materialData, newInstance);
      return newInstance;
    }
  }
  const newNode = importedSceneGraph.map(({mesh, material}) => {
    const materialInsance = getMaterialInstance(material);
    const renderMesh = getRenderMesh(mesh);
    const meshInstance = renderMesh.createInstance();
    meshInstance.materialInstance = materialInsance;
    
    return meshInstance;
  });

  sceneGraph.attachChild(newNode);
}

makeMeshes('./assets/CompareRoughness.glb');


/**
 * 
 * @param {DOMHighResTimeStamp} timestamp 
*/
function startRendering (timestamp) {
  let previousTimestamp = timestamp;
  /**
   * 
   * @param {DOMHighResTimeStamp} timestamp 
   */
  const update = (timestamp) => {
    const dt = (timestamp - previousTimestamp) / 1000;
    previousTimestamp = timestamp;
    playerController.update(dt);
    sceneGraph.updateGlobalTransforms(Transform.updateGPUBuffers);
    renderer.render(renderContext, camera, sceneGlobals, sceneGraph);
    requestAnimationFrame(update);
  }

  requestAnimationFrame(update);
}

requestAnimationFrame(startRendering);