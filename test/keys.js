// @ts-check

export const CMD_FORWARD = 0;
export const CMD_LEFT = 1;
export const CMD_BACK = 2;
export const CMD_RIGHT = 3;
export const CMD_UP = 4;
export const CMD_DOWN = 5;
export const CMD_RUN_EROSION = 6;

export const keyBindings = new Map([
  ["KeyW", CMD_FORWARD],
  ["KeyA", CMD_LEFT],
  ["KeyS", CMD_BACK],
  ["KeyD", CMD_RIGHT],
  ["KeyQ", CMD_UP],
  ["KeyE", CMD_DOWN],
  ["KeyT", CMD_RUN_EROSION]
]);