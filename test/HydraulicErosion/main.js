// @ts-check
import InputHandler from "../../engine/IO/InputHandler.js";
import { Vec3 } from "../../engine/Data/LinearAlgebra.js";
import { convert_hex_sRGB, convert_sRGB_RGB } from "../../engine/utils.js";
import Renderer from "../../engine/Graphics/WebGPU/Render3D/Renderer.js";
import { SlopeColorMaterial } from "../../engine/Graphics/WebGPU/Render3D/Materials/SlopeColor.js";

import HeightmapMesh from "../../engine/Graphics/WebGPU/Render3D/Meshes/HeightmapMesh.js"
import HeightmapCompute from "../../engine/Graphics/WebGPU/Compute/Heightmap.js";
import { keyBindings } from "../keys.js";
import { PlayerController } from "../PlayerController.js";
import Camera from "../../engine/Data/Geometric/Camera.js";
import RenderContext from "../../engine/Graphics/WebGPU/RenderContext.js";
import SceneGlobals from "../../engine/Graphics/WebGPU/Render3D/SceneGlobals.js";
import SceneNode from "../../engine/Data/Geometric/SceneGraph.js";

const canvas = document.getElementById("main-canvas");
if (!(canvas instanceof HTMLCanvasElement)) throw "Failed to get canvas";


const renderContext = new RenderContext(canvas);
const renderer = new Renderer();

const terrainCfg = {
  worldSize : 10.0,
  resolution : 256,
  height : 1.0,
  numOctaves : 8
}

const sceneGlobals = SceneGlobals.create();
const terrainMaterial = new SlopeColorMaterial();
const terrainMaterialInstance = terrainMaterial.createInstance();
const sceneGraph = new SceneNode();

const inputHandler = new InputHandler(canvas, keyBindings);
const camera = new Camera();
const playerController = new PlayerController(camera, inputHandler);

let terrainComputeData;

function generateTerrain() {
  
  const terrainMesh = HeightmapMesh.alloc(terrainCfg.resolution, terrainCfg.worldSize);
  terrainComputeData = HeightmapCompute.create(terrainMesh);
  const terrainMeshInstance = terrainMesh.createInstance();
  sceneGraph.meshes = [terrainMeshInstance];
  terrainMeshInstance.materialInstance = terrainMaterialInstance;
  HeightmapCompute.perlinGPU(terrainComputeData, terrainCfg.height, terrainCfg.numOctaves);
}

generateTerrain();

window.addEventListener("keypress", (evt) => {
  switch (evt.key ) {
    case "t":
      HeightmapCompute.erodeGPU(terrainComputeData, 1024);
      break;
    case "r":
      generateTerrain();
      break;
  }
});

/**
 * 
 * @param {DOMHighResTimeStamp} timestamp 
*/
function startRendering (timestamp) {
  let previousTimestamp = timestamp;
  /**
   * 
   * @param {DOMHighResTimeStamp} timestamp 
   */
  const update = (timestamp) => {
    const dt = (timestamp - previousTimestamp) / 1000;
    previousTimestamp = timestamp;
    playerController.update(dt);
    renderer.render(renderContext, camera, sceneGlobals, sceneGraph);
    requestAnimationFrame(update);
  }

  requestAnimationFrame(update);
}

requestAnimationFrame(startRendering);



const color = new Vec3();

const grassColorPickerElement = document.getElementById("terrain-grass-color-picker");
if (grassColorPickerElement && grassColorPickerElement instanceof HTMLInputElement ) {
  const setColor = () => {
    convert_hex_sRGB(color, grassColorPickerElement.value);
    convert_sRGB_RGB(color, color);
    terrainMaterialInstance.setGrassColor(color);
  }
  grassColorPickerElement.addEventListener("change", setColor);

  setColor();
}

const rockColorPickerElement = document.getElementById("terrain-rock-color-picker");
if (rockColorPickerElement && rockColorPickerElement instanceof HTMLInputElement) {
  const setColor = () => {
    convert_hex_sRGB(color, rockColorPickerElement.value);
    convert_sRGB_RGB(color, color);
    terrainMaterialInstance.setRockColor(color);
  }
  rockColorPickerElement.addEventListener("change", setColor);

  setColor();
}


const terrainSizeSelectElement = document.getElementById("terrain-size-select");
if (terrainSizeSelectElement && terrainSizeSelectElement instanceof HTMLSelectElement) {
  const setTerrainSize = () => {
    terrainCfg.resolution = Number(terrainSizeSelectElement.value);
  }

  terrainSizeSelectElement.addEventListener("change", setTerrainSize);

  setTerrainSize();
}