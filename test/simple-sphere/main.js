// @ts-check
import "../../engine/Graphics/WebGPU/compatibility-layer.js";
import Renderer from "../../engine/Graphics/WebGPU/Render3D/Renderer.js";

import Camera from "../../engine/Data/Geometric/Camera.js";

import RenderContext from "../../engine/Graphics/WebGPU/RenderContext.js";
import SceneGlobals from "../../engine/Graphics/WebGPU/Render3D/SceneGlobals.js";
import { createSphereGeometry } from "../../engine/Data/Geometric/shapes.js";
import StaticMesh from "../../engine/Graphics/WebGPU/Render3D/Meshes/StaticMesh.js";

import DefaultMaterial from "../../engine/Graphics/WebGPU/Render3D/Materials/Default.js";
import { Vec3 } from "../../engine/Data/LinearAlgebra.js";
import SceneNode from "../../engine/Data/Geometric/SceneGraph.js";

const canvas = document.getElementById("main-canvas");
if (!(canvas instanceof HTMLCanvasElement)) throw "Failed to get canvas";

const renderContext = new RenderContext(canvas);
const renderer = new Renderer();
const sceneGlobals = SceneGlobals.create();
const camera = new Camera();
camera.lookAt(Vec3.create(0, 0, -5), Vec3.create(0,0,0));
const sphereGeometry = createSphereGeometry(20, 20);
const sphere = StaticMesh.create(sphereGeometry);
const sphereInstance = sphere.createInstance();
const defaultMaterial = new DefaultMaterial();
const defaultInstance = defaultMaterial.createInstance();
sphereInstance.materialInstance = defaultInstance;
const sceneGraph = new SceneNode();
sceneGraph.meshes.push(sphereInstance);

/**
 * @param {DOMHighResTimeStamp} timestamp 
*/
function startRendering (timestamp) {
  let previousTimestamp = timestamp;
  /**
   * 
   * @param {DOMHighResTimeStamp} timestamp 
   */
  const update = (timestamp) => {
    const dt = (timestamp - previousTimestamp) / 1000;
    previousTimestamp = timestamp;
    renderer.render(renderContext, camera, sceneGlobals, sceneGraph);
    requestAnimationFrame(update);
  }

  requestAnimationFrame(update);
}

requestAnimationFrame(startRendering);