// @ts-check

export default class InputHandler {

  /** @readonly */
  keybindings;

  /**
   * @readonly @type {Uint8Array}
   */
  pressed;

  mousePositionX = -1;
  mousePositionY = -1;
  


  mouseDown = false;

  /**
   * 
   * @param {HTMLCanvasElement} canvas
   * @param {Map<string, number>} keybindings
   */
  constructor(canvas, keybindings) {
    this.keybindings = keybindings;
    this.pressed = new Uint8Array(Math.ceil(this.keybindings.size / 8));
    window.addEventListener("keydown", (evt) => {
      const cmdcode = this.keybindings.get(evt.code);
      if (cmdcode !== undefined) {
        const index = cmdcode/8 | 0;
        const bitmask = 1 << (cmdcode % 8);
        this.pressed[index] = this.pressed[index] | bitmask;
      }
    });
    window.addEventListener("keyup", (evt) => {
      const cmdcode = this.keybindings.get(evt.code);
      if (cmdcode !== undefined) {
        const index = cmdcode/8 | 0;
        const bitmask = 1 << (cmdcode % 8);
        this.pressed[index] = this.pressed[index] & ~bitmask;
      }
    });
    
    canvas.addEventListener("mousemove", (evt) => {
      this.mousePositionX = evt.x/evt.target.clientWidth;
      this.mousePositionY = evt.y/evt.target.clientHeight;
    });

    canvas.addEventListener("mousedown", (evt) => {
      this.mouseDown = true;
    });

    canvas.addEventListener("mouseup", (evt) => {
      this.mouseDown = false;
    })
  }
  /**
   * 
   * @param {number} cmdcode 
   * @returns {1 | 0}
   */
  isPressed(cmdcode) {
    const index = cmdcode/8 | 0;
    const bitmask = 1 << (cmdcode % 8);
    return (this.pressed[index] & bitmask) && 1;
  }
}