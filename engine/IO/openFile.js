// @ts-check

/**
 * @param {(file : File ) => void} func 
 */
export async function openFile(func) {
  const fileInput = document.createElement("input");
  fileInput.type='file';
  fileInput.style.display='none';
  fileInput.onchange = async () => {
    const file = fileInput.files?.item(0);
    if (!file) {
        return;
    }
    func(file);
  }
  document.body.appendChild(fileInput);
  fileInput.click();
}