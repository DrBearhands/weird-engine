// @ts-check
// from https://paulbourke.net/dataformats/pic/
// https://radsite.lbl.gov/radiance/refer/filefmts.pdf
// https://theswissbay.ch/pdf/Gentoomen%20Library/Game%20Development/Programming/Graphics%20Gems%202.pdf (at II-8 adaptive run-length encoding)
import { TextureData } from "../../../Data/Geometric/TextureData.js";
import { Vec4 } from "../../../Data/LinearAlgebra.js";

/**
 * 
 * @param {number} i 
 * @returns {number}
 */
function Uint8toInt8(i) {
  return i = -(i & 128) + (i & ~128);
}

/**
 * 
 * @param {number} mantissa 
 * @param {number} exponent 
 * @returns 
 */
function calcValue(mantissa, exponent) {
  return mantissa/255.0 * Math.pow(2, exponent-127.5)
}

/**
 * 
 * @param {Vec4} v 
 * @param {number} r 
 * @param {number} g 
 * @param {number} b 
 * @param {number} e 
 */
function pixelToFloats(v, r, g, b, e) {
  Vec4.set(v,
    calcValue(r, e),
    calcValue(g, e),
    calcValue(b, e),
    1.0
  );
  if (!Vec4.isFinite(v)) debugger;
}

const magicNeeded = "#?RADIANCE";
const asciiLineFeed = 10;
const asciiPlus = 43;
const asciiMinus = 45;
const asciiX = 88;
const asciiY = 89;
const asciiSpace = 32;

/**
 * 
 * @param {Uint8Array} bytes 
 * @param {number} from 
 * @returns 
 */
function advanceToLine(bytes, from) {
  for (let ii = from; ii < bytes.length; ++ii) {
    if (bytes[ii] === asciiLineFeed)
      return {
        bytes: new Uint8Array(bytes.buffer, bytes.byteOffset + from, ii - from),
        end: ii+1
      };
  }
  return {
    bytes: new Uint8Array(bytes.buffer, bytes.byteOffset + from),
    end: bytes.length - from
  };
}

/**
 * 
 * @param {Uint8Array} bytes 
 * @param {number} char 
 */
function* splitBytes(bytes, char) {
  let lastSplit = 0;
  for (let ii = 0 ; ii < bytes.length; ++ii) {
    if (bytes[ii] === char) {
      yield new Uint8Array(bytes.buffer, bytes.byteOffset + lastSplit, ii - lastSplit);
      lastSplit = ii+1;
    }
  }
  yield new Uint8Array(bytes.buffer, bytes.byteOffset + lastSplit, bytes.byteLength - lastSplit);
}

/**
 * 
 * @param {boolean} yMajor 
 * @param {number} majorRes 
 * @param {number} minorRes 
 * @param {boolean} majorInverted 
 * @param {boolean} minorInverted 
 */
function pixelSetterGenerator(yMajor, majorRes, minorRes, majorInverted, minorInverted) {
  const majorStart = majorInverted ? majorRes -1 : 0;
  const minorStart = minorInverted ? minorRes -1 : 0;
  const majorMul = majorInverted ? -1 : 1;
  const minorMul = minorInverted ? -1 : 1;

  const [xRes, yRes] = yMajor ? [minorRes, majorRes] : [majorRes, minorRes];


  /**
   * @param {Float16Array} arr
   * @param {number} ii
   * @param {Vec4} v
   */
  return (arr, ii, v) => {
    const major = Math.floor(ii / minorRes) * majorMul + majorStart;
    const minor = Math.floor(ii % minorRes) * minorMul + minorStart;
    const [x, y] = yMajor ? [minor, major] : [major, minor];

    const ii_out = x + (yRes-1-y)*xRes;
    arr[ii_out*4+0] = v[0];
    arr[ii_out*4+1] = v[1];
    arr[ii_out*4+2] = v[2];
    arr[ii_out*4+3] = v[3];
  };
}

/**
 * 
 * @param {Uint8Array} bytes
 * 
 */
function parseResolution(bytes) {
  const decoder = new TextDecoder("ascii");
  const parts = [...splitBytes(bytes, asciiSpace)];
  
  const yMajor = parts[0][1] === asciiY ? true : false;

  const majorInverted = parts[0][0] === asciiMinus ? true : false;
  const majorResolutionText = decoder.decode(parts[1]);
  const majorResolution = Number.parseInt(majorResolutionText);

  const minorInverted = parts[2][0] === asciiMinus ? true : false;
  const minorResolutionText = decoder.decode(parts[3]);
  const minorResolution = Number.parseInt(minorResolutionText);

  return {
    xRes : yMajor ? minorResolution : majorResolution,
    yRes : yMajor ? majorResolution : minorResolution,
    setPixel : pixelSetterGenerator(yMajor, majorResolution, minorResolution, majorInverted, minorInverted)
  };
}

/**
 * @param {Uint8Array} p
 */
function pixelIsRun(p) {
  return (p[0] === 255 && p[1] === 255 && p[2] === 255)
}

/**
 * 
 * @param {Uint8Array} p
 * @returns {boolean}
 */
function pixelIsAdaptiveRun(p) {
  return (p[0] === 2) && (p[1] === 2);
}

class SequentialWriter {
  writePosition = 0;
  arr;

  /** @param {number} n */
  constructor(n) {
    this.arr = new Uint8Array(n);
  }

  full() {
    return this.arr.length <= this.writePosition;
  }
  /**
   * 
   * @param {number} byte 
   */
  write(byte) {
    this.arr[this.writePosition++] = byte;
  }

  /**
   * 
   * @param {SequentialReader} reader 
   * @param {number} num 
   */
  writeSequence(reader, num) {
    const arr = this.arr;
    for (let ii = 0; ii < num; ++ii) {
      arr[this.writePosition++] = reader.read1();
    }
  }

  /**
   * 
   * @param {number} byte 
   * @param {number} num 
   */
  writeRepetition(byte, num) {
    const arr = this.arr;
    for (let ii = 0; ii < num; ++ii) {
      arr[this.writePosition++] = byte;
    }
  }
}

class SequentialReader {
  source;
  readPosition = 0;
  /**
   * 
   * @param {Uint8Array} source 
   */
  constructor(source) {
    this.source = source;
  }

  read1() {
    return this.source[this.readPosition++];
  }
  read2() {
    const v = new Uint8Array(2);
    v[0] = this.source[this.readPosition++];
    v[1] = this.source[this.readPosition++];
    return v;
  }
  read4() {
    const v = new Uint8Array(4);
    v[0] = this.source[this.readPosition++];
    v[1] = this.source[this.readPosition++];
    v[2] = this.source[this.readPosition++];
    v[3] = this.source[this.readPosition++];
    return v;
  }

  finished() {
    return this.source.length <= this.readPosition;
  }
}

class SequentialPixelWriter {
  texture;
  setter;
  writePosition = 0;

  /**
   * 
   * @param {Float16Array} texture 
   * @param {(arr : Float16Array, ii : number, v : Vec4) => void} setter 
   */
  constructor(texture, setter) {
    this.texture = texture;
    this.setter = setter;
  }

  /**
   * 
   * @param {Vec4} v 
   */
  write(v) {
    this.setter(this.texture, this.writePosition++, v);
  }

  /**
   * 
   * @param {Vec4} v 
   * @param {number} num 
   */
  writeRepetition(v, num) {
    for (let ii = 0; ii < num; ++ii) {
      this.write(v);
    }
  }
}

/**
 * @param {ArrayBuffer} arrayBuffer 
 * @returns {TextureData}
 */
function parseHDR(arrayBuffer) {
  const bytes = new Uint8Array(arrayBuffer);
  const { bytes: magicBytes, end : headerStart } = advanceToLine(bytes, 0);
  
  const decoder = new TextDecoder("ascii");
  const magicFound = decoder.decode(magicBytes);
  if (magicFound !== magicNeeded) {
    throw `Bad magic ${magicFound}, must be ${magicNeeded}`;
  }

  let resolutionLineReached = false;
  let lineStart = headerStart;
  while(!resolutionLineReached) {
    const { bytes: lineBytes, end : lineEnd } = advanceToLine(bytes, lineStart);
    lineStart = lineEnd;

    if (lineBytes.length === 0) {
      resolutionLineReached = true;
      continue;
    }
    const lineText = decoder.decode(lineBytes);
    console.log(lineText);
  }
  const { bytes: resolutionLineBytes, end : pixelDataStart } = advanceToLine(bytes, lineStart);
  const {setPixel, xRes, yRes } = parseResolution(resolutionLineBytes);

  const pixelData = new Uint8Array(bytes.buffer, pixelDataStart);
  const pixelReader = new SequentialReader(pixelData);
  
  const nPixels = xRes*yRes;
  const textureWriter = new SequentialPixelWriter(new Float16Array(nPixels*4), setPixel);
  let lastPixel = new Vec4;

  while (!pixelReader.finished()) {
    const pixel = pixelReader.read4();
    if (pixelIsRun(pixel)) {
      const runLength = pixel[3];
      textureWriter.writeRepetition(lastPixel, runLength);
    } else
    if (pixelIsAdaptiveRun(pixel)) {
      const scanlineLength = pixel[2]*256+pixel[3];
      const rs = adaptiveRunChannel(scanlineLength, pixelReader);
      const gs = adaptiveRunChannel(scanlineLength, pixelReader);
      const bs = adaptiveRunChannel(scanlineLength, pixelReader);
      const es = adaptiveRunChannel(scanlineLength, pixelReader);
      for (let ii = 0; ii < scanlineLength; ++ii) {
        pixelToFloats(lastPixel, rs[ii], gs[ii], bs[ii], es[ii]);
        textureWriter.write(lastPixel);
      }
      
    } else {
      pixelToFloats(lastPixel, pixel[0], pixel[1], pixel[2], pixel[3]);
      textureWriter.write(lastPixel);
      
    }
  }

  const texture = textureWriter.texture;

  for (let ii = 0; ii < texture.length; ++ii) {
    if (!Number.isFinite(texture[ii])) console.log(`Infinite ${texture[ii]} at ${ii}`);
    if (Number.isNaN(texture[ii])) console.log(`NaN ${texture[ii]} at ${ii}`);
  }

  return {
    data : textureWriter.texture,
    channels : 4,
    xRes,
    yRes,
    name : undefined
  };
}

/**
 * 
 * @param {number} scanlineLength 
 * @param {SequentialReader} byteReader
 */
function adaptiveRunChannel(scanlineLength, byteReader) {
  const rs = new SequentialWriter(scanlineLength);

  while(!rs.full()) {
    const lengthByte = byteReader.read1();
    const isRun = lengthByte > 128;
    if (isRun) {
      const runLength = lengthByte & 127;
      const byteValue = byteReader.read1();
      rs.writeRepetition(byteValue, runLength);
    } else {
      const nonrunLength = lengthByte;
      rs.writeSequence(byteReader, nonrunLength);
    }
  }

  return rs.arr;
}

/**
 * 
 * @param {string} uri 
 */
export async function importHDR(uri) {
  const res = await fetch(uri);
  const data = await res.arrayBuffer();
  return parseHDR(data);
}