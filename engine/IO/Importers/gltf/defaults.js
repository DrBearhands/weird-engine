// @ts-check

import { GLTFMaterialData } from "../../../Data/Geometric/Materials/GLTFMaterialData.js"
import { TextureData } from "../../../Data/Geometric/TextureData.js"

/** @type {TextureData} */
export const baseColorTexture = {
  data: new Uint8Array([255, 255, 255, 255]),
  channels: 4,
  name: "default baseColorTexture",
  xRes: 1,
  yRes: 1
}

/** @type {TextureData} */
export const metallicRoughnessTexture = {
  data: new Uint8Array([0, 250, 0, 0]),
  channels: 4,
  name: "default metallicRoughnessTexture",
  xRes: 1,
  yRes: 1
}

/** @type {TextureData} */
/*export const normalTexture = {
  data: new Int8Array([0, 0, 127, 0]),
  channels: 4,
  name: "default normalTexture"
}*/
export const normalTexture = {
  data: new Uint8Array([128, 128, 255, 0]),
  channels: 4,
  name: "default normalTexture",
  xRes: 1,
  yRes: 1
}

/** @type {TextureData} */
export const occlusionTexture = {
  data : new Uint8Array([1.0, 0.0, 0.0, 0.0]),
  channels: 4,
  name: "default occlusionTexture",
  xRes: 1,
  yRes: 1
}

/** @type {TextureData} */
export const emissiveTexture = {
  data : new Uint8Array([0.0, 0.0, 0.0, 0.0]),
  channels : 4,
  name: "default emissiveTexture",
  xRes: 1,
  yRes: 1
}

export const sampler = {
  /** @type {"linear"} */
  //magFilter : "linear",
  magFilter : "linear",
  /** @type {"nearest"} */
  minFilter : "nearest",
  /** @type {"linear"} */
  mipmapFilter: "linear",
  /** @type {"repeat"} */
  addressModeU : "repeat",
  /** @type {"repeat"} */
  addressModeV : "repeat"
}

/**
* @type {GLTFMaterialData}
*/
export const material = {
  baseColorTexture : {
    texture: baseColorTexture,
    sampler: sampler
  },
  baseColorFactor: [1,1,1,1],
  metallicRoughnessTexture : {
    texture: metallicRoughnessTexture,
    sampler: sampler
  },
  metallicFactor: 1,
  roughnessFactor: 1,
  normalTexture: {
    texture: normalTexture,
    sampler: sampler
  },
  normalScale: 1.0,
  occlusionTexture : {
    texture: occlusionTexture,
    sampler: sampler
  },
  occlusionStrength: 1.0,
  emissiveTexture : {
    texture: emissiveTexture,
    sampler: sampler
  },
  emissiveFactor: [1.0, 1.0, 1.0],
  alphaCutoff: 0.5
}