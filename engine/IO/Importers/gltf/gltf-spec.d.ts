// @ts-check

interface GLTF {
  extensionsUsed?: string[];
  extensionsRequired?: string[];
  accessors?: GLTFAccessor[];
  animations?: GLTFAnimation[];
  asset: GLTFAsset;
  buffers?: GLTFBuffer[];
  bufferViews?: GLTFBufferView[];
  cameras?: GLTFCamera[];
  images?: GLTFImage[];
  materials?: GLTFMaterial[];
  meshes?: GLTFMesh[];
  nodes?: GLTFNode[];
  samplers?: GLTFSampler[];
  scene?: number;
  scenes?: GLTFScene[];
  skins?: GLTFSkin[];
  textures?: GLTFTexture[];
  extensions?: GLTFExtension;
  extras?: GLTFExtras;
}

interface GLTFAccessor {
  bufferView?: number;
  byteOffset?: number;
  componentType: 
    | 5120 // BYTE
    | 5121 // UNSIGNED_BYTE
    | 5122 // SHORT
    | 5123 // UNSIGNED_SHORT
    | 5125 // UNSIGNED_INT
    | 5126;// FLOAT
  normalized?: boolean;
  count: number;
  type: 
    | "SCALAR"
    | "VEC2"
    | "VEC3"
    | "VEC4"
    | "MAT2"
    | "MAT3"
    | "MAT4";
  max?: 
    [ number, number, number, number
    , number, number, number, number
    , number, number, number, number
    , number, number, number, number
    ];
  min?:
    [ number, number, number, number
    , number, number, number, number
    , number, number, number, number
    , number, number, number, number
    ];
  sparse?: GLTFAccessor_Sparse;
  name?: string;
  extensions?: GLTFExtension;
  extras?: GLTFExtras;
}

interface GLTFAccessor_Sparse {
  count: number;
  indices: GLTFAccessor_Sparse_Indices;
  values: GLTFAccessor_Sparse_Values;
  extensions?: GLTFExtension;
  extras?: GLTFExtras;
}

interface GLTFAccessor_Sparse_Indices {
  bufferView: number;
  byteOffset?: number;
  componentType: number;
  extensions?: GLTFExtension;
  extras?: GLTFExtras;
}

interface GLTFAccessor_Sparse_Values {
  bufferView: number;
  byteOffset?: number;
  extensions?: GLTFExtension;
  extras?: GLTFExtras;
}

interface GLTFAnimation {
  channels: GLTFAnimation_Channel[];
  samplers: GLTFAnimation_Samplers[];
  name?: string;
  extensions?: GLTFExtension;
  extras?: GLTFExtras;
}

interface GLTFAnimation_Channel {
  sampler: number;
  target: GLTFAnimation_Channel_Target;
  extensions?: GLTFExtension;
  extras?: GLTFExtras;
}

interface GLTFAnimation_Channel_Target {
  node?: number;
  path: string;
  extensions?: GLTFExtension;
  extras?: GLTFExtras;
}

interface GLTFAsset {
  copyright?: string;
  generator?: string;
  version: GLTFVersion;
  minVersion?: GLTFVersion;
  extensions?: GLTFExtension;
  extras?: GLTFExtras;
}

interface GLTFBuffer {
  uri?: string;
  byteLength: number;
  name?: string;
  extensions?: GLTFExtension;
  extras?: GLTFExtras;
}

interface GLTFBufferView {
  buffer: number;
  byteOffset?: number;
  byteLength: number;
  byteStride?: number;
  target?: 34962 | 34963; // ARRAY_BUFFER | ELEMENT_ARRAY_BUFFER
  name?: string;
  extensions?: GLTFExtension;
  extras?: GLTFExtras;
}

type GLTFCamera =
  | { 
      orthorgraphic: GLTFCamera_Orthographic;
      type: "orthographic";
      name: string;
      extensions?: GLTFExtension;
      extras?: GLTFExtras;
    }
  | {
      perspective: GLTFCamera_Perspective;
      type: "perspective"
      name: string;
      extensions?: GLTFExtension;
      extras?: GLTFExtras;

    }

interface GLTFCamera_Orthographic {
  xmag: number;
  ymag: number;
  zfar: number;
  znear: number;
  extensions?: GLTFExtension;
  extras?: GLTFExtras;
}

interface GLTFCamera_Perspective {
  aspectRatio?: number;
  yfov: number;
  zfar?: number;
  znear: number;
  extensions?: GLTFExtension;
  extras?: GLTFExtras;
}

type GLTFImage =
  {
    uri?: string;
    mimeType?: "image/jpeg" | "image/png";
    bufferView?: number;
    name?: string;
    extensions?: GLTFExtension;
    extras?: GLTFExtras;
  }

interface GLTFMaterial {
  name?: string;
  extensions?: GLTFExtension;
  extras?: GLTFExtras;
  pbrMetallicRoughness?: GLTFMaterial_PBRMetallicRoughness;
  normalTexture?: GLTFMaterial_NormalTextureInfo;
  occlusionTexture?: GLTFMaterial_OcclusionTextureInfo;
  emissiveTexture?: GLTFTextureInfo;
  emissiveFactor?: [number, number, number];
  alphaMode?: 'OPAQUE' | 'MASK' | 'BLEND';
  alphaCutoff?: number;
  doubleSided?: boolean;
}

interface GLTFTextureInfo {
  index: number;
  texCoord?: number;
  extensions?: GLTFExtension;
  extras?: GLTFExtras;
}

interface GLTFMaterial_PBRMetallicRoughness {
  baseColorFactor?: [number, number, number, number];
  baseColorTexture?: GLTFTextureInfo;
  metallicFactor?: number;
  roughnessFactor?: number;
  metallicRoughnessTexture?: GLTFTextureInfo;
  extensions?: GLTFExtension;
  extras?: GLTFExtras;
}

interface GLTFMaterial_NormalTextureInfo {
  index: number;
  texCoord?: number;
  scale?: number;
  extensions?: GLTFExtension;
  extras?: GLTFExtras;
}

interface GLTFMaterial_OcclusionTextureInfo {
  index: number;
  texCoord?: number;
  strength?: number;
  extensions?: GLTFExtension;
  extras?: GLTFExtras;
}

interface GLTFMesh {
  primitives: GLTFMesh_Primitive[];
  weights?: number[];
  name?: string;
  extensions?: GLTFExtension;
  extras?: GLTFExtras;
}

interface GLTFMesh_Primitive {
  attributes: {
    [ key: 
      | `POSITION`
      | `NORMAL`
      | `TANGENT`
      | `TEXCOORD_${number}`
      | `COLOR_${number}`
      | `JOINTS_${number}`
      | `WEIGHTS_${number}`
      | `_${string}`
    ]: number};
  /**
   *  The index of the accessor that contains the vertex indices. When this is
   *  undefined, the primitive defines non-indexed geometry. When defined, the 
   *  accessor MUST have SCALAR type and an unsigned integer component type.
   */
  indices?: number;
  material?: number;
  mode?: 
    | 0 //POINTS 
    | 1 //LINES
    | 2 //LINE_LOOP
    | 3 //LINE_STRIP
    | 4 //TRIANGLES
    | 5 //TRIANGLE_STRIP
    | 6;//TRIANGLE_FAN
  targets?: object[];
  extensions?: GLTFExtension;
  extras?: GLTFExtras;
}

interface GLTFNode {
  camera?: number;
  children?: number[];
  skin?: number;
  matrix?: 
    [ number, number, number, number
    , number, number, number, number
    , number, number, number, number
    , number, number, number, number
    ];
  mesh?: number;
  rotation?: [number, number, number, number];
  scale?: [number, number, number];
  translation?: [number, number, number];
  weights?: number[];
  name?: string;
  extensions?: GLTFExtension;
  extras?: GLTFExtras;
}

interface GLTFSampler {
  magFilter?: 
    | 9728 // NEAREST
    | 9729;// LINEAR
  minFilter?:
    | 9728 // NEAREST
    | 9729 // LINEAR
    | 9984 // NEAREST_MIPMAP_NEAREST
    | 9985 // LINEAR_MIPMAP_NEAREST
    | 9986 // NEAREST_MIPMAP_LINEAR
    | 9987;// LINEAR_MIPMAP_LINEAR
  wrapS?:
    | 33071 // CLAMP_TO_EDGE
    | 33648 // MIRRORED_REPEAT
    | 10497;// REPEAT
  wrapT?:
    | 33071 // CLAMP_TO_EDGE
    | 33648 // MIRRORED_REPEAT
    | 10497;// REPEAT
  name?: string;
  extensions?: GLTFExtension;
  extras?: GLTFExtras;
}

interface GLTFScene {
  nodes?: number[];
  name?: string;
  extensions?: GLTFExtension;
  extras?: GLTFExtras;
}

interface GLTFSkin {
  inverseBindMatrices?: number;
  skeleton?: number;
  joints: number[];
  name?: string;
  extensions?: GLTFExtension;
  extras?: GLTFExtras;
}

interface GLTFTexture {
  sampler?: number;
  source?: number;
  name?: string;
  extensions?: GLTFExtension;
  extras?: GLTFExtras;
}

type GLTFVersion = `${number}.${number}`;

type GLTFExtension = *;
type GLTFExtras = *;