

export const ARRAY_BUFFER = 34962;
export const ELEMENT_ARRAY_BUFFER = 34963;



export const BYTE = 5120;
export const UNSIGNED_BYTE = 5121;
export const SHORT = 5122;
export const UNSIGNED_SHORT = 5123;
export const UNSIGNED_INT = 5125;
export const FLOAT = 5126;

export const NEAREST = 9728;
export const LINEAR = 9729;
export const NEAREST_MIPMAP_NEAREST = 9984;
export const LINEAR_MIPMAP_NEAREST = 9985;
export const NEAREST_MIPMAP_LINEAR = 9986;
export const LINEAR_MIPMAP_LINEAR = 9987;

export const CLAMP_TO_EDGE = 33071;
export const MIRRORED_REPEAT = 33648;
export const REPEAT = 10497;