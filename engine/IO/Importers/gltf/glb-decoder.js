// @ts-check


const glb_header_size = 12;
const chunk_header_size = 8;

const JSON_type = 0x4E4F534A;
const BIN_type = 0x004E4942;

const GLB_MAGIC = 0x46546C67;

/**
 * @param {ArrayBuffer} data 
 */
export async function decodeGLB(data) {
  const header = new Uint32Array(data, 0, glb_header_size);
  if (header[0] !== GLB_MAGIC) throw "Incorrect GLB magic";
  if (header[1] !== 2) throw "Unsupported GLB version";

  const fileSize = header[2];

  const jsonContentOffset = glb_header_size;
  const jsonContentHeader = new Uint32Array(data, jsonContentOffset, chunk_header_size);
  const jsonContentDataLength = jsonContentHeader[0];
  const jsonContentType = jsonContentHeader[1];
  if (jsonContentType !== JSON_type) throw "First chunk must be JSON content";
  const jsonContentDataOffset = glb_header_size+chunk_header_size;
  const jsonContentData = new Uint8Array(data, jsonContentDataOffset, jsonContentDataLength);
  const decoder = new TextDecoder("utf-8");
  const decodedJSON = decoder.decode(jsonContentData);
  const jsonContent = JSON.parse(decodedJSON);

  const binaryBufferOffset = jsonContentDataOffset + jsonContentDataLength;
  const binaryBufferExists = binaryBufferOffset < fileSize;
  /** @type {undefined | Uint8Array} */
  let binaryBuffer;
  if (binaryBufferExists) {
    const binaryBufferHeader = new Uint32Array(data, binaryBufferOffset, chunk_header_size);
    const binaryBufferDataLength = binaryBufferHeader[0];
    const binaryBufferType = binaryBufferHeader[1];
    if (binaryBufferType !== BIN_type) throw "Second chunk must be BIN";
    const binaryBufferDataOffset = binaryBufferOffset + chunk_header_size;
    binaryBuffer = new Uint8Array(data, binaryBufferDataOffset, binaryBufferDataLength);
  }

  return {jsonContent, binaryBuffer};
}