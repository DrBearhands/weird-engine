// @ts-check

import { BYTE, CLAMP_TO_EDGE, FLOAT, LINEAR, LINEAR_MIPMAP_LINEAR, LINEAR_MIPMAP_NEAREST, MIRRORED_REPEAT, NEAREST, NEAREST_MIPMAP_LINEAR, NEAREST_MIPMAP_NEAREST, REPEAT, SHORT, UNSIGNED_BYTE, UNSIGNED_INT, UNSIGNED_SHORT } from "./consts.js";
import { withDefault } from "../../../Data/Undefined.js";
import StaticMeshData from "../../../Data/Geometric/Meshes/StaticMesh.js";
import DynamicMeshData from "../../../Data/Geometric/Meshes/DynamicMeshData.js";
import { sequenceUint32Array } from "../../../utils.js";

import { SamplerData, TextureData, TextureSamplerData } from "../../../Data/Geometric/TextureData.js";
import { GLTFMaterialData } from "../../../Data/Geometric/Materials/GLTFMaterialData.js";
import { decodeGLB } from "./glb-decoder.js";
import { fromFilePath } from "../../uri-path.js";
import * as defaults from "./defaults.js";
import SceneNode from "../../../Data/Geometric/SceneGraph.js";
import Mat4 from "../../../Data/LinearAlgebra/Mat4.js";
import * as Normals from "../../../Data/Geometric/normals.js";
import vec2array from "../../../Data/VectorArrays/Vec2Array.js";
import vec4array from "../../../Data/VectorArrays/Vec4Array.js";

/**
 * @param {string} uri 
 */
export async function importGLB(uri) {
  const res = await fetch(uri);
  const data = await res.arrayBuffer();
  const {jsonContent, binaryBuffer} = await decodeGLB(data);
  return await parseGLTF(uri, jsonContent, binaryBuffer);

}

/**
 * @param {string} uri
 */
export async function importGLTF(uri) {
  const res = await fetch(uri);
  const jsonContent = await res.json();
  return await parseGLTF(uri, jsonContent);
}

/**
 * @typedef {{
 *   sceneGraph : SceneNode<{mesh : MeshData, material : GLTFMaterialData}>,
 * }} ImportedGLTFData
 */

/**
 * @typedef {{
*    type: "static",
*    mesh: StaticMeshData
*  } | {
*    type: "dynamic",
*    mesh: DynamicMeshData
*  }} MeshData
*/

/**
 * @param {string} baseUri
 * @param {GLTF} gltf 
 * @param {Uint8Array | undefined} binaryBuffer
 * @returns {Promise<ImportedGLTFData>}
 */
export async function parseGLTF(baseUri, gltf, binaryBuffer = undefined) {
  const gltf_buffers = withDefault(gltf.buffers, []);
  const gltf_bufferViews = withDefault(gltf.bufferViews, []);
  const gltf_accessors = withDefault(gltf.accessors, []);
  const gltf_meshes = withDefault(gltf.meshes, []);
  const gltf_textures = withDefault(gltf.textures, []);
  const gltf_samplers = withDefault(gltf.samplers, []);
  const gltf_images = withDefault(gltf.images, []);
  
  const buffersData = await getBuffersData(baseUri, gltf_buffers, binaryBuffer)
  const meshesMetaData = getMeshesData(buffersData, gltf_bufferViews, gltf_accessors, gltf_meshes);
  const texturesSamplersData = await getTexturesSamplersData(baseUri, buffersData, gltf_bufferViews, gltf_textures, gltf_samplers, gltf_images);

  /**
   * 
   * @param {number | undefined} index 
   * @param {TextureData} def
   * @returns {TextureSamplerData}
   */
  function getTextureSampler(index, def) {
    if (index === undefined) {
      return {
        texture: def,
        sampler: defaults.sampler
      };
    } else {
      return texturesSamplersData[index];
    }
  }

  /** @type {GLTFMaterialData[]} */
  const materialsData = withDefault(gltf.materials, []).map(gltf_material => {

    const baseColorTexture = getTextureSampler(gltf_material.pbrMetallicRoughness?.baseColorTexture?.index, defaults.baseColorTexture);
    const metallicRoughnessTexture = getTextureSampler(gltf_material.pbrMetallicRoughness?.metallicRoughnessTexture?.index, defaults.metallicRoughnessTexture);
    const normalTexture = getTextureSampler(gltf_material.normalTexture?.index, defaults.normalTexture);
    const occlusionTexture = getTextureSampler(gltf_material.occlusionTexture?.index, defaults.occlusionTexture);
    const emissiveTexture = getTextureSampler(gltf_material.emissiveTexture?.index, defaults.emissiveTexture);

    const res = {
      baseColorTexture,
      baseColorFactor: withDefault(gltf_material.pbrMetallicRoughness?.baseColorFactor, [1,1,1,1]),
      metallicRoughnessTexture,
      metallicFactor: withDefault(gltf_material.pbrMetallicRoughness?.metallicFactor, 1),
      roughnessFactor: withDefault(gltf_material.pbrMetallicRoughness?.roughnessFactor, 1),
      normalTexture,
      normalScale: withDefault(gltf_material.normalTexture?.scale, 1.0),
      occlusionTexture,
      occlusionStrength: withDefault(gltf_material.occlusionTexture?.strength, 1.0),
      emissiveTexture,
      emissiveFactor: withDefault(gltf_material.emissiveFactor, 1.0),
      alphaCutoff: withDefault(gltf_material.alphaCutoff, 0.5)
    };
    return res;
  });

  const meshesWithMaterialsData = meshesMetaData.map(m => {
    const mesh = m.meshData;
    const material = m.materialIndex === undefined ? defaults.material : materialsData[m.materialIndex];

    return {
      mesh,
      material
    }
  });

  const sceneGraph = getSceneGraph(gltf.scene || 0, gltf.scenes || [], gltf.nodes || [], meshesWithMaterialsData)
  

  return {
    sceneGraph
  };
}


/**
 * @param {string} baseUri
 * @param {GLTFBuffer[]} gltfBuffers
 * @param {Uint8Array | undefined} defaultBuffer
 * @returns {Promise<Uint8Array[]>}
 */
async function getBuffersData(baseUri, gltfBuffers, defaultBuffer) {
  const res = [];
  for (const b of gltfBuffers) {
    const uri = b.uri;
    if (uri) {
      res.push(new Uint8Array(await getBufferURI(fromFilePath(baseUri, uri))));
    } else if (defaultBuffer !== undefined) {
      res.push(defaultBuffer)
    } else {
      throw "buffer without URI nor valid data buffer in glb";
    }
  }

  return res;
}

/**
 * @param {Uint8Array[]} buffers
 * @param {GLTFBufferView[]} bufferViews
 * @param {GLTFAccessor[]} accessors
 * @param {GLTFMesh[]} gltf_meshes
 * @returns {{meshData : MeshData, materialIndex : number | undefined}[]}
 */
function getMeshesData(buffers, bufferViews, accessors, gltf_meshes) {
  /** @type {{meshData : MeshData, materialIndex : number | undefined}[]} */
  const res = [];
  for (const mesh of gltf_meshes) {
    for (const primitive of mesh.primitives) {
      const attributes = primitive.attributes;
 
      /**
       * @param {string} str
       * @returns {GLTFAccessor | undefined}
       */
      function getAccessor(str) {
        const index = attributes[str];
        return accessors[index];
      }
 
      /**
       * @param {string} baseString 
       * @returns {Array<GLTFAccessor>}
       */
      function getIndexedAccessors(baseString) {
        const res = [];
        for (let ii = 0; ; ++ii) {
          const texcoordIndex = attributes[baseString + ii];
          if (texcoordIndex === undefined) break;
          res[ii] = accessors[texcoordIndex];
        }
        return res;
      }
      
      const positions = new Float32Array([...access(buffers, bufferViews, getAccessor("POSITION"))]);
      const normals = new Float32Array([...access(buffers, bufferViews, getAccessor("NORMAL"))]);
      const tangents = new Float32Array([...access(buffers, bufferViews, getAccessor("TANGENT"))]);
      
 
      const texcoords = getIndexedAccessors("TEXCOORD_").map( accessor =>
        new Float32Array([...access(buffers, bufferViews, accessor)])
      );
      const colors = getIndexedAccessors("COLOR_").map( accessor => {
        const rgbOrRgbaArray = new Float32Array([...access(buffers, bufferViews, accessor)]);
        if (accessor.type === "VEC3") {
          return vec4array.fromVec3s(rgbOrRgbaArray, 1);
        }
        else return rgbOrRgbaArray;
      });
      const joints = getIndexedAccessors("JOINTS_").map(accessor => 
        new Uint16Array([...access(buffers, bufferViews, accessor)])
      );
 
      const weights = getIndexedAccessors("WEIGHTS_").map(accessor => 
        new Float32Array([...access(buffers, bufferViews, accessor)])
      )
 
      const indices = primitive.indices !== undefined
        ? new Uint32Array([...access(buffers, bufferViews, accessors[primitive.indices])]) 
        : sequenceUint32Array(0, positions.length);
 
      let mesh = {
        indices,
        positions,
        normals,
        tangents,
        texcoords,
        colors,
        //joints,
        //weights,
        mode : withDefault(primitive.mode, 4)
      };
      
      if (mesh.normals.length === 0)
        mesh = Normals.generateNormals(mesh);
      if (mesh.tangents.length === 0)
        Normals.generateSillyTangents(mesh);
      if (mesh.texcoords.length === 0)
        mesh.texcoords.push(new vec2array(positions.length));
      if (mesh.colors.length === 0) {
        mesh.colors.push(new vec4array(positions.length));
        for (let ii = 0; ii < mesh.colors[0].length; ++ii) mesh.colors[0][ii] = 1;
      }

      
      res.push({
        meshData : {
          type: "static",
          mesh
        },
        materialIndex : primitive.material
        }
      );
    }
  }
  return res;
}

/**
 * @param {string} basePath
 * @param {Uint8Array[]} buffers 
 * @param {GLTFBufferView[]} bufferViews 
 * @param {GLTFTexture[]} gltf_textures 
 * @param {GLTFSampler[]} gltf_samplers
 * @param {GLTFImage[]} gltf_images
 * @returns {Promise<TextureSamplerData[]>}
*/
async function getTexturesSamplersData(basePath, buffers, bufferViews, gltf_textures, gltf_samplers, gltf_images) {
  /** @type {ImageBitmap[]} */
  const bitmaps = [];

  for (let img of gltf_images) {
    let bitmap;
    if (img.uri !== undefined) {
      const res = await fetch(fromFilePath(basePath, img.uri));
      bitmap = await createImageBitmap(await res.blob(), { colorSpaceConversion: 'none' });
    } else if (img.bufferView) {
      const bufferView = bufferViews[img.bufferView];
      const buffer = buffers[bufferView.buffer];
      const bytes = new Uint8Array(buffer.buffer, buffer.byteOffset + (bufferView.byteOffset || 0), bufferView.byteLength);
      if (!img.mimeType) throw "Image with bufferView must have mimeType";
      const imageBlob = new Blob([bytes], {type: img.mimeType });
      try {
        bitmap = await createImageBitmap(imageBlob, { colorSpaceConversion: 'none' });
      } catch {
        throw "Invalid image data";
      }
    } else throw "image has neither uri nor bufferView";
    bitmaps.push(bitmap);
  }

  /**
   * @type {SamplerData[]}
   */
  const samplers = gltf_samplers.map(s => {
    return {
      magFilter: "nearest", //convertFilterType(withDefault(s.magFilter, LINEAR)),
      minFilter: "nearest", //convertFilterType(withDefault(s.minFilter, NEAREST_MIPMAP_LINEAR)),
      mipmapFilter: "nearest", //convertFilterType(withDefault(s.minFilter, NEAREST_MIPMAP_LINEAR), true),
      addressModeU: convertWrapType(withDefault(s.wrapS, REPEAT)),
      addressModeV: convertWrapType(withDefault(s.wrapT, REPEAT))
    }
  });

  const textures = [];

  for (const texture of gltf_textures) {
    if (texture.source === undefined) {
      throw "Texture does not have a source";
    }
    const bitmap = bitmaps[texture.source];
    
    textures.push({
      sampler : texture.sampler ? samplers[texture.sampler] : defaults.sampler,
      texture : {
        data: bitmap,
        /** @type {4} */
        channels: 4,
        name: texture.name,
        xRes: bitmap.width,
        yRes: bitmap.height
      }
    });
  }

  return textures;
}

/**
 * @template T
 * @param {number} sceneId 
 * @param {GLTFScene[]} scenes 
 * @param {GLTFNode[]} nodes 
 * @param {T[]} meshes
 * @returns {SceneNode<T>}
 */
function getSceneGraph(sceneId, scenes, nodes, meshes) {
  const scene = scenes[sceneId];
  const sceneRoot = new SceneNode();

  sceneRoot.attachChild(...(scene.nodes?.map(idx => getSceneGraphRecursive(idx, nodes, meshes)) || []))

  return sceneRoot;
}

/**
 * @template T
 * @param {number} idx
 * @param {GLTFNode[]} allNodes
 * @param {T[]} meshes
 * @returns {SceneNode<T>}
 */
function getSceneGraphRecursive(idx, allNodes, meshes) {
  const node = allNodes[idx];
  const out_children = node.children?.map(i => 
    getSceneGraphRecursive(i, allNodes, meshes)
  ) || [];
  /** @type {SceneNode<T>} */
  const out_node = new SceneNode();
  out_node.attachChild(...out_children);
  if (node.matrix) {
    out_node.localTransform = new Float32Array(node.matrix);
  } else {
    const rotation = new Float32Array(node.rotation || [0,0,0,0]);
    const translation = new Float32Array(node.translation || [0,0,0]);
    const scale = new Float32Array(node.scale || [1,1,1]);
    const m = Mat4.createZeroes();
    Mat4.fromRotationTranslationScale(m, rotation, translation, scale);
    out_node.localTransform = m;
  }
   
  if (node.mesh !== undefined) {
    const mesh = meshes[node.mesh];
    out_node.meshes.push(mesh);
  }
  return out_node;

}

/**
 * @param {string} uri 
 * @returns {Promise<Uint8Array>}
 */
async function getBufferURI(uri) {
  const res = await fetch(uri);
  return new Uint8Array(await res.arrayBuffer());
}

/**
 * 
 * @param { AttributeComponentType } componentType 
 * @returns 
 */
function componentByteSize(componentType) {
  switch (componentType) {
    case BYTE:
      return 1;
    case UNSIGNED_BYTE:
      return 1;
    case SHORT:
      return 2;
    case UNSIGNED_SHORT:
      return 2;
    case UNSIGNED_INT:
      return 4;
    case FLOAT:
      return 4;
  }
}

/**
 * 
 * @param { AttributeType } type 
 */
function typeSize(type) {
  switch (type) {
    case "SCALAR":
      return 1;
    case "VEC2":
      return 2;
    case "VEC3":
      return 3;
    case "VEC4":
      return 4;
    case "MAT2":
      return 2;
    case "MAT3":
      return 9;
    case "MAT4":
      return 16;
  }
}

/**
 * 
 * @param {AttributeType} type 
 * @param {AttributeComponentType} componentType 
 * @returns 
 */
function elementByteSize(type, componentType) {
  return typeSize(type) * componentByteSize(componentType);
}

/**
 * @typedef { "SCALAR"
 *          | "VEC2"
 *          | "VEC3"
 *          | "VEC4"
 *          | "MAT2"
 *          | "MAT3"
 *          | "MAT4" 
 * } AttributeType
 */

/**
 * @typedef { BYTE
 *          | UNSIGNED_BYTE
 *          | SHORT
 *          | UNSIGNED_SHORT
 *          | UNSIGNED_INT
 *          | FLOAT
 * } AttributeComponentType
 */

/**
 * @param {Uint8Array[]} buffers
 * @param {GLTFBufferView[]} bufferViews
 * @param {GLTFAccessor | undefined} accessor
 */
function* access(buffers, bufferViews, accessor) {
  if (!accessor) return;
  if (accessor.bufferView === undefined) throw "Accessor has no bufferView";
  const bufferView = bufferViews[accessor.bufferView];
  const buffer = buffers[bufferView.buffer];

  const offset = (bufferView.byteOffset || 0) + (accessor.byteOffset || 0);

  let dataView = new DataView(buffer.buffer, buffer.byteOffset, buffer.byteLength);
  /**
   * @type {(byteOffset : number) => number}
   */
  let read;
  switch (accessor.componentType) {
    case BYTE:
      read = (byteOffset) => dataView.getInt8(byteOffset);
      break;
    case UNSIGNED_BYTE:
      read = (byteOffset) => dataView.getUint8(byteOffset);
      break;
    case SHORT:
      read = (byteOffset) => dataView.getInt16(byteOffset, true);
      break;
    case UNSIGNED_SHORT:
      read = (byteOffset) => dataView.getUint16(byteOffset, true);
      break;
    case UNSIGNED_INT:
      read = (byteOffset) => dataView.getInt32(byteOffset, true);
      break;
    case FLOAT:
      read = (byteOffset) => dataView.getFloat32(byteOffset, true);
      break;
    default:
      throw `Nonexistant component type ${accessor.componentType} in accessor`
  }

  const numComponents = typeSize(accessor.type);
  const byteStride = withDefault(bufferView.byteStride, elementByteSize(accessor.type, accessor.componentType));
  const componentByteStride = componentByteSize(accessor.componentType);
  
  //TODO: sparse stuff
  //TODO: normalization
  for (let ii = 0; ii < accessor.count; ++ii) {
    for (let jj = 0; jj < numComponents; ++jj) {
      yield read(offset + ii * byteStride + jj * componentByteStride);
    }
  }
}


/**
 * 
 * @param {NEAREST | LINEAR | NEAREST_MIPMAP_NEAREST | LINEAR_MIPMAP_NEAREST | NEAREST_MIPMAP_LINEAR | LINEAR_MIPMAP_LINEAR} n
 */
function convertFilterType(n, formipmap = false) {
  switch (n) {
    case NEAREST:
      return "nearest";
    case LINEAR:
      return "linear";
    case NEAREST_MIPMAP_NEAREST:
      return "nearest";
    case LINEAR_MIPMAP_NEAREST:
      return formipmap? "nearest" : "linear";
    case NEAREST_MIPMAP_LINEAR:
      return formipmap? "linear" : "nearest";
    case LINEAR_MIPMAP_LINEAR:
      return "linear";
  }
}

/**
 * 
 * @param {CLAMP_TO_EDGE | MIRRORED_REPEAT | REPEAT} n 
 * @returns 
 */
function convertWrapType(n) {
  switch (n) {
    case CLAMP_TO_EDGE:
      return "clamp-to-edge";
    case MIRRORED_REPEAT:
      return "mirror-repeat";
    case REPEAT:
      return "repeat";
  }
}




