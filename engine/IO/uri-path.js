// @ts-check

/**
 * @param {string} base 
 * @param {string} relative 
 */
export function fromFilePath(base, relative) {
  const pathEnd = base.lastIndexOf('/');
  const dirPath = base.slice(0, pathEnd);
  return dirPath + "/" + relative;
}

