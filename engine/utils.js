// @ts-check

import { Vec3 } from "./Data/LinearAlgebra.js";

/**
 * 
 * @param {number} n 
 * @param {number} m 
 * @returns {number}
 */
export function incrementToMultiple(n,m) {
  return Math.ceil(n/m)*m;
}

/**
 * @param {number} a
 * @param {number} min
 * @param {number} max
 */
export function clamp(a, min, max) {
  return Math.min(Math.max(a, min), max);
}

/**
 * 
 * @param {Array<{data : Float32Array, elementSize : number}>} arrays 
 * @returns {{data : Float32Array, nVerts : number}}
 */
export function interleave(...arrays) {
  const nVerts = Math.min(...arrays.map(a => Math.floor(a.data.length / a.elementSize)));
  let stride = 0;
  for (const a of arrays) {
    stride += a.elementSize;
  }
  const out = new Float32Array(nVerts*stride);
  
  for (let ii = 0, jj = 0; ii < nVerts; ++ii) {
    for (const a of arrays) {
      const data = a.data;
      const start = a.elementSize * ii;
      const stop = start + a.elementSize;
      for (let kk = start; kk < stop; ++kk) {
        out[jj++] = data[kk];
      }
    }
  }
  return {data : out, nVerts : nVerts};
}

/**
 * @param {number} min 
 * @param {number} max 
 * @returns {Uint32Array}
 */
export function sequenceUint32Array(min, max) {
  const length = max - min;
  const res = new Uint32Array(length);
  for (let ii = 0; ii < length; ++ii) {
    res[ii] = min + ii;
  }
  return res;
}

/**
 * 
 * @param {number} sqrtSize 
 * @param {number} x 
 * @param {number} z 
 * @returns {number}
 */
export function squareArrayIndex(sqrtSize, x, z) {
  return x*sqrtSize + z;
}

/**
 * 
 * @param {Vec3} out 
 * @param {string} hex 
 */
export function convert_hex_sRGB(out, hex) {
  const bigInt = parseInt(hex.slice(1), 16);
  out[0] = ((bigInt >> 16) & 255) / 255 ;
  out[1] = ((bigInt >> 8) & 255) / 255;
  out[2] = (bigInt & 255) / 255;
}

/**
 * 
 * @param {Vec3} out 
 * @param {Vec3} sRGB 
 */
export function convert_sRGB_RGB(out, sRGB) {
  out[0] = Math.pow(sRGB[0], 2.2);
  out[1] = Math.pow(sRGB[1], 2.2);
  out[2] = Math.pow(sRGB[2], 2.2);
}