
// This does not work
export class Float16Array {
  dataView;

  constructor(n) {
    const ab = new ArrayBuffer(n*2);
    this.dataView = new DataView(ab);
  }

  /**
   * 
   * @param {number} index 
   * @param {number} value
   */
  set(index, value) {
    throw "Unimplemented";
  }
}

  function check(value) {
    function dec2bin(dec, length = 8) {
      const truncated = (dec >>> 0).toString(2);
      return "0b" + truncated.padStart(length, "0");

    }

    function clamp(v, a, b) {
      if (v < a) return a;
      if (v > b) return b;
      return v;
    }

    const float = new Float32Array(1),
          bytes = new Uint8Array(float.buffer);

    float[0] = value;

    const sign = bytes[3] >> 7;
    const expo32 = bytes[3] << 1 | bytes[2] >> 7;
    const frac32 = ((bytes[2] & 0b01111111) << (16) | bytes[1] << 8 | bytes[0]);

    console.log("exponent 32", dec2bin(expo32, 10));
    console.log("Fraction bits", dec2bin(frac32, 23));
    console.log("Fraction number", 1 + frac32 * Math.pow(2, -23));

    console.log("f32", Math.pow(2, expo32-127) * (1 + frac32 * Math.pow(2, -23)));

    const hf0 = new Float16Array(1);
    hf0[0] = value;
    const hf0_bytes = new Uint8Array(hf0.buffer);
    console.log("Bytes should be:", dec2bin(hf0_bytes[1], 8), dec2bin(hf0_bytes[0]));

    const expo16 = clamp(expo32 - 127 + 14, -14, 15);
    const frac16 = frac32 >> 12;

    console.log("exponent 16", dec2bin(expo16, 5));
    console.log("Fraction bits 16", dec2bin(frac16, 10));
    console.log("Fraction number 16", 1 + frac16 * Math.pow(2, -11));

    const byte1 = (sign << 7) | (expo16 << 2) ;//| (frac16 >> 8);
    const byte0 = frac16 & 0b11111111;

    console.log("Bytes are:", dec2bin(byte1, 8), dec2bin(byte0, 8));
    
    const half_float = new Float16Array(1);
    const hf_bytes = new Uint8Array(half_float.buffer);
    
    hf_bytes[0] = byte0;
    hf_bytes[1] = byte1;
    
    console.log("half-float value", half_float[0]);
  }

// 0b100000000000000000000