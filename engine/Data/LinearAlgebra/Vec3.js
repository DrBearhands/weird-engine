// @ts-check

export default class Vec3 extends Float32Array {
  constructor() {
    super(3);
  }

  /**
   * 
   * @param {number} x 
   * @param {number} y 
   * @param {number} z
   * @returns {Vec3}
   */
  static create(x, y, z) {
    const v = new Float32Array(3);
    v[0] = x;
    v[1] = y;
    v[2] = z;
    return v;
  }

  /**
   * 
   * @param {Vec3} out 
   * @param {number} x 
   * @param {number} y 
   * @param {number} z 
   */
  static set(out, x, y, z) {
    out[0] = x;
    out[1] = y;
    out[2] = z;
  }

  /**
   * 
   * @param {Vec3} out 
   * @param {Vec3} a 
   */
  static copy(out, a) {
    out[0] = a[0];
    out[1] = a[1];
    out[2] = a[2];
  }

  /**
   * 
   * @param {Vec3} out 
   * @param {Vec3} a 
   * @param {Vec3} b 
   */
  static subtract(out, a, b) {
    out[0] = a[0] - b[0];
    out[1] = a[1] - b[1];
    out[2] = a[2] - b[2];
  }

  /**
   * 
   * @param {Vec3} a 
   * @param {Vec3} b 
   * @return {number}
   */
  static dot(a, b) {
    return a[0]*b[0] + a[1]*b[1] + a[2]*b[2];
  }

  static length_() {

  }

  /**
   * @param {Vec3} a 
   * @param {Vec3} b 
   */
  static angle(a, b) {
    const la = Math.sqrt(a[0]*a[0] + a[1]*a[1] + a[2]*a[2]);
    const lb = Math.sqrt(b[0]*b[0] + b[1]*b[1] + b[2]*b[2]);
    const dot = a[0]*b[0] + a[1]*b[1] + a[2]*b[2];

    return Math.acos(dot / la / lb);
  }

  /**
   * 
   * @param {Vec3} out 
   * @param {Vec3} a 
   * @param {Vec3} b 
   */
  static cross(out, a, b) {
    out[0] = a[1]*b[2]-a[2]*b[1];
    out[1] = a[2]*b[0]-a[0]*b[2];
    out[2] = a[0]*b[1]-a[1]*b[0];
  }

  /**
   * 
   * @param {Vec3} out 
   * @param {Vec3} a 
   */
  static normalize(out, a) {
    const l = Math.sqrt(a[0]*a[0]+a[1]*a[1]+a[2]*a[2]);
    out[0] = a[0] / l;
    out[1] = a[1] / l;
    out[2] = a[2] / l;
  }

  /**
   * 
   * @param {Vec3} out 
   * @param {Vec3} a 
   * @param {number} s 
   */
  static scale(out, a, s) {
    out[0] = a[0] * s;
    out[1] = a[1] * s;
    out[2] = a[2] * s;
  }
}