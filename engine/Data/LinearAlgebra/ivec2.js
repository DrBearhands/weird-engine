// @ts-check

export default class ivec2 extends Uint32Array {
  static createZeroes() {
    return new Uint32Array(2);
  }
  
  /**
   * 
   * @param {ivec2} a 
   * @param {number} x 
   * @param {number} y 
   */
  static set(a, x, y) {
    a[0] = x;
    a[1] = y;
  }
}