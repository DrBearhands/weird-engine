// @ts-check


export default class Vec4 extends Float32Array {
  constructor() {
    super(4);
  }

  /**
   * @param {number} x 
   * @param {number} y 
   * @param {number} z 
   * @param {number} w 
   * @returns {Vec4}
   */
  static create(x, y, z, w) {
    const r = new Float32Array(4);
    r[0] = x;
    r[1] = y;
    r[2] = z;
    r[3] = w;
    return r;
  }

  /**
   * 
   * @param {Vec4} out 
   * @param {number} x 
   * @param {number} y 
   * @param {number} z 
   * @param {number} w 
   */
  static set(out, x, y, z, w) {
    out[0] = x;
    out[1] = y;
    out[2] = z;
    out[3] = w;
  }

  /**
   * @param {Vec4} v
   */
  static isFinite(v) {
    return Number.isFinite(v[0]) && Number.isFinite(v[1]) && Number.isFinite(v[2]) && Number.isFinite(v[3]);
  }
}