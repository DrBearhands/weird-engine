// @ts-check

import { EPSILON } from "./base.js";
import Vec3 from "./Vec3.js";
import Vec4 from "./Vec4.js";
import Quat from "./Quat.js";
import Mat3 from "./Mat3.js";

export default class Mat4 extends Float32Array {

  static BYTE_LENGTH = 16*Float32Array.BYTES_PER_ELEMENT;

  /**
   * @returns {Mat4}
   */
  static createIdentity() {
    const r = new Float32Array(16);
    r[0] = 1;
    r[5] = 1;
    r[10] = 1;
    r[15] = 1;
    return r;
  }

  /**
   * @returns {Mat4}
   */
  static createZeroes() {
    return new Float32Array(16);
  }

  /**
   * @param {Mat4} out 
   * @param {Mat4} a 
   */
  static copy(out, a) {
    out[0] = a[0];
    out[1] = a[1];
    out[2] = a[2];
    out[3] = a[3];
    out[4] = a[4];
    out[5] = a[5];
    out[6] = a[6];
    out[7] = a[7];
    out[8] = a[8];
    out[9] = a[9];
    out[10] = a[10];
    out[11] = a[11];
    out[12] = a[12];
    out[13] = a[13];
    out[14] = a[14];
    out[15] = a[15];
  }

  /**
   * 
   * @param {Mat4} out 
   */
  static identity(out) {
    out[0] = 1;
    out[1] = 0;
    out[2] = 0;
    out[3] = 0;

    out[4] = 0;
    out[5] = 1;
    out[6] = 0;
    out[7] = 0;

    out[8] = 0;
    out[9] = 0;
    out[10] = 1;
    out[11] = 0;

    out[12] = 0;
    out[13] = 0;
    out[14] = 0;
    out[15] = 1;
  }

  /**
   * 
   * @param {Mat4} out 
   * @param {Mat4} a 
   * @param {Mat4} b 
   * @returns 
   */
  static multiply(out, a, b) {
    const a00 = a[0],
          a01 = a[1],
          a02 = a[2],
          a03 = a[3],

          a10 = a[4],
          a11 = a[5],
          a12 = a[6],
          a13 = a[7],

          a20 = a[8],
          a21 = a[9],
          a22 = a[10],
          a23 = a[11],

          a30 = a[12],
          a31 = a[13],
          a32 = a[14],
          a33 = a[15];

  let b0 = b[0],
      b1 = b[1],
      b2 = b[2],
      b3 = b[3];

    out[0] = b0 * a00 + b1 * a10 + b2 * a20 + b3 * a30;
    out[1] = b0 * a01 + b1 * a11 + b2 * a21 + b3 * a31;
    out[2] = b0 * a02 + b1 * a12 + b2 * a22 + b3 * a32;
    out[3] = b0 * a03 + b1 * a13 + b2 * a23 + b3 * a33;

    b0 = b[4];
    b1 = b[5];
    b2 = b[6];
    b3 = b[7];

    out[4] = b0 * a00 + b1 * a10 + b2 * a20 + b3 * a30;
    out[5] = b0 * a01 + b1 * a11 + b2 * a21 + b3 * a31;
    out[6] = b0 * a02 + b1 * a12 + b2 * a22 + b3 * a32;
    out[7] = b0 * a03 + b1 * a13 + b2 * a23 + b3 * a33;

    b0 = b[8];
    b1 = b[9];
    b2 = b[10];
    b3 = b[11];

    out[8] = b0 * a00 + b1 * a10 + b2 * a20 + b3 * a30;
    out[9] = b0 * a01 + b1 * a11 + b2 * a21 + b3 * a31;
    out[10] = b0 * a02 + b1 * a12 + b2 * a22 + b3 * a32;
    out[11] = b0 * a03 + b1 * a13 + b2 * a23 + b3 * a33;

    b0 = b[12];
    b1 = b[13];
    b2 = b[14];
    b3 = b[15];

    out[12] = b0 * a00 + b1 * a10 + b2 * a20 + b3 * a30;
    out[13] = b0 * a01 + b1 * a11 + b2 * a21 + b3 * a31;
    out[14] = b0 * a02 + b1 * a12 + b2 * a22 + b3 * a32;
    out[15] = b0 * a03 + b1 * a13 + b2 * a23 + b3 * a33;
  }

  /**
   * 
   * @param {Mat4} out 
   * @param {Mat4} a 
   * @param {number} s 
   */
  static scale(out, a, s) {
    out[0] = a[0] * s;
    out[1] = a[1];
    out[2] = a[2];
    out[3] = a[3];
    out[4] = a[4];
    out[5] = a[5] * s;
    out[6] = a[6];
    out[7] = a[7];
    out[8] = a[8];
    out[9] = a[9];
    out[10] = a[10]*s;
    out[11] = a[11];
    out[12] = a[12];
    out[13] = a[13];
    out[14] = a[14];
    out[15] = a[15];

  }

  /**
   * 
   * @param {Mat4} out 
   * @param {Vec3} v 
   */
  static fromTranslationV(out, v) {
    out[0] = 1;
    out[1] = 0;
    out[2] = 0;
    out[3] = 0;
    out[4] = 0;
    out[5] = 1;
    out[6] = 0;
    out[7] = 0;
    out[8] = 0;
    out[9] = 0;
    out[10] = 1;
    out[11] = 0;
    out[12] = v[0];
    out[13] = v[1];
    out[14] = v[2];
    out[15] = 1;
  }

   /**
   * 
   * @param {Mat4} out 
   * @param {number} x
   * @param {number} y
   * @param {number} z
   */
   static fromTranslation(out, x, y, z) {
    out[0] = 1;
    out[1] = 0;
    out[2] = 0;
    out[3] = 0;
    out[4] = 0;
    out[5] = 1;
    out[6] = 0;
    out[7] = 0;
    out[8] = 0;
    out[9] = 0;
    out[10] = 1;
    out[11] = 0;
    out[12] = x;
    out[13] = y;
    out[14] = z;
    out[15] = 1;
  }

  /**
   * 
   * @param {Mat4} out 
   * @param {Vec3} v 
   * @param {number} pitch 
   */
  static fromTranslationPitch(out, v, pitch) {
    const cosPitch = Math.cos(pitch);
    const sinPitch = Math.sin(pitch);
    /*
    pitchMatrix = 
      [ 1, 0,        0,        0
      , 0, cosPitch,-sinPitch, 0
      , 0, sinPitch, cosPitch, 0
      , 0, 0,        0,        1
      ]
    */
    out[0] = 1;
    out[1] = 0;
    out[2] = 0;
    out[3] = 0;
    out[4] = 0;
    out[5] = cosPitch;
    out[6] = -sinPitch;
    out[7] = 0;
    out[8] = 0;
    out[9] = sinPitch;
    out[10] = cosPitch;
    out[11] = 0;
    out[12] = v[0];
    out[13] = v[1];
    out[14] = v[2];
    out[15] = 1;
  }

  /**
   * 
   * @param {Mat4} out 
   * @param {Vec3} v 
   * @param {number} yaw 
   */
    static fromTranslationYaw(out, v, yaw) {
      const cosYaw = Math.cos(yaw);
      const sinYaw = Math.sin(yaw);
      /*
      yawMatrix = 
        [ cosYaw, 0, -sinYaw, 0
        , 0,      1,  0,        0
        , sinYaw, 0,  cosYaw, 0
        , 0,      0,  0,        1
        ]
      */
      out[0] = cosYaw;
      out[1] = 0;
      out[2] = -sinYaw;
      out[3] = 0;
      out[4] = 0;
      out[5] = 1;
      out[6] = 0;
      out[7] = 0;
      out[8] = sinYaw;
      out[9] = 0;
      out[10] = cosYaw;
      out[11] = 0;
      out[12] = v[0];
      out[13] = v[1];
      out[14] = v[2];
      out[15] = 1;
    }

    /**
   * 
   * @param {Mat4} out 
   * @param {Vec3} v 
   * @param {number} pitch 
   */
    static fromTranslationPitchYaw(out, v, pitch, yaw) {
      const cP = Math.cos(pitch);
      const sP = Math.sin(pitch);
      const cY = Math.cos(yaw);
      const sY = Math.sin(yaw);
      /*

      yawMatrix = 
                                        [ cY, 0, -sY, 0
                                        , 0,  1,  0,  0
                                        , sY, 0,  cY, 0
                                        , 0,  0,  0,  1
                                        ]

      pitchMatrix = 
        [ 1, 0,   0, 0
        , 0, cP,-sP, 0
        , 0, sP, cP, 0
        , 0, 0,   0, 1
        ]

        cY,      0, -sY,   0,
        sY*-sP, cP, -sP*cY, 0,
        sY*cP,  sP, cP*cY, 0,
        0,       0,    0,  1
      */
      out[0] = cY;
      out[1] = 0;
      out[2] = -sY;
      out[3] = 0;
      out[4] = sY*-sP;
      out[5] = cP;
      out[6] = -sP*cY;
      out[7] = 0;
      out[8] = sY*cP;
      out[9] = sP;
      out[10] = cP*cY;
      out[11] = 0;
      out[12] = v[0];
      out[13] = v[1];
      out[14] = v[2];
      out[15] = 1;
    }

  /**
   * 
   * @param {Mat4} out 
   * @param {number} fovy 
   * @param {number} aspect 
   * @param {number} near 
   * @param {number} far may be Infinity
   * @returns 
   */
  static perspectiveProjection(out, fovy, aspect, near, far) {
    const f = 1.0 / Math.tan(fovy / 2);
    let nf;
    out[0] = f / aspect;
    out[1] = 0;
    out[2] = 0;
    out[3] = 0;
    out[4] = 0;
    out[5] = f;
    out[6] = 0;
    out[7] = 0;
    out[8] = 0;
    out[9] = 0;
    out[11] = -1;
    out[12] = 0;
    out[13] = 0;
    out[15] = 0;
    if (far !== Infinity) {
      nf = 1 / (near - far);
      out[10] = (far + near) * nf;
      out[14] = 2 * far * near * nf;
    } else {
      out[10] = -1;
      out[14] = -2 * near;
    }
    return out;
  }

  /**
   * @param {Vec3} out 
   * @param {Mat4} m 
   * @param {Vec3} v 
   */
  static multiplyV3(out, m, v) {
    const x = v[0],
          y = v[1],
          z = v[2];

    const w = ( m[3] * x + m[7] * y + m[11] * z + m[15] ) || 1.0;

    out[0] = (m[0] * x + m[4] * y + m[8] * z + m[12]) / w;
    out[1] = (m[1] * x + m[5] * y + m[9] * z + m[13]) / w;
    out[2] = (m[2] * x + m[6] * y + m[10] * z + m[14]) / w;
  }

  /**
   * ignores the matrix' w transform
   * @param {Vec3} out 
   * @param {Mat4} m 
   * @param {Vec3} v 
   */
  static multiplyV3_(out, m, v) {
    const x = v[0],
          y = v[1],
          z = v[2];

    out[0] = (m[0] * x + m[4] * y + m[8] * z + m[12]);
    out[1] = (m[1] * x + m[5] * y + m[9] * z + m[13]);
    out[2] = (m[2] * x + m[6] * y + m[10] * z + m[14]);
  }

  /**
   * ignores the matrix' w transform
   * @param {Vec4} out 
   * @param {Mat4} m 
   * @param {Vec4} v 
   */
  static multiplyV4(out, m, v) {
    const x = v[0],
          y = v[1],
          z = v[2],
          w = v[3];

    const w_ = ( m[3] * x + m[7] * y + m[11] * z + m[15] * w ) || 1.0;
    out[0] = ( m[0] * x + m[4] * y + m[8] * z + m[12] * w ) / w_;
    out[1] = ( m[1] * x + m[5] * y + m[9] * z + m[13] * w ) / w_;
    out[2] = ( m[2] * x + m[6] * y + m[10] * z + m[14] * w ) / w_;
    out[3] = 1;
  }

  /**
   * 
   * @param {Mat4} out 
   * @param {Mat4} a
   * @returns {boolean} whether inversion succeeded
   */
  static invert(out, a) {
    const a00 = a[0],
          a01 = a[1],
          a02 = a[2],
          a03 = a[3];

    const a10 = a[4],
          a11 = a[5],
          a12 = a[6],
          a13 = a[7];

    const a20 = a[8],
          a21 = a[9],
          a22 = a[10],
          a23 = a[11];

    const a30 = a[12],
          a31 = a[13],
          a32 = a[14],
          a33 = a[15];

    const b00 = a00 * a11 - a01 * a10;
    const b01 = a00 * a12 - a02 * a10;
    const b02 = a00 * a13 - a03 * a10;
    const b03 = a01 * a12 - a02 * a11;
    const b04 = a01 * a13 - a03 * a11;
    const b05 = a02 * a13 - a03 * a12;
    const b06 = a20 * a31 - a21 * a30;
    const b07 = a20 * a32 - a22 * a30;
    const b08 = a20 * a33 - a23 * a30;
    const b09 = a21 * a32 - a22 * a31;
    const b10 = a21 * a33 - a23 * a31;
    const b11 = a22 * a33 - a23 * a32;

    // Calculate the determinant
    const detDiv =
      b00 * b11 - b01 * b10 + b02 * b09 + b03 * b08 - b04 * b07 + b05 * b06;

    if (!detDiv) {
      return false;
    }

    const det = 1.0 / detDiv;

    out[0] = (a11 * b11 - a12 * b10 + a13 * b09) * det;
    out[1] = (a02 * b10 - a01 * b11 - a03 * b09) * det;
    out[2] = (a31 * b05 - a32 * b04 + a33 * b03) * det;
    out[3] = (a22 * b04 - a21 * b05 - a23 * b03) * det;
    out[4] = (a12 * b08 - a10 * b11 - a13 * b07) * det;
    out[5] = (a00 * b11 - a02 * b08 + a03 * b07) * det;
    out[6] = (a32 * b02 - a30 * b05 - a33 * b01) * det;
    out[7] = (a20 * b05 - a22 * b02 + a23 * b01) * det;
    out[8] = (a10 * b10 - a11 * b08 + a13 * b06) * det;
    out[9] = (a01 * b08 - a00 * b10 - a03 * b06) * det;
    out[10] = (a30 * b04 - a31 * b02 + a33 * b00) * det;
    out[11] = (a21 * b02 - a20 * b04 - a23 * b00) * det;
    out[12] = (a11 * b07 - a10 * b09 - a12 * b06) * det;
    out[13] = (a00 * b09 - a01 * b07 + a02 * b06) * det;
    out[14] = (a31 * b01 - a30 * b03 - a32 * b00) * det;
    out[15] = (a20 * b03 - a21 * b01 + a22 * b00) * det;

    return true;
  }

  /**
   * 
   * @param {Mat4} out 
   * @param {Mat4} a 
   */
  static transpose(out, a) {
    const a00 = a[0],
          a01 = a[1],
          a02 = a[2],
          a03 = a[3],
          a10 = a[4],
          a11 = a[5],
          a12 = a[6],
          a13 = a[7],
          a20 = a[8],
          a21 = a[9],
          a22 = a[10],
          a23 = a[11],
          a30 = a[12],
          a31 = a[13],
          a32 = a[14],
          a33 = a[15];
    out[0] = a00;
    out[1] = a10;
    out[2] = a20;
    out[3] = a30;
    out[4] = a01;
    out[5] = a11;
    out[6] = a21;
    out[7] = a31;
    out[8] = a02;
    out[9] = a12;
    out[10] = a22;
    out[11] = a32;
    out[12] = a03;
    out[13] = a13;
    out[14] = a23;
    out[15] = a33;
  }

  /**
   * 
   * @param {Mat3} out 
   * @param {Mat4} a 
   */
  static normalMatrix(out, a) {

    const a00 = a[0],
          a01 = a[1],
          a02 = a[2],
          a03 = a[3];

    const a10 = a[4],
          a11 = a[5],
          a12 = a[6],
          a13 = a[7];

    const a20 = a[8],
          a21 = a[9],
          a22 = a[10],
          a23 = a[11];

    const a30 = a[12],
          a31 = a[13],
          a32 = a[14],
          a33 = a[15];

    const b00 = a00 * a11 - a01 * a10;
    const b01 = a00 * a12 - a02 * a10;
    const b02 = a00 * a13 - a03 * a10;
    const b03 = a01 * a12 - a02 * a11;
    const b04 = a01 * a13 - a03 * a11;
    const b05 = a02 * a13 - a03 * a12;
    const b06 = a20 * a31 - a21 * a30;
    const b07 = a20 * a32 - a22 * a30;
    const b08 = a20 * a33 - a23 * a30;
    const b09 = a21 * a32 - a22 * a31;
    const b10 = a21 * a33 - a23 * a31;
    const b11 = a22 * a33 - a23 * a32;

    // Calculate the determinant
    const detDiv =
      b00 * b11 - b01 * b10 + b02 * b09 + b03 * b08 - b04 * b07 + b05 * b06;

    if (!detDiv) {
      return false;
    }

    const det = 1.0 / detDiv;


    out[0] = (a11 * b11 - a12 * b10 + a13 * b09) * det;
    out[1] = (a12 * b08 - a10 * b11 - a13 * b07) * det;
    out[2] = (a10 * b10 - a11 * b08 + a13 * b06) * det;

    out[3] = (a02 * b10 - a01 * b11 - a03 * b09) * det;
    out[4] = (a00 * b11 - a02 * b08 + a03 * b07) * det;
    out[5] = (a01 * b08 - a00 * b10 - a03 * b06) * det;

    out[6] = (a31 * b05 - a32 * b04 + a33 * b03) * det;
    out[7] = (a32 * b02 - a30 * b05 - a33 * b01) * det;
    out[8] = (a30 * b04 - a31 * b02 + a33 * b00) * det;

    return true;
  }

  /**
   * 
   * @param {Mat4} out 
   * @param {Vec3} eye 
   * @param {Vec3} target 
   * @param {Vec3} up 
   * @returns 
   */
  static lookAt(out, eye, target, up) {
    let x0, x1, x2, y0, y1, y2, z0, z1, z2, len;

    const eyex = eye[0],
          eyey = eye[1],
          eyez = eye[2];
    const upx = up[0],
          upy = up[1],
          upz = up[2];
    const targetx = target[0],
          targety = target[1],
          targetz = target[2];
    if (
      Math.abs(eyex - targetx) < EPSILON &&
      Math.abs(eyey - targety) < EPSILON &&
      Math.abs(eyez - targetz) < EPSILON
    ) {
      return Mat4.identity(out);
    }
    z0 = eyex - targetx;
    z1 = eyey - targety;
    z2 = eyez - targetz;
    len = 1 / Math.hypot(z0, z1, z2);
    z0 *= len;
    z1 *= len;
    z2 *= len;
    x0 = upy * z2 - upz * z1;
    x1 = upz * z0 - upx * z2;
    x2 = upx * z1 - upy * z0;
    len = Math.hypot(x0, x1, x2);
    if (!len) {
      x0 = 0;
      x1 = 0;
      x2 = 0;
    } else {
      len = 1 / len;
      x0 *= len;
      x1 *= len;
      x2 *= len;
    }
    y0 = z1 * x2 - z2 * x1;
    y1 = z2 * x0 - z0 * x2;
    y2 = z0 * x1 - z1 * x0;
    len = Math.hypot(y0, y1, y2);
    if (!len) {
      y0 = 0;
      y1 = 0;
      y2 = 0;
    } else {
      len = 1 / len;
      y0 *= len;
      y1 *= len;
      y2 *= len;
    }
    out[0] = x0;
    out[1] = y0;
    out[2] = z0;
    out[3] = 0;
    out[4] = x1;
    out[5] = y1;
    out[6] = z1;
    out[7] = 0;
    out[8] = x2;
    out[9] = y2;
    out[10] = z2;
    out[11] = 0;
    out[12] = -(x0 * eyex + x1 * eyey + x2 * eyez);
    out[13] = -(y0 * eyex + y1 * eyey + y2 * eyez);
    out[14] = -(z0 * eyex + z1 * eyey + z2 * eyez);
    out[15] = 1;
  }

 /**
  * @param {Mat4} out Mat4 receiving operation result
  * @param {Quat} q Rotation quaternion
  * @param {Vec3} v Translation vector
  * @param {Vec3} s Scaling vector
  * @returns {Mat4} out
  */
  static fromRotationTranslationScale(out, q, v, s) {
    // Quaternion math
    const x = q[0],
          y = q[1],
          z = q[2],
          w = q[3];

    const x2 = x + x;
    const y2 = y + y;
    const z2 = z + z;
    const xx = x * x2;
    const xy = x * y2;
    const xz = x * z2;
    const yy = y * y2;
    const yz = y * z2;
    const zz = z * z2;
    const wx = w * x2;
    const wy = w * y2;
    const wz = w * z2;
    const sx = s[0];
    const sy = s[1];
    const sz = s[2];
    
    out[0] = (1 - (yy + zz)) * sx;
    out[1] = (xy + wz) * sx;
    out[2] = (xz - wy) * sx;
    out[3] = 0;
    out[4] = (xy - wz) * sy;
    out[5] = (1 - (xx + zz)) * sy;
    out[6] = (yz + wx) * sy;
    out[7] = 0;
    out[8] = (xz + wy) * sz;
    out[9] = (yz - wx) * sz;
    out[10] = (1 - (xx + yy)) * sz;
    out[11] = 0;
    out[12] = v[0];
    out[13] = v[1];
    out[14] = v[2];
    out[15] = 1;

    return out;

  }
}
