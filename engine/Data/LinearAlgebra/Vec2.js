// @ts-check

import { interpolateBicubic, interpolateBilinear, interpolateCubic } from "../Numbers.js";

export default class Vec2 extends Float32Array {
  constructor() {
    super(2);
  }

  get x() {
    return this[0];
  }

  get y() {
    return this[1];
  }

  set x(x) {
    this[0] = x;
  }

  set y(y) {
    this[1] = y;
  }

  /**
   * @param {number} x 
   * @param {number} y 
   */
  static create(x, y) {
    const res = new Vec2();
    res[0] = x;
    res[1] = y;
    return res;
  }

  /**
   * @param {Vec2} a 
   * @param {number} x 
   * @param {number} y 
   */
  static set(a, x, y) {
    a[0] = x;
    a[1] = y;
  }

  /**
   * 
   * @param {Vec2} out 
   * @param {Vec2} a 
   */
  static copy(out, a) {
    out[0] = a[0];
    out[1] = a[1];
  }

  static negate(out, a) {
    out[0] = -a[0];
    out[1] = -a[1];
  }

  /**
   * 
   * @param {Vec2} out 
   * @param {Vec2} a 
   * @param {Vec2} b 
   * @param {number} s 
   */
  static interpolateCubic(out, a, b, s) {
    out[0] = interpolateCubic(a[0], b[0], s);
    out[1] = interpolateCubic(a[1], b[1], s);
  }

  /**
   * 
   * @param {Vec2} out 
   * @param {number} wx 
   * @param {number} wy 
   * @param {Vec2} v00 
   * @param {Vec2} v10 
   * @param {Vec2} v01 
   * @param {Vec2} v11 
   */
  static interpolateBilinear(out, wx, wy, v00, v10, v01, v11) {
    out[0] = interpolateBilinear(wx, wy, v00[0], v10[0], v01[0], v11[0]);
    out[1] = interpolateBilinear(wx, wy, v00[1], v10[1], v01[1], v11[1]);
  }

    /**
   * 
   * @param {Vec2} out 
   * @param {number} wx 
   * @param {number} wy 
   * @param {Vec2} v00 
   * @param {Vec2} v10 
   * @param {Vec2} v01 
   * @param {Vec2} v11 
   */
    static interpolateBicubic(out, wx, wy, v00, v10, v01, v11) {
      out[0] = interpolateBicubic(wx, wy, v00[0], v10[0], v01[0], v11[0]);
      out[1] = interpolateBicubic(wx, wy, v00[1], v10[1], v01[1], v11[1]);
    }

  /**
   * 
   * @param {Vec2} out 
   * @param {Vec2} a 
   * @param {Vec2} b 
   * @param {number} s 
   */
  static scaleAndAdd(out, a, b, s) {
    out[0] = a[0] + b[0]*s;
    out[1] = a[1] + b[1]*s;
  }

  static scale(out, a, s) {
    out[0] = a[0]*s;
    out[1] = a[1]*s;
  }

  /**
   * 
   * @param {Vec2} out 
   * @param {Vec2} a 
   * @param {Vec2} b 
   */
  static add(out, a, b) {
    out[0] = a[0] + b[0];
    out[1] = a[1] + b[1];
  }

  /**
   * 
   * @param {Vec2} out 
   * @param {Vec2} a 
   * @param {Vec2} b 
   */
  static sub(out, a, b) {
    out[0] = a[0] - b[0];
    out[1] = a[1] - b[1];
  }

  static length(a) {
    return Math.sqrt(a[0]*a[0]+a[1]*a[1]);
  }

  static normalize(out, a) {
    const d = 1.0 / Math.sqrt(a[0]*a[0]+a[1]*a[1]);
    out[0] = a[0]*d;
    out[1] = a[1]*d;
  }

  /**
   * 
   * @param {Vec2} out 
   */
  static randomUnit(out) {
    const a = Math.random() * Math.PI * 2;
    out[0] = Math.sin(a);
    out[1] = Math.cos(a);
  }

  /**
   * 
   * @param {Vec2} v 
   * @param {number} a 
   * @param {number} b 
   */
  static inBounds(v, a, b) {
    return v[0] > a && v[0] < b && v[1] > a && v[1] < b;
  }

  /**
   * 
   * @param {Vec2} v 
   * @param {Vec2} a 
   * @param {Vec2} b 
   */
  static isInBoundingRect(v, a, b) {
    return v[0] > a[0] && v[0] < b[0] && v[1] > a[1] && v[1] < b[1];
  }

  /**
   * 
   * @param {Vec2} a
   * @returns {boolean}
   */
  static hasNaN(a) {
    return isNaN(a[0]) || isNaN(a[1]);
  }
}