// @ts-check

export default class Mat3 extends Float32Array {
  static BYTE_SIZE = 9*Float32Array.BYTES_PER_ELEMENT;

  constructor() {
    super(9);
  }

  static identity() {
    const res = new Mat3();
    res[0] = 1;
    res[4] = 1;
    res[8] = 1;
    return res;
  }

  /**
   * 
   * @param {number} a 
   * @param {number} b 
   * @param {number} c 
   * @param {number} d 
   * @param {number} e 
   * @param {number} f 
   * @param {number} g 
   * @param {number} h 
   * @param {number} i 
   * @returns 
   */
  static fromValues(a,b,c,d,e,f,g,h,i) {
    const res = new Mat3();
    res[0] = a;
    res[1] = b;
    res[2] = c;
    res[3] = d;
    res[4] = e;
    res[5] = f;
    res[6] = g;
    res[7] = h;
    res[8] = i;
    return res;
  }

  static plusX() {
    return Mat3.fromValues(
      0, 0, -1,
      0, 1, 0,
      1, 0, 0
    );
  }

  static minX() {
    return Mat3.fromValues(
      0, 0, 1,
      0, 1, 0,
      -1, 0, 0
    );
  }

  static plusY() {
    return Mat3.fromValues(
      1, 0, 0,
      0, 0, -1,
      0, 1, 0
    );
  }

  static minY() {
    return Mat3.fromValues(
      1, 0, 0,
      0, 0, 1,
      0, -1, 0
    );
  }

  static plusZ() {
    return Mat3.identity();
  }
  
  static minZ() {
    return Mat3.fromValues(
      -1, 0, 0,
      0, 1, 0,
      0, 0, -1
    )
  }
}