// @ts-check

import { Vec2 } from "../LinearAlgebra.js"

export default class Vec2Array extends Float32Array {

  /** @param {number} n */
  constructor(n) {
    super(n*2);
  }
  /**
   * @param {number} vecCount 
   * @returns {Vec2Array}
   */
  static createZeroes(vecCount) {
    return new Float32Array(vecCount*2);
  }

  /**
   * @param {Vec2} out 
   * @param {Vec2Array} a 
   * @param {number} i 
   */
  static getAt(out, a, i) {
    out[0] = a[i*2];
    out[1] = a[i*2+1];
  }

  /**
   * 
   * @param {Vec2Array} inout 
   * @param {number} i 
   * @param {Vec2} v 
   */
  static setAt(inout, i, v) {
    inout[i*2] = v[0];
    inout[i*2+1] = v[1];
  }
}