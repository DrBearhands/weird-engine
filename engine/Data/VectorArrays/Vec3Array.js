// @ts-check

import {Vec3} from "../LinearAlgebra.js";

export default class Vec3Array extends Float32Array {
  /**
   * 
   * @param {number} size 
   */
  static createZeroes(size) {
    return new Float32Array(size*3);
  }

  /**
   * 
   * @param {Vec3} out 
   * @param {Vec3Array} a 
   * @param {number} i 
   */
  static getAt(out, a, i) {
    out[0] = a[i*3+0];
    out[1] = a[i*3+1];
    out[2] = a[i*3+2];
  }

  /**
   * 
   * @param {Vec3Array} inout 
   * @param {number} i 
   * @param {Vec3} v 
   */
  static setAt(inout, i, v) {
    inout[i*3+0] = v[0];
    inout[i*3+1] = v[1];
    inout[i*3+2] = v[2];
  }

  /**
   * 
   * @param {Vec3Array} inout 
   * @param {number} i 
   * @param {Vec3} v 
   */
  static addVec3(inout, i, v) {
    inout[i*3+0] += v[0];
    inout[i*3+1] += v[1];
    inout[i*3+2] += v[2];
  }

  /**
   * 
   * @param {Vec3Array} inout 
   * @param {number} i 
   * @param {Vec3} v 
   * @param {*} s 
   */
  static addVec3Scaled(inout, i, v, s) {
    inout[i*3+0] += v[0] * s;
    inout[i*3+1] += v[1] * s;
    inout[i*3+2] += v[2] * s;
  }

  /**
   * @param {Vec3Array} inout 
   */
  static normalizeVec3s(inout) {
    for (let ii = 0; ii < inout.length / 3; ++ii) {
      const x = inout[ii*3+0];
      const y = inout[ii*3+1];
      const z = inout[ii*3+2];
      const l = Math.sqrt(x*x+y*y+z*z);
      inout[ii*3+0] /= l;
      inout[ii*3+1] /= l;
      inout[ii*3+2] /= l;
    }
  }
}