// @ts-check

import { Vec4 } from "../LinearAlgebra.js";
import Vec3Array from "./Vec3Array.js";

export default class Vec4Array extends Float32Array {

  /**
   * 
   * @param {number} n 
   */
  constructor(n) {
    super(n*4);
  }

  /**
   * 
   * @param {Vec3Array} a 
   * @param {number} [fill] 
   */
  static fromVec3s(a, fill = 0) {
    const numElems = a.length / 3;
    const res = new Float32Array(numElems * 4)
    for (let ii = 0; ii < numElems; ++ii) {
      res[ii*4+0] = a[ii*3 + 0];
      res[ii*4+1] = a[ii*3 + 1];
      res[ii*4+2] = a[ii*3 + 2];
      res[ii*4+3] = fill;
    }

    return res;
  }

  /**
   * @param {Vec4} out 
   * @param {Vec4Array} a 
   * @param {number} i 
   */
  static getAt(out, a, i) {
    out[0] = a[i*4];
    out[1] = a[i*4+1];
    out[2] = a[i*4+2];
    out[3] = a[i*4+3];
  }


  /**
   * 
   * @param {Vec4Array} inout 
   * @param {number} i 
   * @param {Vec4} a 
   */
  static setAt(inout, i, a) {
    inout[i*4+0] = a[0];
    inout[i*4+1] = a[1];
    inout[i*4+2] = a[2];
    inout[i*4+3] = a[3];
  }
}