


/**
 * @template T
 * @param {T | undefined} a
 * @param {T} default
 * @returns {T}
 */
export function withDefault (a, def) {
  if (a === undefined)
    return def;
  else
    return a;
}

/**
 * @template T
 * @param {T[]} array 
 * @param {number | undefined} n 
 * @param {T} def 
 */
export function indexWithDefault(array, n, def) {
  if (n === undefined) return def;
  else return array[n];
}

/**
 * @template T1
 * @template T2
 * @param {T1[] | undefined} marray 
 * @param {(_ : T1) => T2} f 
 */
export function mapWithEmpty(marray, f) {
  if (marray) {
    return marray.map(f);
  } else return [];
}