// @ts-check

import { Vec2 } from "../../Data/LinearAlgebra.js";
import Canvas2DRenderer from "./Canvas2DRenderer.js";
import { DataflowNode } from "./DataflowGraph.js";
import Rectangle from "./Rectangle.js";

export class CfgEntry {
  /** @type {() => Vec2} */
  extraPadding;
  /** @type {(renderer : Canvas2DRenderer, innerRect: Rectangle) => void} */
  view;
  /** @type {(pos : Vec2) => void} */
  onclick;
}

/**
 * @typedef { {config? : CfgEntry}} Cfg
 */

/**
 * @template T
 * @typedef {DataflowNode<Cfg & T>} NodeCfg
 */