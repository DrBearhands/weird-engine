// @ts-check

import { Vec2 } from "../../Data/LinearAlgebra.js";
import Canvas2DRenderer from "./Canvas2DRenderer.js";
import Rectangle from "./Rectangle.js";

/**
 * @todo: make Node2D generic
 */
const titleStart = 5;
const connectorStart = 30;
const connectorIconSize = 10;
const connectorSpacing = 5;
const connectorStride = connectorIconSize + connectorSpacing;
const connectorXPadding = -3;
const connectorIconTextSpacing = 5;
const connectorMaxLabelSize = 100;
const minimumNodeWidth = 140;

/**
 * @typedef {{position : Vec2}} PositionData
 */
/**
 * @template T
 * @typedef {import("./GraphCfg.js").NodeCfg<PositionData & T>} Node2D
 */

export class GraphDrawer {

  /**
   * @template T
   * @param {Readonly<Vec2>} screenPosition
   * @param {Readonly<Node2D<T>>} node 
   * @returns {Rectangle}
   */
  static nodeRect(screenPosition, node) {
    const maxNumConnectors = Math.max(node.inputs.length, node.outputs.length);
    const innerPadding = node.nodeData.config?.extraPadding() || Vec2.create(0,0);
    const size = Vec2.create(
      minimumNodeWidth + innerPadding.x, 
      connectorStart + Math.max(connectorStride * maxNumConnectors, innerPadding.y)
    );
    const start = Vec2.create(
      node.nodeData.position.x - size.x*0.5 - screenPosition.x, 
      node.nodeData.position.y - size.y*0.5 - screenPosition.y
    );
    return new Rectangle(start, size);
  }

  /**
   * @template T
   * @param {Readonly<Vec2>} screenPosition
   * @param {Readonly<Node2D<T>>} node 
   * @returns {Rectangle}
   */
  static nodeInnerRect(screenPosition, node) {
    /**
     * @todo:
     * Pass inner rect to cfg function
     * update cfg of material elements to draw their uniform name using new cfg definition
     */
    const rect = GraphDrawer.nodeRect(screenPosition, node);
    const innerPadding = node.nodeData.config?.extraPadding() || Vec2.create(0,0);
    rect.size = innerPadding;
    rect.start.x += minimumNodeWidth/2;
    rect.start.y += connectorStart;
    return rect;
  }

  /**
   * @template T
   * @param {Readonly<Vec2>} screenPosition
   * @param {Readonly<Node2D<T>>} node 
   * @returns {Rectangle}
   */
  static nodeBoundingRect(screenPosition, node) {
    const rect = GraphDrawer.nodeRect(screenPosition, node);
    const boundPadding = Math.min(connectorXPadding, 0);
    rect.start.x += boundPadding;
    rect.size.x += boundPadding*2;
    return rect;
  }

  /**
   * @template T
   * @param {Readonly<Vec2>} screenPosition
   * @param {Readonly<Node2D<T>>} node 
   * @param {number} inputIdx
   * @returns {Rectangle}
   */
  static inputRect(screenPosition, node, inputIdx) {
    const rect = GraphDrawer.nodeRect(screenPosition, node);
    rect.start.x += connectorXPadding;
    rect.start.y += connectorStart + connectorStride * inputIdx;
    rect.size.x = connectorIconSize;
    rect.size.y = connectorIconSize;
    return rect;
  }

  /**
   * @template T
   * @param {Readonly<Vec2>} screenPosition
   * @param {Readonly<Node2D<T>>} node
   * @param {number} outputIdx 
   * @returns {Rectangle}
   */
  static outputRect(screenPosition, node, outputIdx) {
    const rect = GraphDrawer.nodeRect(screenPosition, node);
    rect.start.x += rect.size.x - connectorIconSize - connectorXPadding;
    rect.start.y += connectorStart + connectorStride * outputIdx;
    rect.size.x = connectorIconSize;
    rect.size.y = connectorIconSize;
    return rect;
  }

  /**
   * @template T
   * @param {Readonly<Vec2>} screenPosition
   * @param {ReadonlyArray<Node2D<T>>} graph 
   * @param {Readonly<Vec2>} pos 
   * @returns {{node: Node2D<T>, inputIdx: number, rect: Rectangle} | null}
   */
  static getInputAt(screenPosition, graph, pos) {
    for (const node of graph) {
      for (let inputIdx = 0; inputIdx < node.inputs.length; ++inputIdx) {
        const rect = GraphDrawer.inputRect(screenPosition, node, inputIdx);
        if (Rectangle.contains(rect, pos)) {
          return {node, inputIdx, rect};
        }
      }
    }
    return null;
  }

  /**
   * @template T
   * @param {Readonly<Vec2>} screenPosition
   * @param {ReadonlyArray<Node2D<T>>} graph 
   * @param {Readonly<Vec2>} pos 
   * @returns {{node: Node2D<T>, outputIdx: number, rect: Rectangle} | null}
   */
  static getOutputAt(screenPosition, graph, pos) {
    for (const node of graph) {
      for (let outputIdx = 0; outputIdx < node.outputs.length; ++outputIdx) {
        const rect = GraphDrawer.outputRect(screenPosition, node, outputIdx);
        if (Rectangle.contains(rect, pos)) {
          return {node, outputIdx, rect};
        }
      }
    }

    return null;
  }

  /**
   * @template T
   * @param {Vec2} screenPosition
   * @param {ReadonlyArray<Node2D<T>>} graph 
   * @param {Vec2} pos 
   * @returns {{node: Node2D<T>, rect: Rectangle} | null}
   */
  static getNodeAt(screenPosition, graph, pos) {
    for (const node of graph) {
      const rect = GraphDrawer.nodeRect(screenPosition, node);
      if (Rectangle.contains(rect, pos)) {
        return {
          node, rect
        }
      }
    }
    return null;
  }

  /**
   * @todo: make more efficient
   * @template T
   * @param {Readonly<Vec2>} screenPosition
   * @param {ReadonlyArray<Node2D<T>>} graph 
   * @param {Readonly<Vec2>} pos 
   * @returns {Element<T>}
   */
  static getElementAt(screenPosition, graph, pos) {
    const inputHit = GraphDrawer.getInputAt(screenPosition, graph, pos);
    if (inputHit) {
      return {
        hitType: "input",
        node: inputHit.node,
        inputIdx: inputHit.inputIdx,
        rect: inputHit.rect
      }
    }
    const outputHit = GraphDrawer.getOutputAt(screenPosition, graph, pos);
    if (outputHit) {
      return {
        hitType: "output",
        node: outputHit.node,
        outputIdx: outputHit.outputIdx,
        rect: outputHit.rect
      }
    }
    const nodeHit = GraphDrawer.getNodeAt(screenPosition, graph, pos);
    if (nodeHit) {
      const innerRect = GraphDrawer.nodeInnerRect(screenPosition, nodeHit.node);
      if (Rectangle.contains(innerRect, pos)) {
        return {
          hitType: "inner",
          node: nodeHit.node,
          rect: innerRect
        }
      }
      else {
        return {
          hitType: "node",
          node: nodeHit.node,
          rect: nodeHit.rect
        }
      }
    }
    return {
      hitType: "none"
    }
  }

  /**
   * @param {Readonly<Vec2>} p0 
   * @param {Readonly<Vec2>} p3 
   * @returns 
   */
  static bezierBetween(p0,p3) {
    return {
      p0,
      p1 : Vec2.create(p0.x+140, p0.y),
      p2 : Vec2.create(p3.x - 140, p3.y),
      p3
    }
  }

  /**
   * @param {Canvas2DRenderer} renderer 
   * @param {Readonly<Vec2>} screenPosition
   * @param {ReadonlyArray<Node2D<any>>} graph 
   */
  static draw(renderer, screenPosition, graph) {
    renderer.drawInit();
    renderer.setStrokeColor("red");
    for (const node of graph) {
      const inputs = node.inputs;
      for (let inputIdx = 0; inputIdx < inputs.length; ++inputIdx) {
        const input = inputs[inputIdx];
        const connection = input.connection;
        if (connection !== null) {
          const oRect = GraphDrawer.outputRect(screenPosition, connection.node, connection.targetIdx);
          const iRect = GraphDrawer.inputRect(screenPosition, node, inputIdx);
          const {p0, p1, p2, p3} = GraphDrawer.bezierBetween(Rectangle.rightOf(oRect), Rectangle.leftOf(iRect));
          renderer.drawBezier(p0, p1, p2, p3);
        }
      }
    }
    
    for (const node of graph) {
      GraphDrawer.drawNode(renderer, screenPosition, node);
    }
  }

  /**
   * @template T
   * @param {Canvas2DRenderer} renderer
   * @param {Readonly<Vec2>} screenPosition
   * @param {Readonly<Node2D<T>>} node
   */
  static drawNode(renderer, screenPosition, node) {
    const rect = GraphDrawer.nodeRect(screenPosition, node);
    renderer.setStrokeColor("black");
    renderer.setFillColor("lightgrey")
    renderer.drawRect(rect.start, rect.size, true);
    renderer.setFillColor("black");
    const top = Rectangle.topOf(rect);
    const labelPosition = Vec2.create(top.x, top.y+titleStart);
    renderer.drawText(node.label, labelPosition, "top", "center");

    if (node.nodeData.config) {
      const innerRect = GraphDrawer.nodeInnerRect(screenPosition, node);
      node.nodeData.config.view(renderer, innerRect);
    }
    
    renderer.setFillColor("red");
    const inputs = node.inputs;
    for (let inputIdx = 0; inputIdx < inputs.length; ++inputIdx) {
      const rect = GraphDrawer.inputRect(screenPosition, node, inputIdx);
      const right = Rectangle.rightOf(rect);
      renderer.drawRect(rect.start, rect.size);
      const labelPosition = Vec2.create(right.x + connectorIconTextSpacing, right.y);
      renderer.drawText(node.inputs[inputIdx].label, labelPosition, "middle", "left");
    }
    const outputs = node.outputs;
    for (let outputIdx = 0; outputIdx < outputs.length; ++outputIdx) {
      const rect = GraphDrawer.outputRect(screenPosition, node, outputIdx);
      renderer.drawRect(rect.start, rect.size);
      const left = Rectangle.leftOf(rect);
      const labelPosition = Vec2.create(left.x - connectorIconTextSpacing, left.y);
      renderer.drawText(node.outputs[outputIdx].label, labelPosition, "middle", "right");
    }
  }
}

/**
 * @template T
 * @typedef {{
 *  hitType : "none"
 * } | {
 *  hitType : "input",
 *  node : Node2D<T>,
 *  inputIdx : number,
 *  rect : Rectangle
 * } | {
 *  hitType : "output",
 *  node : Node2D<T>,
 *  outputIdx : number,
 *  rect : Rectangle
 * } | {
 *  hitType : "node"
 *  node : Node2D<T>,
 *  rect : Rectangle
 * } | {
 *  hitType : "inner"
 *  node : Node2D<T>,
 *  rect : Rectangle
 * }} Element
 */