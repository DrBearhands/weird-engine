//@ts-check

import { DataflowNode } from "./DataflowGraph.js";

/**
 * @typedef {import("./GraphDrawer.js").PositionData} PositionData
 */

/**
 * @template T
 */
export default class GraphEditorDescriptor {
  nodeGenerators;

  /**
   * @param {Array<(t : PositionData) => DataflowNode<PositionData & T>>} nodeGenerators 
   */
  constructor(nodeGenerators) {
    this.nodeGenerators = nodeGenerators;
  }
}