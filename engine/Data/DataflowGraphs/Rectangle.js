//@ts-check

import { Vec2 } from "../../Data/LinearAlgebra.js";

export default class Rectangle {
  start;
  size;

  /**
   * @param {Vec2} start 
   * @param {Vec2} size
   */
  constructor(start, size) {
    this.start = Vec2.create(start.x, start.y);
    this.size = Vec2.create(size.x, size.y)
  }
  
  /**
   * @param {Rectangle} rect 
   * @returns {Vec2}
   */
  static leftOf(rect) {
    return Vec2.create(rect.start.x, rect.start.y+rect.size.y*0.5);
  }
  /**
   * @param {Rectangle} rect 
   * @returns {Vec2}
   */
  static rightOf(rect) {
    return Vec2.create(rect.start.x+rect.size.x, rect.start.y+rect.size.y*0.5);
  }
  /**
   * @param {Rectangle} rect 
   * @returns {Vec2}
   */
  static topOf(rect) {
    return Vec2.create(rect.start.x+rect.size.x*0.5, rect.start.y);
  }

  /**
   * @param {Rectangle} rect
   * @param {Vec2} pos 
   * @returns 
   */
  static contains(rect, pos) {
    const xRel = pos.x - rect.start.x;
    const yRel = pos.y - rect.start.y;
    return xRel >= 0 && xRel <= rect.size.x && yRel >= 0 && yRel <= rect.size.y;
  }
}
