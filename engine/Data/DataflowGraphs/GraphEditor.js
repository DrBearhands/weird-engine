// @ts-check

import { Vec2 } from "../../Data/LinearAlgebra.js";
import Canvas2DRenderer from "./Canvas2DRenderer.js";
import { DataflowNode } from "./DataflowGraph.js";
import { GraphDrawer } from "./GraphDrawer.js";
import GraphEditorDescriptor from "./GraphEditorDescriptor.js";
import Rectangle from "./Rectangle.js";

const highlightColor = "yellow";

/**
 * @template T
 * @typedef {import("./GraphDrawer.js").Element<T>} Element
 */
/**
 * @template T
 * @typedef {import("./GraphDrawer.js").Node2D<T>} Node2D
 */
/**
 * @typedef {import("./GraphDrawer.js").PositionData} PositionData 
 */

/**
 * @template T
 */
export default class GraphEditor {
  renderer;
  graph;
  descriptor;
  #tryConnect;
  screenPosition = new Vec2();
  /** @type {MouseAction} */
  #initialAction;
  /** @type {MouseAction} */
  #action;

  #highlight = new Highlight();
  /**
   * 
   * @param {Canvas2DRenderer} renderer 
   * @param {GraphEditorDescriptor<T>} descriptor
   * @param {(
   *  outputNode : Node2D<T>,
   *  outputIdx : number,
   *  inputNode : Node2D<T>,
   *  inputIdx : number
   * ) => boolean} tryConnect
   */
  constructor(renderer, descriptor, tryConnect) {
    const canvas = renderer.canvas;
    renderer.drawInit();
    
    const firstNodePosition = {
      position: Vec2.create(renderer.canvas.width-300,renderer.canvas.height*0.5)
    }
    const graph = [descriptor.nodeGenerators[0](firstNodePosition)];
    
    canvas.addEventListener("mousedown", (evt) => {
      this.handleMouseDown(evt);
    });
 
    canvas.addEventListener("mousemove", (evt) => {
      this.handleMouseMove(evt);
    });
  
    canvas.addEventListener("mouseup", (evt) => {
      this.handleMouseUp(evt);
    });

    this.renderer = renderer;
    this.descriptor = descriptor;
    this.graph = graph;
    this.#tryConnect = tryConnect;
    this.#initialAction = new InitialAction(this);
    this.#action = this.#initialAction;
  }

  /**
   * @type {() => void}
   */
  onGraphChange;

  tryConnect(outputNode, outputIdx, inputNode, inputIdx) {
    const modified = this.#tryConnect(outputNode, outputIdx, inputNode, inputIdx);
    if (modified && this.onGraphChange) this.onGraphChange();
  }

  redraw() {
    const renderer = this.renderer;
    const graph = this.graph;
    GraphDrawer.draw(renderer, this.screenPosition, graph);
    this.#action.postDraw(renderer);

    const highlight = this.#highlight;
    const node = highlight.node;
    if (node) {
      
      renderer.setFillColor(highlightColor);
      renderer.setStrokeColor(highlightColor);
      const nodeRect = GraphDrawer.nodeRect(this.screenPosition, node);
      
      const inputIdx = highlight.inputIdx;
      const outputIdx = highlight.outputIdx;
      if (inputIdx !== null) {
        const inputRect = GraphDrawer.inputRect(this.screenPosition, node, inputIdx);
        renderer.drawRect(inputRect.start, inputRect.size, false);
      }
      else if (outputIdx !== null) {
        const outputRect = GraphDrawer.outputRect(this.screenPosition, node, outputIdx);
        renderer.drawRect(outputRect.start, outputRect.size, false);
      } else {
        renderer.drawRect(nodeRect.start, nodeRect.size, true, false);
      }
    }
  }

  /**
   * @param {MouseEvent} evt 
   */
  handleMouseDown(evt) {
    evt.preventDefault();
    evt.stopPropagation();
    evt.stopImmediatePropagation();
    const clickPos = Vec2.create(evt.offsetX * devicePixelRatio, evt.offsetY * devicePixelRatio);
    const underMouse = GraphDrawer.getElementAt(this.screenPosition, this.graph, clickPos);
    let newAction = null;
    switch (evt.button) {
      case 0:
        newAction = this.#action.mouseDown(underMouse, clickPos);
      break;
      case 1:
        newAction = this.#action.middleMouseDown(clickPos);
      break;
      case 2:
        newAction = this.#action.rightMouseDown(underMouse, clickPos);
      break;
    }
    this.#action = newAction || this.#initialAction;

    return true;
  }

  /**
   * @param {MouseEvent} evt 
   */
  handleMouseMove(evt) {
    const pos = Vec2.create(evt.offsetX * devicePixelRatio, evt.offsetY * devicePixelRatio);
    const underMouse = GraphDrawer.getElementAt(this.screenPosition, this.graph, pos);
    this.#action.mouseMove(underMouse, pos);
    this.#highlight = Highlight.fromElement(underMouse);
  }
  /**
   * @param {MouseEvent} evt 
   */
  handleMouseUp(evt) {
    evt.preventDefault();
    evt.stopPropagation();
    evt.stopImmediatePropagation();
    const clickPos = Vec2.create(evt.offsetX * devicePixelRatio, evt.offsetY * devicePixelRatio);
    const underMouse = GraphDrawer.getElementAt(this.screenPosition, this.graph, clickPos);
    let newAction = null;
    switch (evt.button) {
      case 0:
        newAction = this.#action.mouseUp(underMouse, clickPos);
        break;
      case 1:
        newAction = this.#action.middleMouseUp(clickPos);
        break;
      case 2:
        newAction = this.#action.rightMouseUp(underMouse, clickPos);
    }

    this.#action = newAction || this.#initialAction;

    return false;
  }

  /**
   * @param {number} generatorIdx 
   */
  addNode(generatorIdx) {
    const nodeGenerator = this.descriptor.nodeGenerators[generatorIdx];
    this.#action = new NewNodeAction(this, nodeGenerator);
  }
}

/**
 * @template T
 */
class MouseAction {
  /**
   * @param {Element<T>} e 
   * @param {Vec2} pos 
   * @returns {MouseAction<T> | null} the next action to perform
   */
  mouseDown(e, pos) { return this; };
  /**
   * @param {Element<T>} e 
   * @param {Vec2} pos 
   * @returns {MouseAction<T> | null} the next action to perform
   */
  mouseUp(e, pos) { return this; };

  /**
   * @param {Vec2} pos
   * @returns {MouseAction<T> | null} the next action to perform
   */
  middleMouseDown(pos) { return this; }
  /**
   * @param {Vec2} pos
   * @returns {MouseAction<T> | null} the next action to perform
   */
  middleMouseUp(pos) { return this; }
  /**
   * @param {Element<T>} e 
   * @param {Vec2} pos
   * @returns {MouseAction<T> | null} the next action to perform
   */
  rightMouseDown(e, pos) { return this; }
  /**
   * @param {Element<T>} e 
   * @param {Vec2} pos
   * @returns {MouseAction<T> | null} the next action to perform
   */
  rightMouseUp(e, pos) { return this; }
  /**
   * @param {Element<T>} e 
   * @param {Vec2} pos
   */
  mouseMove(e, pos) { };

  /**
   * @param {Canvas2DRenderer} renderer 
   */
  postDraw(renderer) {};
}

/**
 * @template T
 */
class InitialAction extends MouseAction {
  graphEditor;
  /**
   * 
   * @param {GraphEditor<T>} graphEditor 
   */
  constructor(graphEditor) {
    super();
    this.graphEditor = graphEditor;
  }
  /**
   * @param {Element<T>} e 
   * @param {Vec2} pos 
   */
  mouseDown(e, pos) { 
    const graphEditor = this.graphEditor;
    switch (e.hitType) {
      case "none":
        break;
      case "node":
        return new MoveNodeAction(e.node, pos);
      case "input":
        DataflowNode.disconnect(e.node, e.inputIdx);
        graphEditor.onGraphChange();
        return new DragConnectorAction(graphEditor, e.node, e.inputIdx, true, e.rect, pos);
      case "output":
        return new DragConnectorAction(graphEditor, e.node, e.outputIdx, false, e.rect, pos);
      case "inner":
        const relativePos = new Vec2();
        Vec2.sub(relativePos, pos, GraphDrawer.nodeInnerRect(graphEditor.screenPosition, e.node).start);
        e.node.nodeData.config?.onclick(relativePos);
        graphEditor.onGraphChange();
        break;
    }
    return this; 
  };

  /**
   * @param {Vec2} pos 
   */
  middleMouseDown(pos) {
    return new PanScreenAction(this.graphEditor, pos);
  }
}

/**
 * @template T
 */
class MoveNodeAction extends MouseAction {
  prevPos = new Vec2();
  node;

  /**
   * @param {Node2D<T>} node 
   * @param {Vec2} clickPos 
   */
  constructor(node, clickPos) {
    super();

    Vec2.copy(this.prevPos, clickPos);
    this.node = node;
  }
  
  mouseUp() { 
    return null; 
  };

  /**
   * @param {Element<T>} e 
   * @param {Vec2} pos
   * @returns Whether the action was completed
   */
  mouseMove(e, pos) { 
    const node = this.node;
    const delta = new Vec2();
    const nodePosition = node.nodeData.position;
    Vec2.sub(delta, pos, this.prevPos);
    Vec2.add(nodePosition, nodePosition, delta);
    Vec2.copy(this.prevPos, pos);
    return false; 
  };
}

/**
 * @template T
 */
class NewNodeAction extends MouseAction {
  graphEditor;
  newNode;

  /**
   * @param {GraphEditor<T>} graphEditor
   * @param {(p : PositionData) => Node2D<T>} nodeGenerator 
   */
  constructor(graphEditor, nodeGenerator) {
    super();
    this.graphEditor = graphEditor;
    this.newNode = nodeGenerator({ position: new Vec2()});
  }
  
  /**
   * @param {Element<T>} e 
   * @param {Vec2} pos 
  */
  mouseDown(e, pos) {
    this.graphEditor.graph.push(this.newNode);
    return null;
  }

  /**
   * 
   * @param {Element<T>} e 
   * @param {Vec2} pos 
   */
  mouseMove(e, pos) {
    Vec2.add(this.newNode.nodeData.position, pos, this.graphEditor.screenPosition);
  }
  /**
   * 
   * @param {Canvas2DRenderer} renderer 
   */
  postDraw(renderer) {
    GraphDrawer.drawNode(renderer, this.graphEditor.screenPosition, this.newNode);
  }

  rightMouseDown() {
    return null;
  }
}

/**
 * @template T
 */
class DragConnectorAction extends MouseAction {
  graphEditor;
  node;
  connectorId;
  isInput;
  from;
  pos = new Vec2();

  /**
   * @param {GraphEditor<T>} graphEditor
   * @param {Node2D<T>} node 
   * @param {number} connectorId 
   * @param {boolean} isInput 
   * @param {Rectangle} from
   * @param {Vec2} pos
   */
  constructor(graphEditor, node, connectorId, isInput, from, pos) {
    super();
    this.graphEditor = graphEditor;
    this.node = node;
    this.connectorId = connectorId;
    this.isInput = isInput;
    this.from = from;
    Vec2.copy(this.pos, pos);

  }

  /**
   * @param {Element<T>} e 
   * @param {Vec2} pos 
   */
  mouseUp(e, pos) {
    const graphEditor = this.graphEditor;
    if (this.isInput) {
      const dropTarget = GraphDrawer.getOutputAt(this.graphEditor.screenPosition, graphEditor.graph, pos);
      if (dropTarget) {
        graphEditor.tryConnect(dropTarget.node, dropTarget.outputIdx, this.node, this.connectorId)
      }
    } else {
      const dropTarget = GraphDrawer.getInputAt(this.graphEditor.screenPosition, graphEditor.graph, pos);
      if (dropTarget) {
        graphEditor.tryConnect(this.node, this.connectorId, dropTarget.node, dropTarget.inputIdx)
      }
    }
    return null; 
  };

  /**
   * @param {Element<T>} e 
   * @param {Vec2} pos 
   */
  mouseMove(e, pos) {
    Vec2.copy(this.pos, pos);
  }

  /**
   * @param {Vec2} pos 
   */
  #bezierPoints(pos) {
    if (this.isInput) {
      return GraphDrawer.bezierBetween(pos, Rectangle.rightOf(this.from));
    } else {
      return GraphDrawer.bezierBetween(Rectangle.leftOf(this.from), pos)
    }
  }
  /**
   * @param {Canvas2DRenderer} renderer 
   */
  postDraw(renderer) {
    renderer.setStrokeColor(highlightColor);
    const {p0, p1, p2, p3} = this.#bezierPoints(this.pos);
    renderer.drawBezier(p0, p1, p2, p3);
  };
}

/**
 * @template T
 */
class PanScreenAction extends MouseAction {
  graphEditor;
  prevPos;

  /**
   * @param {GraphEditor<T>} graphEditor 
   * @param {Vec2} pos
   */
  constructor(graphEditor, pos) {
    super();
    this.graphEditor = graphEditor;
    this.prevPos = Vec2.create(pos.x, pos.y);
  }
  /**
   * 
   * @param {*} e 
   * @param {Vec2} pos 
   */
  mouseMove(e, pos) {
    const prevPos = this.prevPos;
    const delta = Vec2.create(prevPos.x - pos.x, prevPos.y - pos.y);
    Vec2.add(this.graphEditor.screenPosition, this.graphEditor.screenPosition, delta);
    Vec2.copy(prevPos, pos);
  }
  middleMouseUp() {
    return null;
  }
}

class Highlight {
  /** @type {DataflowNode | null} */
  node = null;
  /** @type {number | null} */
  inputIdx = null;
  /** @type {number | null} */
  outputIdx = null;

  /**
   * @param {Element<any>} e 
   * @returns {Highlight}
   */
  static fromElement(e) {
    let node = null;
    let inputIdx = null;
    let outputIdx = null;
    switch (e.hitType) {
      case "none":
      break;
      case "node":
        node = e.node;
        break;
      case "input":
        node = e.node;
        inputIdx = e.inputIdx;
        break;
      case "output":
        node = e.node;
        outputIdx = e.outputIdx;
        break;
    }
    return {
      node,
      inputIdx,
      outputIdx
    }
  }
}