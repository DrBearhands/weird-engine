// @ts-check

/**
 * @template T
 */
export class DataflowNode {
  /** @type {string} */
  label;
  /** @type {{label: string, connection: Connection<T> | null}[]} */
  inputs;
  /** @type {{label: string, connections: Map<Connection<T>, Connection<T>>}[]} */
  outputs;
  /** @type {T} */
  nodeData;

  /**
   * 
   * @param {{
   *  label : string,
   *  inputs : Array<{label : string}>,
   *  outputs : Array<{label : string}>,
   *  nodeData : T
   * }} param0 
   */
  constructor({label, inputs, outputs, nodeData}) {
    this.label = label;
    this.inputs = inputs.map(({label}) => { return {label, connection: null}})
    this.outputs = outputs.map(({label}) => { return { label, connections: new Map() }; });
    this.nodeData = nodeData;
  }

  /**
   * @template T
   * @param {DataflowNode<T>} outputNode 
   * @param {number} outputIdx 
   * @param {DataflowNode<T>} inputNode 
   * @param {number} inputIdx 
   */
  static connect(outputNode, outputIdx, inputNode, inputIdx) {
    DataflowNode.disconnect(inputNode, inputIdx);
    const connection = new Connection(outputNode, outputIdx);
    inputNode.inputs[inputIdx].connection = connection;
    outputNode.outputs[outputIdx].connections.set(connection, new Connection(inputNode, inputIdx));
  }

  /**
   * @template T
   * @param {DataflowNode<T>} inputNode 
   * @param {number} inputIdx 
   */
  static disconnect(inputNode, inputIdx) {
    const oldConnection = inputNode.inputs[inputIdx].connection;
    if (oldConnection) {
      oldConnection.node.outputs[oldConnection.targetIdx].connections.delete(oldConnection);
      inputNode.inputs[inputIdx].connection = null;
    }
  }

  /**
   * @template T
   * @param {(node : DataflowNode<T>) => void} f
   * @param {DataflowNode<T>} node
   */
  static traverse(f, node) {
    const visitedNodes = new Set();
    /**
     * @param {DataflowNode<T>} node 
     */
    function recurse(node) {
      if (visitedNodes.has(node)) return;
      visitedNodes.add(node);
      const inputs = node.inputs
    
      for (const {connection} of inputs) {
        if (connection) {
          recurse(connection.node);
        }
      }
      f(node);
    }
    recurse(node);
  }

  /**
   * @template T
   * @param {(node : DataflowNode<T>) => boolean} f
   * @param {DataflowNode<T>} node
   */
  static partialReverseTraverse(f, node) {
    const visitedNodes = new Set();
    /**
     * @param {DataflowNode<T>} node 
     */
    function recurse(node) {
      if (visitedNodes.has(node)) return;
      visitedNodes.add(node);
      
      const doRecurse = f(node);

      if (doRecurse) {
        for (const output of node.outputs) {
          for (const connection of output.connections.values()) {
            recurse(connection.node);
          }
        }
      }
    }
    
    recurse(node);
  }
}

/** 
 * @template T
 * @typedef { {
 *   label : string, 
 *   inputs : ReadonlyArray<{label: string, connection: Readonly<Connection<T>> | null}>,
 *   outputs : ReadonlyArray<{label: string, connections: Map<Connection<T>, Connection<T>>}>,
 *   nodeData : Readonly<T>
 * } } ReadonlyNode 
 */
/**
 * @template T
 */
export class Connection {
  node;
  targetIdx;

  /**
   * @param {DataflowNode<T>} node 
   * @param {number} targetIdx
   */
  constructor(node, targetIdx) {
    this.node = node
    this.targetIdx = targetIdx;
  }
}
