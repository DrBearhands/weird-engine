// @ts-check

import { Vec2 } from "../../Data/LinearAlgebra.js";

export default class Canvas2DRenderer {
  /** @readonly */
  canvas;
  /** @readonly */
  context;
  /**
   * @param {HTMLCanvasElement} canvas 
   */
  constructor(canvas) {
    const context = canvas.getContext("2d");
    if (!context) throw "Failed to get 2d context from canvas";

    this.canvas = canvas;
    this.context = context;
  }

  drawInit() {
    const canvas = this.canvas;
    const width = canvas.clientWidth * devicePixelRatio;
    const height = canvas.clientHeight * devicePixelRatio;
    canvas.width = width;
    canvas.height = height;
  }

  /**
   * @param {Vec2} start 
   * @param {Vec2} size 
   */
  drawRect(start, size, withBorder = false, withFill = true) {
    const ctx = this.context;
    if (withFill) {
      ctx.fillRect(start.x, start.y, size.x, size.y);
    }
    if (withBorder) {
      ctx.strokeRect(start.x, start.y, size.x, size.y);
    }
  }

  /**
   * @param {Vec2} p0 
   * @param {Vec2} p1 
   * @param {Vec2} p2 
   * @param {Vec2} p3 
   */
  drawBezier(p0, p1, p2, p3) {
    const ctx = this.context;
    ctx.beginPath();
    ctx.moveTo(p0.x, p0.y);
    ctx.bezierCurveTo(p1.x, p1.y, p2.x, p2.y, p3.x, p3.y);
    ctx.stroke();
  }

  /**
   * 
   * @param {string} text 
   * @param {Vec2} pos 
   */
  drawText(text, pos, baseline, align) {
    const ctx = this.context;
    ctx.textBaseline = baseline;
    ctx.textAlign = align;
    ctx.fillText(text, pos.x, pos.y);
  }

  setStrokeColor(color) {
    this.context.strokeStyle = color;
  }

  setFillColor(color) {
    this.context.fillStyle = color;
  }

  /**
   * 
   * @param {HTMLImageElement} img
   * @param {number} x
   * @param {number} y
   * @param {number} width
   * @param {number} height
   */
  drawImage(img, x , y, width, height) {
    this.context.drawImage(img, x, y, width, height)
  }
}

