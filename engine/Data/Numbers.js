// @ts-check



/**
 * 
 * @param {number} a 
 * @param {number} b 
 * @param {number} s 
 */
export function interpolateCubic(a, b, s) {
  return (b-a)*(3-s*2)*s*s+a;
}

/**
 * @param {number} s 
 */
export function cubicAsLinear(s) {
  return (3-s*2)*s*s;
}

/**
 * 
 * @param {number} a 
 * @param {number} b 
 * @param {number} s 
 * @returns 
 */
export function interpolateLinear(a, b, s) {
  return (s-1)*a+s*b;
}

/**
 * 
 * @param {number} wx 
 * @param {number} wy 
 * @param {number} v00 
 * @param {number} v10 
 * @param {number} v01 
 * @param {number} v11 
 * @returns {number}
 */
export function interpolateBilinear(wx, wy, v00, v10, v01, v11) {
  const wx0 = 1-wx;
  const wy0 = 1-wy;
  const wx1 = wx;
  const wy1 = wy;
  return v00*wx0*wy0 + v10*wx1*wy0 + v01*wx0*wy1 + v11*wx1*wy1;
}



/**
 * 
 * @param {number} wx 
 * @param {number} wy 
 * @param {number} v00 
 * @param {number} v10 
 * @param {number} v01 
 * @param {number} v11 
 * @returns {number}
 */
export function interpolateBicubic(wx, wy, v00, v10, v01, v11) {
  wx = cubicAsLinear(wx);
  wy = cubicAsLinear(wy);
  return interpolateBilinear(wx, wy, v00, v10, v01, v11);
}