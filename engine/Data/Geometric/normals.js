// @ts-check

import StaticMeshData, { triangles } from "../Geometric/Meshes/StaticMesh.js";
import { Vec3 } from "../../Data/LinearAlgebra.js";

/**
 * replaces normal and tangents data
 * @param {StaticMeshData} staticMeshData
 * @returns {StaticMeshData}
 */
export function generateNormals(staticMeshData) {
  const n_indices = staticMeshData.indices.length;
  const n = new Vec3();
  const v1 = new Vec3();
  const v2 = new Vec3();

  const iterable = (function* () {
    for (const triangle of triangles(staticMeshData)) {

      const p0 = triangle.v0.pos;
      const p1 = triangle.v1.pos;
      const p2 = triangle.v2.pos;
  
      Vec3.subtract(v1, p1, p0);
      Vec3.subtract(v2, p2, p0);
      Vec3.cross(n, v1, v2);
      Vec3.normalize(n, n);

      Vec3.copy(triangle.v0.nor, n);
      Vec3.copy(triangle.v1.nor, n);
      Vec3.copy(triangle.v2.nor, n);

      yield triangle;
    }
  })();
  
  return StaticMeshData.fromTriangles(n_indices, iterable)
}

/**
 * replaces tangent normal data according to MikkTSpace
 * @param {StaticMeshData} staticMeshData 
 */
export function generateMikkTangents(staticMeshData) {
  console.warn("Fake placeholder MikkTSpace tangents used!");
  generateSillyTangents(staticMeshData);
  
}

/**
 * 
 * @param {StaticMeshData} staticMeshData 
 */
export function generateSillyTangents(staticMeshData) {
  const normals = staticMeshData.normals;
  const tangents = staticMeshData.tangents = new Float32Array(normals.length/3*4);
  let t = new Vec3();
  let up = Vec3.create(0, 1, 0);
  let n = new Vec3();
  for (let ii = 0; ii < normals.length/3; ii++) {
    Vec3.set(n, normals[ii*3], normals[ii*3+1], normals[ii*3+2]);
    Vec3.cross(t, n, up);
    Vec3.normalize(t, t);
    tangents[ii*4+0] = t[0];
    tangents[ii*4+1] = t[1];
    tangents[ii*4+2] = t[2];
    tangents[ii*4+3] = 1;
  }
}