// @ts-check

import { perlinNoiseMap } from "../../Algorithms/Random/Noise/Perlin.js";
import { interpolateBicubic, interpolateBilinear } from "../Numbers.js";
import { Vec2, Vec3 } from "../../Data/LinearAlgebra.js"
import ivec2 from "../LinearAlgebra/ivec2.js";
import { clamp, squareArrayIndex } from "../../utils.js";

export class Heightmap {
  /** @readonly @type {Float32Array} */
  points;
  /** @readonly @type {number} */
  sqrtPointCount;
  /** @readonly @type {number} */
  worldSize;
  /** @readonly @type {number} */
  maxHeight;

  /**
   * 
   * @param {Float32Array} points 
   * @param {number} worldSize
   * @param {number} maxHeight
   */
  constructor(points, worldSize, maxHeight) {
    this.points = points;
    this.sqrtPointCount = Math.sqrt(points.length);
    this.worldSize = worldSize;
    this.maxHeight = maxHeight;
  }

  /**
   * 
   * @param {number} sqrtPointCount 
   * @param {number} worldSize 
   * @param {number} maxHeight 
   * @param {number} [octaveCount]
   * @returns 
   */
  static random(sqrtPointCount, worldSize, maxHeight, octaveCount = 8) {
    const points = new Float32Array(sqrtPointCount*sqrtPointCount);
    for (let ii = 0; ii < octaveCount; ++ii) {
      const frequency = Math.pow(2, ii);
      const octave = perlinNoiseMap(sqrtPointCount, frequency);
      for (let jj = 0; jj < octave.length; ++jj) {
        points[jj] += octave[jj] / frequency;
      }
    }

    return new Heightmap(points, worldSize, maxHeight);
  }

  get cellSize() {
    return this.worldSize / this.sqrtPointCount;
  }

  get cellArea() {
    return this.worldSize*this.worldSize / (this.sqrtPointCount * this.sqrtPointCount);
  }

  get cellVolume() {
    const cellWorldSize = this.worldSize / this.sqrtPointCount;
    return cellWorldSize*cellWorldSize*this.maxHeight; 
  }
}

/**
 * height at a specific pixel-coordinate
 * @param {Heightmap} heightmap
 * @param {ivec2} ip 
 */
export function pixelHeight(heightmap, ip) {
  return pixelHeight2(heightmap, ip[0], ip[1]);
}

/**
 * @param {Heightmap} heightmap
 * @param {number} x 
 * @param {number} z
 * @returns 
 */
export function pixelHeight2(heightmap, x, z) {
  const maxIndex = heightmap.sqrtPointCount-1;
  x = clamp(x, 0, maxIndex);
  z = clamp(z, 0, maxIndex)
  return heightmap.points[squareArrayIndex(heightmap.sqrtPointCount, x, z)] * heightmap.maxHeight;
}

/**
 * Slope of specific pixel, based on heights of immediately sorrounding pixels in a plus-pattern (+).
 * Edges are clamped.
 * @param {Vec2} out 
 * @param {Heightmap} heightmap
 * @param {ivec2} ip  
 */
export function pixelSlope(out, heightmap, ip) {
  pixelSlope2(out, heightmap, ip[0], ip[1]);
}

/**
 * Slope of specific pixel, based on heights of immediately sorrounding pixels in a plus-pattern (+).
 * Edges are clamped.
 * @param {Vec2} out 
 * @param {Heightmap} heightmap
 * @param {number} ix
 * @param {number} iz
 */
export function pixelSlope2(out, heightmap, ix, iz) {
  const points = heightmap.points;
  const sqrtPointCount = heightmap.sqrtPointCount;
  const ix_pre = Math.max(ix-1, 0);
  const ix_post = Math.min(ix+1, sqrtPointCount-1);
  const iz_pre = Math.max(iz-1, 0);
  const iz_post = Math.min(iz+1, sqrtPointCount-1);

  const h_west = points[squareArrayIndex(sqrtPointCount, ix_pre, iz)];
  const h_east = points[squareArrayIndex(sqrtPointCount, ix_post, iz)];
  const h_south = points[squareArrayIndex(sqrtPointCount, ix, iz_pre)];
  const h_north = points[squareArrayIndex(sqrtPointCount, ix, iz_post)];
  
  const dLdx = (h_west - h_east) / 2;
  const dLdz = (h_south - h_north) / 2;

  const slopeScale = heightmap.maxHeight / heightmap.cellSize;
  out[0] = dLdx * slopeScale;
  out[1] = dLdz * slopeScale;
}


/**
 * converts [0,1] to pixel indices, where 0,0 is the center of pixel 0,0 and 1,1 is the center of pixel (x,z)
 * @param {ivec2} out 
 * @param {Heightmap} heightmap
 * @param {Vec2} v 
 */
export function nearestIndex(out, heightmap, v) {
  const sqrtPointCount = heightmap.sqrtPointCount;
  const maxIndex = sqrtPointCount-1;
  out[0] = Math.round(v[0]*maxIndex);
  out[1] = Math.round(v[1]*maxIndex);
}

export const nearestHeight = (() => {
  const ip = ivec2.createZeroes();
  
  /**
   * @param {Heightmap} heightmap
   * @param {Vec2} p 
   * @returns {number}
   */
  return (heightmap, p) => {
    nearestIndex(ip, heightmap, p);
    return pixelHeight(heightmap, ip);
  }
})();

export const nearestSlope = (() => {
  const ip = ivec2.createZeroes();
  /**
   * 
   * @param {Vec2} out 
   * @param {Heightmap} heightmap
   * @param {Vec2} p 
   */
  return (out, heightmap, p) => {
    nearestIndex(ip, heightmap, p);
    pixelSlope(out, heightmap, ip);
  };
})();

export const exactSlope = (() => {
  const ip = ivec2.createZeroes();
  const w = new Vec2();;

  /**
   * @param {Vec2} out
   * @param {Heightmap} heightmap
   * @param {Vec2} p
   */
  return function(out, heightmap, p) {
    bilinearIndices(ip, w, heightmap, p);

    const h00 = pixelHeight2(heightmap, ip[0], ip[1]);
    const h10 = pixelHeight2(heightmap, ip[0]+1, ip[1]);
    const h01 = pixelHeight2(heightmap, ip[0], ip[1]+1);
    const h11 = pixelHeight2(heightmap, ip[0]+1, ip[1]+1);    

    out[0]=h00+h01-h10-h11;
    out[1]=h00+h10-h01-h11;
  };
})();

/**
 * 
 * @param {ivec2} out_p 
 * @param {Vec2} out_w 
 * @param {Heightmap} heightmap
 * @param {Vec2} p 
 */
export function bilinearIndices(out_p, out_w, heightmap, p) {
  const sqrtPointCount = heightmap.sqrtPointCount;
  const maxIndex0 = sqrtPointCount-2;
  const maxIndex1 = sqrtPointCount-1;
  const fx = p[0] * maxIndex1;
  const fz = p[1] * maxIndex1;
  const ix0 = Math.min(Math.floor(fx), maxIndex0);
  const iz0 = Math.min(Math.floor(fz), maxIndex0);

  out_p[0] = ix0;
  out_p[1] = iz0;

  out_w[0] = fx-ix0;
  out_w[1] = fz-iz0;
}

export const bilinearHeight = (() => {
  const ip = ivec2.createZeroes();
  const w = new Vec2();
  /**
   * @param {Heightmap} heightmap
   * @param {Vec2} p [0,1]
   * @returns {number}
   */
  return (heightmap, p) => {
    bilinearIndices(ip, w, heightmap, p);

    const h00 = pixelHeight2(heightmap, ip[0], ip[1]);
    const h10 = pixelHeight2(heightmap, ip[0]+1, ip[1]);
    const h01 = pixelHeight2(heightmap, ip[0], ip[1]+1);
    const h11 = pixelHeight2(heightmap, ip[0]+1, ip[1]+1);
    
    return interpolateBilinear(w[0], w[1], h00, h10, h01, h11);
  }
})();

export const bilinearSlope = (() => {
  const ip = ivec2.createZeroes();
  const w = new Vec2();
  const slope00 = new Vec2();
  const slope10 = new Vec2();
  const slope01 = new Vec2();
  const slope11 = new Vec2();
  
  /**
   * @param {Vec2} out
   * @param {Heightmap} heightmap
   * @param {Vec2} p 
   */
  return (out, heightmap, p) => {
    bilinearIndices(ip, w, heightmap, p);

    pixelSlope2(slope00, heightmap, ip[0], ip[1]);
    pixelSlope2(slope10, heightmap, ip[0]+1, ip[1]);
    pixelSlope2(slope01, heightmap, ip[0], ip[1]+1);
    pixelSlope2(slope11, heightmap, ip[0]+1, ip[1]+1);

    Vec2.interpolateBilinear(out, w[0], w[1], slope00, slope10, slope01, slope11);
  };
})();

export const bicubicHeight = (() => {
  const ip = ivec2.createZeroes();
  const w = new Vec2();
  /**
   * @param {Heightmap} heightmap
   * @param {Vec2} p [0,1]
   * @returns {number}
   */
  return (heightmap, p) => {
    bilinearIndices(ip, w, heightmap, p);

    const h00 = pixelHeight2(heightmap, ip[0], ip[1]);
    const h10 = pixelHeight2(heightmap, ip[0]+1, ip[1]);
    const h01 = pixelHeight2(heightmap, ip[0], ip[1]+1);
    const h11 = pixelHeight2(heightmap, ip[0]+1, ip[1]+1);
    
    return interpolateBicubic(w[0], w[1], h00, h10, h01, h11);
  }
})();

export const bicubicSlope = (() => {
  const ip = ivec2.createZeroes();
  const w = new Vec2();
  const dh00 = new Vec2();
  const dh10 = new Vec2();
  const dh01 = new Vec2();
  const dh11 = new Vec2();

  /**
   * @param {Vec2} out
   * @param {Heightmap} heightmap
   * @param {Vec2} p 
   */
  return (out, heightmap, p) => {
    bilinearIndices(ip, w, heightmap, p);

    pixelSlope2(dh00, heightmap, ip[0], ip[1]);
    pixelSlope2(dh10, heightmap, ip[0]+1, ip[1]);
    pixelSlope2(dh01, heightmap, ip[0], ip[1]+1);
    pixelSlope2(dh11, heightmap, ip[0]+1, ip[1]+1);
    
    Vec2.interpolateBicubic(out, w[0], w[1], dh00, dh10, dh01, dh11);
  };
})();

/**
 * @param {Vec3} out
 * @param {Heightmap} heightmap
 * @param {Vec2} a
 */
export function worldPosition(out, heightmap, a) {
  const worldSize = heightmap.worldSize;
  out[0] = a[0]*worldSize;
  out[1] = bilinearHeight(heightmap, a);
  out[2] = a[1]*worldSize;
}