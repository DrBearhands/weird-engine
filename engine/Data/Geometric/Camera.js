// @ts-check
import { Vec3, Mat4 } from "../../Data/LinearAlgebra.js";

import { UP } from "./constants.js";

export default class Camera {
  /** @type {Mat4} */
  #viewMatrix;        //World Space 3D -> Camera Space 3D
  /** @type {Mat4} */
  #projectionMatrix;  //Camera Space 3D -> Clip Space 2D
  /** @type {number} */
  #vfov = Math.PI*0.25;

  constructor(vfov = Math.PI*0.25) {
    this.#vfov = vfov;
    this.#viewMatrix = Mat4.createIdentity();
    const P = this.#projectionMatrix = Mat4.createIdentity();
    Mat4.perspectiveProjection(P, this.#vfov, 1, 0.1, Infinity);
  }

  /**
   * 
   * @param {number} r 
   */
  setAspectRatio(r) {
    Mat4.perspectiveProjection(this.#projectionMatrix, this.#vfov, r, 0.1, Infinity);
  }

  /**
   * @param {Vec3} eye 
   * @param {Vec3} target 
   */
  lookAt(eye, target) {
    Mat4.lookAt(this.#viewMatrix, eye, target, UP);
  }

  /**
   * camera matrix: World Space 3D -> Clip Space 2D
   * @param {Mat4} out 
   */
  getCameraMatrix(out) {
    Mat4.multiply(out, this.#projectionMatrix, this.#viewMatrix);
  }

  /**
   * 
   * @param {Mat4} out 
   */
  getViewMatrix(out) {
    Mat4.copy(out, this.#viewMatrix);
  }

  /**
   * 
   * @param {Vec3} out 
   */
  getPosition(out) {
    const transform = new Float32Array(16);
    Mat4.invert(transform, this.#viewMatrix);
    Mat4.multiplyV3(out, transform, new Float32Array(3));
  }

  /**
   * @param {Mat4} a 
   */
  setTransform(a) {
    Mat4.invert(this.#viewMatrix, a);
  }
}