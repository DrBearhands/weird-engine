import { Vec3 } from "../../Data/LinearAlgebra.js";

export const UP = Vec3.create(0, 1, 0);