// @ts-check

import DefaultWeakMap from "../../Datastructures/DefaultWeakMap.js";
import { nameGenerator, UniqueVarNameGenerator } from "../../../Algorithms/UniqueVarNameGenerator.js";
import { DataflowNode } from "../../DataflowGraphs/DataflowGraph.js";
import * as eDSL from "./MaterialEDSL.js"
import { Vec2 } from "../../../Data/LinearAlgebra.js";
import Rectangle from "../../DataflowGraphs/Rectangle.js";
import { openFile } from "../../../IO/openFile.js";

const default_image_res = await fetch(import.meta.resolve("./defaultImage.png"));
const default_image_blob = await default_image_res.blob();
const default_image_bitmap = await createImageBitmap(default_image_blob, { colorSpaceConversion: 'none' });

/**
 * @todo: enforce accumulate functions to set an expression (e.g. through return type)
 * @todo: change overloaded function type calculation to use type variables rather than ungodly complet type getters
 */
/**
 * @typedef {import("./MaterialEDSL.js").Expression} Expression 
 */
/**
 * @typedef {import("./MaterialEDSL.js").Statement} Statement 
 */

/**
 * @template T
 * @typedef { import("../../DataflowGraphs/GraphCfg.js").NodeCfg<T>} NodeCfg
 */
/**
 * @template T
 * @typedef {NodeCfg<{materialData : MaterialNodeTypeData<T>} & T>} MaterialNode
 */

export class PBR_output {
  /** @type {Expression} */
  normal;
  /** @type {Expression} */
  albedo;
  /** @type {Expression} */
  roughness;
  /** @type {Expression} */
  metallic;
  /** @type {Expression} */
  ambientOcclusion;
}

/**
 * @template T
 */
export class MaterialBuilder {
  /** @type {eDSL.MaterialData<PBR_output>} */
  data;
  /** @type {DefaultWeakMap<MaterialNode<T>, Expression[]>} */
  outputExpressions;

  nameGenerator = new UniqueVarNameGenerator();
  
  constructor() {
    this.data = new eDSL.MaterialData();
    this.outputExpressions = new DefaultWeakMap((node) => new Array(node.outputs.length));
  }
}

/**
 * @template T
 * @callback typeConstraintCbk
 * @param {MaterialNode<T>} node
 * @param {number} connectorIdx
 * @param {Set<MaterialNode<T>>} [visitedNodes]
 * @returns {eDSL.TypeConstraint} 
 */

/**
 * @template T
 */
class MaterialNodeTypeData {
  /** @type {typeConstraintCbk<T>} */
  inputTypeConstraint;
  /** @type {typeConstraintCbk<T>} */
  outputTypeConstraint;
  /** @type {(builder: MaterialBuilder, node: MaterialNode<T>) => void} */
  accumulate;
}

/**
 * 
 * @param {{label : string}[]} inArr 
 * @returns {{label : string}[]}
 */
function onlyLabels(inArr) {
  return inArr.map(({label}) => { return {label};});
}


/**
 * @template T
 * @param {MaterialBuilder} builder
 * @param {MaterialNode<T>} node 
 * @param {Expression[]} defaults
 */
function getInputExpressions(builder, node, defaults) {
  return node.inputs.map(({connection}, idx) => {
    if (connection) {
      return builder.outputExpressions.get(connection.node)[connection.targetIdx];
    } else {
      return defaults[idx];
    }
  });
}

/**
 * @param {eDSL.Type} type
 * @returns {Expression}
 */
function zeroExpressionForType(type) {
  switch (type) {
    case "scalar":
      return {
        tag: 'constant',
        value: [0]
      };
    case "vec2f":
      return {
        tag: 'constant',
        value: [0, 0]
      };
    case "vec3f":
      return {
        tag: 'constant',
        value: [0, 0, 0]
      };
    case "vec4f":
      return {
        tag: 'constant',
        value: [0, 0, 0, 0]
      };
    default:
      throw `No zero value for type ${type}`;
  }
}

/**
 * @template T
 * @param {{
 *   label: string, 
 *   inputs: {label:string, type:eDSL.Type}[], 
 *   outputs: {label:string, type:eDSL.Type}[], 
 *   accumulate: (builder: MaterialBuilder, node: MaterialNode<T>) => void
 * }} param0 
 * @returns {(t: T) => MaterialNode<T>}
 */
function simplyTypedMaterialNode({label, inputs, outputs, accumulate}) {
  const nodeInputs = onlyLabels(inputs)
  const nodeOutputs = onlyLabels(outputs);

  /** @type {MaterialNodeTypeData<T>} */
  const materialData = {
    accumulate,
    inputTypeConstraint: (node, inputIdx) => {
      return new eDSL.TypeConstraint([inputs[inputIdx].type]);
    },
    outputTypeConstraint: (node, outputIdx) => {
      return new eDSL.TypeConstraint([outputs[outputIdx].type]);
    }
  }

  return function(t) {
    return new DataflowNode({
      label,
      inputs: nodeInputs,
      outputs: nodeOutputs,
      nodeData: {
        ...t,
        materialData
      }
    });
  }
}

/**
 * @template T
 * @param {string} label 
 * @param {string} functionName
 * @param {number} numArgs 
 * @returns {(t:T) => MaterialNode<T>}
 */
function trigonometryDescriptor(label, functionName, numArgs) {
  
  /**
   * @type {Array<{label: string, type: eDSL.Type}>}
   */
  const inputs = new Array(numArgs).fill({label: "", type: "scalar"});
  const outputs = [ { label : "", type: "scalar" } ];

  /** @type {MaterialNodeTypeData<T>} */
  const materialData = {
    inputTypeConstraint: () => {
      return eDSL.scalar;
    },
    outputTypeConstraint: () => {
      return eDSL.scalar;
    },
    accumulate: (builder, node) => {
      const varName = builder.nameGenerator.generate("v");
      const zero = zeroExpressionForType('scalar');
      const args = getInputExpressions(builder, node, inputs.map(() => zero));
      builder.data.statements.push({
        tag: 'assignment',
        varName,
        expression: {
          tag: 'functionCall',
          functionName,
          arguments: args
        }
      });
      builder.outputExpressions.get(node)[0] = {
        tag: "variable",
        varName
      }
    }
  }

  return (t) => {
    return new DataflowNode({
      label,
      inputs: onlyLabels(inputs),
      outputs: onlyLabels(outputs),
      nodeData: {
        ...t,
        materialData
      }
    });
  }
}

/**
 * 
 * @param {import("./MaterialEDSL.js").Type} uniformType
 * @returns {import("./MaterialEDSL.js").UniformInformation}
 */
function uniformInfoFromType(uniformType) {
  switch (uniformType) {
    case "scalar":
      return {
        type: "scalar",
        defaultValue: [0]
      }
    case "vec2f":
      return {
        type: "vec2f",
        defaultValue: [0, 0]
      }
    case "vec3f":
      return {
        type: "vec3f",
        defaultValue: [0, 0, 0]
      }
    case "vec4f":
      return {
        type: "vec4f",
        defaultValue: [0, 0, 0, 0]
      }
    case "mat2":
      return {
        type: "mat2",
        defaultValue: [1, 0, 0, 1]
      }
    case "mat3":
      return {
        type: "mat3",
        defaultValue: [1, 0, 0, 0, 1, 0, 0, 0, 1]
      }
    case "mat4":
      return {
        type: "mat4",
        defaultValue: [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1]
      }
    case "texture2D":
    case "textureCube":
    {
      const img_width = 128;
      const img_height = 128;
      
      const image = new Image(img_width, img_height);
      image.src = import.meta.resolve("./defaultImage.png");
      return {
        type: uniformType,
        defaultValue: default_image_bitmap
      }
    } 
  }
}

/**
 * @todo split between simple uniforms and textures
 * @template T
 * @param {import("./MaterialEDSL.js").TensorType} uniformType 
 * @returns {(t:T) => MaterialNode<T>}
 */
function uniformMaterialNode(uniformType) {
  
  /**
   * @type {{label: string, type: eDSL.Type}[]}
   */
  const inputs = [];
  const outputs = [{ label: "", type: uniformType }];
  const nodeInputs = onlyLabels(inputs);
  const nodeOutputs = onlyLabels(outputs);
  const label = `Uniform ${uniformType}`;

  const arrayLength = eDSL.numScalars(uniformType);
  const defaultValues = new Array();


  /** 
   * @param {{name: string, uInfo: import("./MaterialEDSL.js").UniformInformation}} uniformCfg
   * @returns {MaterialNodeTypeData<T>} 
   */
  const materialData = (uniformCfg) => {
    return {
      accumulate: (builder, node) => {
        const uniformName = uniformCfg.name;
        builder.data.uniforms[uniformName] = uniformCfg.uInfo;
        builder.outputExpressions.get(node)[0] = {
          tag: "variable",
          varName: uniformName
        }
      },
      inputTypeConstraint: (node, inputIdx) => {
        return new eDSL.TypeConstraint([inputs[inputIdx].type]);
      },
      outputTypeConstraint: (node, outputIdx) => {
        return new eDSL.TypeConstraint([outputs[outputIdx].type]);
      }
    }
  }

  return function(t) {
    const uniformCfg = {
      name: nameGenerator.generate("u"),
      uInfo: uniformInfoFromType(uniformType)
    };

    /** @type {number} */
    const numValues = uniformCfg.uInfo.defaultValue.length;
    const lineHeight = 15;
    return new DataflowNode({
      label,
      inputs: nodeInputs,
      outputs: nodeOutputs,
      nodeData: {
        ...t,
        materialData: materialData(uniformCfg),
        config: {
          label: "Name",
          type: "string",
          extraPadding: () => Vec2.create(100, lineHeight*(1+numValues)),
          view: (renderer, innerRect) => {
            const top = Rectangle.topOf(innerRect);
            renderer.drawText(`${uniformCfg.name}:`, top, "top", "center");
            for (let ii = 0; ii < numValues; ++ii) {
              const position = Vec2.create(top.x, top.y+lineHeight*(ii+1));
              renderer.drawText(`${uniformCfg.uInfo.defaultValue[ii]}`, position, "top", "center");
            }
          },
          onclick: (pos) => {
            const promptResult = prompt("enter new value");
            const index = Math.floor(pos.y / lineHeight)-1;
            if (promptResult !== null) {
              
              const newValue = Number.parseFloat(promptResult);
              uniformCfg.uInfo.defaultValue[index] = newValue;
            }
          }
        }
      }
    });
  }
}

/**
 * @template T
 * @param {'texture2D' | 'textureCube'} uniformType 
 * @returns {(t:T) => MaterialNode<T>}
 */
function uniformTextureMaterialNode(uniformType) {
  /**
   * @type {{label: string, type: eDSL.Type}[]}
   */
  const inputs = (() => {
    return [ { label : "uvs", type: eDSL.coordinatesForTexture(uniformType)}]
  })();
  /**
   * @type {{label: string, type: eDSL.Type}[]}
   */
  const outputs = [{ label: "color", type: 'vec4f'}];
  const nodeInputs = onlyLabels(inputs);
  const nodeOutputs = onlyLabels(outputs);
  const label = `${uniformType}`;


  /** 
   * @param {{name: string, uInfo: import("./MaterialEDSL.js").UniformInformation}} uniformCfg
   * @returns {MaterialNodeTypeData<T>} 
   */
  const materialData = (uniformCfg) => {
    return {
      
      accumulate: (builder, node) => {
        const uniformName = uniformCfg.name;
        builder.data.uniforms[uniformName] = uniformCfg.uInfo;
        const varName = builder.nameGenerator.generate();
        const inputExpressions = getInputExpressions(builder, node, [{
          tag: "variable",
          varName: "texCoords"
        }]);
        
        builder.data.statements.push({
          tag: 'assignment',
          varName,
          expression: {
            tag: "textureSample2D",
            textureName: uniformName,
            texCoords: inputExpressions[0]
          }
        });

        builder.outputExpressions.get(node)[0] = {
          tag: "variable",
          varName
        }
        
      },
      inputTypeConstraint: (node, inputIdx) => {
        return new eDSL.TypeConstraint([inputs[inputIdx].type]);
      },
      outputTypeConstraint: (node, outputIdx) => {
        return new eDSL.TypeConstraint([outputs[outputIdx].type]);
      }
    }
  }
  return function(t) {
    const img_width = 128;
    const img_height = 128;
    const img_pad = 10;
    const uniformCfg = {
      name: nameGenerator.generate("uTexture"),
      uInfo: uniformInfoFromType(uniformType)
    }
    return new DataflowNode({
      label,
      inputs: nodeInputs,
      outputs: nodeOutputs,
      nodeData: {
        ...t,
        materialData: materialData(uniformCfg),
        config: {
          label: "Name",
          type: "string",
          extraPadding: () => Vec2.create(img_width+img_pad*2, img_height+img_pad*2),
          view: (renderer, innerRect) => {
            renderer.drawImage(uniformCfg.uInfo.defaultValue, innerRect.start.x+img_pad, innerRect.start.y+img_pad, img_width, img_height);
          },
          onclick: () => {
            openFile((file) => {
              createImageBitmap(file, { colorSpaceConversion: 'none' }).then(image => {
                uniformCfg.uInfo.defaultValue = image;
              });
            })
          }
        }
      }
    });
  }
}

/**
 * @template T
 * @param {string} label 
 * @param {number} numArgs
 * @param {(expressions : Expression[]) => Expression} createExpression 
 * @returns {(t:T) => MaterialNode<T>}
 */
function overloadedMaterialNode(label, numArgs, createExpression) {

  /**
   * @param {MaterialNode<T>} node 
   * @param {number} excludeInput 
   * @param {Set<MaterialNode<T>>} visitedNodes 
   * @returns 
   */
  function getType(node, excludeInput, visitedNodes = new Set()) {
    visitedNodes.add(node);
    let constraint = new eDSL.TypeConstraint(['scalar', 'vec2f', 'vec3f', 'vec4f']);
    for (let ii = 0; ii < node.inputs.length; ++ii) {
      if (ii === excludeInput) continue;
      const connection = node.inputs[ii].connection;
      if (connection) {
        const connectedNode = connection.node;
        if (!visitedNodes.has(connectedNode)) {
          const recursiveConstraint = connectedNode.nodeData.materialData.outputTypeConstraint(connectedNode, connection.targetIdx, visitedNodes);
          constraint = eDSL.TypeConstraint.intersect(constraint, recursiveConstraint);
        }
      }
    }
    for (let ii = 0; ii < node.outputs.length; ++ii) {
      const {connections} = node.outputs[ii];
      for (const connection of connections.values()) {
        const connectedNode = connection.node;
        if (!visitedNodes.has(connectedNode)) {
          const recursiveConstraint = connectedNode.nodeData.materialData.inputTypeConstraint(connectedNode, connection.targetIdx, visitedNodes);
          constraint = eDSL.TypeConstraint.intersect(constraint, recursiveConstraint);
        }
      }
    }
    return constraint;
  }
  /**
   * @returns {MaterialNodeTypeData<T>}
   */
  const materialData = {
    inputTypeConstraint: (node, inputIdx, visitedNodes = new Set()) => {
      return getType(node, inputIdx, visitedNodes);
    },
    outputTypeConstraint: (node, {}, visitedNodes = new Set()) => {
      return getType(node, -1, visitedNodes);
    },
    accumulate: (/** @type {MaterialBuilder} */ builder, /** @type {MaterialNode<T>} */ node) => {
      const varName = builder.nameGenerator.generate();
      const zeroes = zeroExpressionForType(getType(node, -1).acceptList[0]);
      const inputExpressions = getInputExpressions(builder, node, [zeroes, zeroes]);
      builder.outputExpressions.get(node)[0] = {
        tag: 'variable',
        varName
      }
      builder.data.statements.push({
        tag: 'assignment',
        varName,
        expression: createExpression(inputExpressions)
      });
    }
  }
  return (t) => {
    const innerType = new eDSL.TypeConstraint(['scalar', 'vec2f', 'vec3f', 'vec4f']);
    return new DataflowNode({
      label,
      inputs: new Array(numArgs).fill({label: ""}),
      outputs: [{label: ""}],
      nodeData: { ...t, materialData }
    })
  };
}

/**
 * @template T
 * @param {string} label
 * @param {'+' | '-' | '*' | '/'} operator
 * @returns {(t:T) => MaterialNode<T>}
 */
function binaryOperatorMaterialNode(label, operator) {
  return overloadedMaterialNode(label, 2, ([leftSide, rightSide]) => {
    return {
      tag: 'operator',
      operator,
      leftSide,
      rightSide
    };
  });
}

/**
 * @template T
 * @param {string} label
 * @param {string} functionName
 * @param {number} numArgs
 * @returns {(t:T) => MaterialNode<T>}
 */
function overloadedFunctionMaterialNode(label, functionName, numArgs) {
  return overloadedMaterialNode(label, numArgs, (args) => {
    return {
      tag: 'functionCall',
      functionName,
      arguments: args
    }
  })
}

/**
 * @template T
 * @param {string} functionName 
 * @param {Expression[]} defaults 
 * @returns {(builder:MaterialBuilder, node:MaterialNode<T>) => void}
 */
function accumulateForFunction(functionName, defaults) {
  /**
   * @param {MaterialBuilder} builder 
   * @param {MaterialNode<T>} node 
   */
  return function(builder, node) {
    const varName = builder.nameGenerator.generate();
    builder.outputExpressions.get(node)[0] = {
      tag: "variable",
      varName
    }
    const args = getInputExpressions(builder, node, defaults);
    builder.data.statements.push({
      tag: 'assignment',
      varName,
      expression: {
        tag: 'functionCall',
        functionName,
        arguments: args
      }
    });
  }
}

export const pbr = (() => {
  /**
   * @type {{
   *   label: string,
   *   type: eDSL.Type,
   *   defaultExpression: Expression
   * }[]}
   */
  const inputs = [
    { label: "normal", type: "vec3f", defaultExpression: 
      { 
        tag: "functionCall", 
        functionName: "normalize",
        arguments: [{tag: "variable", varName: "normal"}] 
      } 
    },
    { label: "albedo", type: "vec3f", defaultExpression: { tag: "constant", value: [1,1,1] } },
    { label: "roughness", type: "scalar", defaultExpression: { tag: "constant", value: [0.9] } },
    { label: "metallic", type: "scalar", defaultExpression: { tag: "constant", value: [0] } },
    { label: "ambientOcclusion", type: "scalar", defaultExpression: { tag: "constant", value: [0] } },
  ];

  const defaults = inputs.map(({defaultExpression}) =>
    defaultExpression
  )

  const nodeInputs = onlyLabels(inputs);

  const materialData = {
    accumulate: (builder, node) => {
      const [normal, albedo, roughness, metallic, ambientOcclusion] = getInputExpressions(builder, node, defaults);

      builder.data.outputs = {
        normal, albedo, roughness, metallic, ambientOcclusion
      };
    },
    inputTypeConstraint: (node, inputIdx) => {
      return new eDSL.TypeConstraint([inputs[inputIdx].type]);
    },
    outputTypeConstraint: (node, outputIdx) => {
      return new eDSL.TypeConstraint([]);
    }
  }

  const img_pad = 10;
  const img_width = 128;
  const img_height = 128;

  return function(t) {
    let img;
    return new DataflowNode({
      label: "PBR Material",
      inputs: nodeInputs,
      outputs: [],
      nodeData: {
        ...t,
        materialData,
        setImage : (newimg) => { img = newimg },
        config: {
          label: "Name",
          type: "string",
          extraPadding: () => Vec2.create(img_width+img_pad*2, img_height+img_pad*2),
          view: (renderer, innerRect) => {
            if (img)
              renderer.drawImage(img, innerRect.start.x+img_pad, innerRect.start.y+img_pad, img_width, img_height);
          }
        }
      }
    });
  }
})();

export const attributes = (() => {
  /** @type {{label : string, type : eDSL.Type}[]} */
  const outputs = Object.entries(eDSL.MaterialData.attributes).map(([label, type]) => {
    return {
      label, type
    }
  });
  return simplyTypedMaterialNode({
    label: "Attributes",
    inputs: [],
    outputs: outputs,
    accumulate: (builder, node) => {
      for (let ii = 0; ii < outputs.length; ++ii) {
        const output = outputs[ii];
        builder.outputExpressions.get(node)[ii] = {
          tag: "variable",
          varName: output.label
        }
      }
    },
  });
})();


export const texture2D = uniformTextureMaterialNode("texture2D");

export const decompose = simplyTypedMaterialNode({
  label: "decompose",
  inputs: [
    { label: "vec4", type: "vec4f" },
  ],
  outputs: [
    { label: "rgb", type: "vec3f" },
    { label: "a", type: "scalar" }
  ],
  accumulate: (builder, node) => {
    const [inExpr] = getInputExpressions(builder, node, [{
      tag: 'constant',
      value: [0,0,0,0]
    }]);

    builder.outputExpressions.get(node)[0] = {
      tag: "selector",
      expression: inExpr,
      selector: "rgb"
    };
    builder.outputExpressions.get(node)[1] = {
      tag: "selector",
      expression: inExpr,
      selector: "a"
    };
  }
});

export const add = binaryOperatorMaterialNode("Add", '+');
export const multiply = binaryOperatorMaterialNode("Multiply", '*');
export const subtract = binaryOperatorMaterialNode("Subtract", '-');
export const divide = binaryOperatorMaterialNode("divide", '/');

export const sin = trigonometryDescriptor("sin", "sin", 1);
export const cos = trigonometryDescriptor("cos", "cos", 1);
export const tan = trigonometryDescriptor("tan", "tan", 1);
export const asin = trigonometryDescriptor("asin", "asin", 1);
export const acos = trigonometryDescriptor("acos", "acos", 1);
export const atan = trigonometryDescriptor("atan", "atan", 1);
export const atan2 = trigonometryDescriptor("atan2", "atan2", 2);

export const min = overloadedFunctionMaterialNode("min", "min", 2);
export const max = overloadedFunctionMaterialNode("max", "max", 2);
export const clamp = overloadedFunctionMaterialNode("clamp", "clamp", 3);



export const uniformScalar = uniformMaterialNode("scalar");
export const uniformVec2f = uniformMaterialNode("vec2f");
export const uniformVec3f = uniformMaterialNode("vec3f");
export const uniformVec4f = uniformMaterialNode("vec4f");


export const dot = simplyTypedMaterialNode({
  label : "dot",
  inputs: [
    { label : "", type: "vec3f" },
    { label : "", type: "vec3f" }
  ],
  outputs: [
    { label : "", type : "scalar" }
  ],
  accumulate: accumulateForFunction("dot", [zeroExpressionForType('vec3f'), zeroExpressionForType('vec3f')])
});

export const cross = simplyTypedMaterialNode({
  label: "cross",
  inputs: [
    { label : "", type : "vec3f" },
    { label : "", type : "vec3f" }
  ],
  outputs: [
    { label : "", type : "vec3f" }
  ],
  accumulate: accumulateForFunction("cross", [zeroExpressionForType('vec3f'), zeroExpressionForType('vec3f')])
});

export const normalize = simplyTypedMaterialNode({
  label : "normalize",
  inputs: [
    { label : "", type : "vec3f" },
  ],
  outputs: [
    {label : "", type: "vec3f" }
  ],
  accumulate: accumulateForFunction("normalize", [{tag:'constant', value: [0,1,0]}])
})


/**
 * @template T
 * @param {MaterialNode<T>} outputNode
 * @param {number} outputIdx
 * @param {MaterialNode<T>} inputNode
 * @param {number} inputIdx
 * @returns {boolean}
 */
export function tryConnect(outputNode, outputIdx, inputNode, inputIdx) {
  const outputTypeConstraint = outputNode.nodeData.materialData.outputTypeConstraint(outputNode, outputIdx);
  const inputTypeConstraint = inputNode.nodeData.materialData.inputTypeConstraint(inputNode, inputIdx);
  for (const acceptedFromType of outputTypeConstraint.acceptList ) {
    if (inputTypeConstraint.acceptList.includes(acceptedFromType)) {
      DataflowNode.connect(outputNode, outputIdx, inputNode, inputIdx);
      /** @todo update connect materialData */
      return true;
    }
  }
  return false;
}

/**
 * @param {MaterialBuilder} builder 
 */
export function generateAccumulate(builder) {
  /**
   * @template T
   * @param {MaterialNode<T>} node
   */
  return (node) => {
    node.nodeData.materialData.accumulate(builder, node);
  }
}

export const nodeGenerators = [
  pbr, attributes, 
  texture2D, 
  decompose, 
  add, multiply, subtract, divide, 
  sin, cos, tan, asin, acos, atan, atan2,
  min, max, clamp,
  uniformScalar, uniformVec2f, uniformVec3f, uniformVec4f,
  dot, cross, normalize
]