// @tss-check

/**
 * @typedef {"scalar" | "vec2f" | "vec3f" | "vec4f" | "mat2" | "mat3" | "mat4"} TensorType
 */
/**
 * @typedef {"texture2D" | "textureCube"} TextureType
 */
/**
 * @typedef {TensorType | TextureType} Type
 */

/**
 * @typedef {{
 *   tag: "assignment",
 *   varName : string,
 *   expression : Expression
 * } | {
 *   tag: "conditional",
 *   expression : Expression,
 *   trueCase : Statement[],
 *   falseCase: Statement[]
 * }} Statement
 */

/**
 * @todo use a regex for the selector field or something
 * @typedef {{
 *   tag: "functionCall",
 *   functionName : string,
 *   arguments : Expression[]
 * } | {
 *   tag: "operator",
 *   operator: ('+' | '-' | '*' | '/'),
 *   leftSide : ?Expression,
 *   rightSide : Expression
 * } | {
 *   tag: "variable",
 *   varName: string
 * } | {
 *   tag: "constant",
 *   value: [number] | [number, number] | [number, number, number] | [number,number,number,number]
 * } | {
 *   tag: "textureSample2D",
 *   textureName: string,
 *   texCoords: Expression
 * } | {
 *   tag: "selector",
 *   expression: Expression,
 *   selector: string
 * }} Expression
 */


/**
 * @typedef {{
 *   type: "scalar",
 *   defaultValue: [number]
 * } | {
 *   type: "vec2f",
 *   defaultValue: [number, number]
 * } | {
 *   type: "vec3f",
 *   defaultValue: [number, number, number]
 * } | {
 *   type: "vec4f",
 *   defaultValue: [number, number, number, number]
 * } | {
 *   type: "mat2",
 *   defaultValue: [number, number, number, number]
 * } | {
 *   type: "mat3",
 *   defaultValue: [number, number, number, number, number, number, number, number, number]
 * } | {
 *   type: "mat4",
 *   defaultValue: [number, number, number, number, number, number, number, number, number, number, number, number, number, number, number, number]
 * } | {
 *   type: "texture2D",
 *   defaultValue: ImageBitmap
 * } | {
 *   type: "textureCube",
 *   defaultValue: ImageBitmap
 * }} UniformInformation
 */

/**
 * @template T
 */
export class MaterialData {
  /** @type {Object.<string, UniformInformation>} */
  uniforms = {};
  /** @type {Object.<string, TensorType>} */
  static attributes = {
    position: "vec3f",
    normal: "vec3f",
    tangent: "vec4f",
    color: "vec4f",
    texCoords: "vec2f",
  };
  /** @type {Statement[]} */
  statements = [];
  /** @type {T} */
  outputs;
}



export class TypeConstraint {
  acceptList;
  /**
   * @param {Type[]} acceptList 
   */
  constructor(acceptList) {
    this.acceptList = acceptList;
  }

  /**
   * 
   * @param {TypeConstraint} a 
   * @param {TypeConstraint} b 
   */
  static intersect(a, b) {
    const newList = a.acceptList.filter(e => b.acceptList.includes(e));
    return new TypeConstraint(newList);
  }
}

/**
 * @param {TextureType} textureType 
 */
export function coordinatesForTexture(textureType) {
  switch (textureType) {
    case 'texture2D' :
      return 'vec2f';
    case 'textureCube':
      return 'vec3f';
  }
}

/**
 * 
 * @param {TensorType} tensorType 
 */
export function numScalars(tensorType) {
  switch (tensorType) {
    case "scalar":
      return 1;
    case "vec2f":
      return 2;
    case "vec3f":
      return 3;
    case "vec4f":
      return 4;
    case "mat2":
      return 4;
    case "mat3":
      return 9;
    case "mat4":
      return 16;
  }
}

export const scalar = new TypeConstraint(["scalar"]);
export const vec2f = new TypeConstraint(["vec2f"]);
export const vec3f = new TypeConstraint(["vec3f"]);
export const vec4f = new TypeConstraint(["vec4f"]);
export const anyType = new TypeConstraint(["scalar", "vec2f", "vec3f", "vec4f"]);