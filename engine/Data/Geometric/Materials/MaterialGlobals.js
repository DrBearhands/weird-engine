// @ts-check

/**
 * @typedef {import("../../../Material/MaterialEDSL").Type} Type
 */
export class MaterialGlobals {
  /** @type {Object.<string, Type>} */
  uniforms = {
    camera_MVP: "mat4",
    camera_position: "vec3f",
    irradiance_map: "textureCube",
    prefiltered_environment_map: "textureCube",
    BRDF_integration_map: "texture2D"
  };
  constructor() {}
}