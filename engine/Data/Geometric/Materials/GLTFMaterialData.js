//@ts-check

import { TextureSamplerData } from "../TextureData.js";

export class GLTFMaterialData {
  /** @type {TextureSamplerData} */ 
  baseColorTexture;
  /** @type {[number, number, number, number]} */
  baseColorFactor;
  /** @type {TextureSamplerData} */
  metallicRoughnessTexture;
  /** @type {number} */
  metallicFactor;
  /** @type {number} */
  roughnessFactor;
  /** @type {TextureSamplerData} */
  normalTexture;
  /** @type {number} */
  normalScale;
  /** @type {TextureSamplerData} */
  occlusionTexture;
  /** @type {number} */
  occlusionStrength;
  /** @type {TextureSamplerData} */
  emissiveTexture;
  /** @type {[number, number, number]} */
  emissiveFactor;
  /** @type {number} */
  alphaCutoff;
}