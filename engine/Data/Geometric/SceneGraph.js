// @ts-check

import Mat4 from "../../Data/LinearAlgebra/Mat4.js";

const identity = Mat4.createIdentity();

/**
 * @template Mesh
 */
export default class SceneNode {
  /** @type {SceneNode<Mesh>[]} */
  #children = [];
  /** @type {Mesh[]} */
  meshes = [];
  /** @type {Mat4} */
  #localTransform = Mat4.createIdentity();
  /** @type {Mat4} */
  #globalTransform = Mat4.createIdentity();
  /** @type {boolean} */
  #isDirty = true;

  /**
   * @param {Mat4} m
   */
  set localTransform(m) {
    Mat4.copy(this.#localTransform, m);
    this.#isDirty = true;
  }

  get localTransform() {
    const res = Mat4.createZeroes();
    Mat4.copy(res, this.#localTransform);
    return res;
  }

  /**
   * @param {(mesh : Mesh) => void} f 
   */
  traverse(f) {
    for (const mesh of this.meshes) {
      f(mesh);
    }
    for (const child of this.#children) {
      child.traverse(f);
    }
  }

  /**
   * @param {(mesh : Mesh, globalTransform : Mat4) => void} updateGPUBuffers
   * @param {Mat4} [parentGlobalTransform]
   * @param {boolean} [parentDirty]
   */
  updateGlobalTransforms(updateGPUBuffers, parentGlobalTransform = identity, parentDirty = false) {
    const globalTransform = this.#globalTransform;
    const dirty = this.#isDirty || parentDirty;
    if (dirty) {
      Mat4.multiply(globalTransform, this.localTransform, parentGlobalTransform);
      for (const mesh of this.meshes) {
        updateGPUBuffers(mesh, globalTransform)
      }
    }
    for(const child of this.#children) {
      child.updateGlobalTransforms(updateGPUBuffers, globalTransform, dirty);
    }
    this.#isDirty = false;
  }

  /**
   * @param {...SceneNode} children 
   */
  attachChild(...children) {
    this.#children.push(...children);
    for (const child of children)
      child.#isDirty = true;
  }

  *children() {
    for (const child of this.#children)
      yield child;
  }

  /**
   * @template Mesh2
   * @param {(mesh : Mesh) => Mesh2} f
   * @returns {SceneNode<Mesh2>}
   */
  map(f) {
    const that = new SceneNode();
    that.meshes = this.meshes.map(f);
    that.#children = this.#children.map(n => n.map(f));
    Mat4.copy(that.#localTransform, this.#localTransform);
    Mat4.copy(that.#globalTransform, this.#globalTransform);
    that.#isDirty = true;
    return that;
  }
}