// @ts-check

export class TextureData {
  /** 
   * @type { ImageBitmap | Uint8Array | Int8Array | Float16Array }
   */
  data;
  /** @type {1 | 2 | 4} */
  channels;
  /** @type {string | undefined} */
  name;

  /** @type {number} */
  xRes;
  /** @type {number} */
  yRes;
}

export class SamplerData {
  /** @type {"nearest" | "linear"} */
  magFilter;
  /** @type {"nearest" | "linear"} */
  minFilter;
  /** @type {"nearest" | "linear"} */
  mipmapFilter;
  /** @type {"clamp-to-edge" | "mirror-repeat" | "repeat"} */
  addressModeU;
  /** @type {"clamp-to-edge" | "mirror-repeat" | "repeat"} */
  addressModeV;
}


export class TextureSamplerData {
  /** @type {TextureData} */
  texture;
  /** @type {SamplerData} */
  sampler;
}