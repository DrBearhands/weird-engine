// @ts-check

import StaticMeshData from "./Meshes/StaticMesh.js";
import { Vec2, Vec3, Vec4 } from "../../Data/LinearAlgebra.js";

/**
 * @type {StaticMeshData}
 */
export const cube = (() => {
  const positions = [
    Vec3.create(-1, -1, -1),
    Vec3.create(1, 1, -1),
    Vec3.create(1, -1, -1),
    Vec3.create(-1,  1, -1),
    Vec3.create(1, -1, -1),
    Vec3.create(1,  1,  1),
    Vec3.create(1, -1,  1),
    Vec3.create(1,  1, -1),
    Vec3.create(1, -1,  1),
    Vec3.create(-1,  1,  1),
    Vec3.create(-1, -1,  1),
    Vec3.create(1,  1,  1),
    Vec3.create(-1, -1,  1),
    Vec3.create(-1,  1, -1),
    Vec3.create(-1, -1, -1),
    Vec3.create(-1,  1,  1),
    Vec3.create(-1,  1, -1),
    Vec3.create(1,  1,  1),
    Vec3.create(1,  1, -1),
    Vec3.create(-1,  1,  1),
    Vec3.create(1, -1, -1),
    Vec3.create(-1, -1,  1),
    Vec3.create(-1, -1, -1),
    Vec3.create(1, -1,  1),
  ];
  const texcoords = [
    Vec2.create(0,  0),
    Vec2.create(1,  1),
    Vec2.create(1,  0),
    Vec2.create(0,  1),
    Vec2.create(0,  0),
    Vec2.create(1,  1),
    Vec2.create(0,  1),
    Vec2.create(1,  0),
    Vec2.create(1,  0),
    Vec2.create(0,  1),
    Vec2.create(0,  0),
    Vec2.create(1,  1),
    Vec2.create(0,  1),
    Vec2.create(1,  0),
    Vec2.create(0,  0),
    Vec2.create(1,  1),
    Vec2.create(0,  0),
    Vec2.create(1,  1),
    Vec2.create(1,  0),
    Vec2.create(0,  1),
    Vec2.create(1,  0),
    Vec2.create(0,  1),
    Vec2.create(0,  0),
    Vec2.create(1,  1),
  ];
  const col = [Vec4.create(0,0,0,1)];

  const normals = [
    Vec3.create(0,0,-1),
    Vec3.create(1, 0, 0),
    Vec3.create(0,0,1),
    Vec3.create(-1,0,0),
    Vec3.create(0,1,0),
    Vec3.create(0, -1, 0)
  ];

  const tangents = [
    Vec3.create(1,0,0),
    Vec3.create(0,0,1),
    Vec3.create(-1,0,0),
    Vec3.create(0,0,-1),
    Vec3.create(1,0,0),
    Vec3.create(-1,0,0)
  ];

  function* triangleIterator() {
    for (let ii = 0; ii < 6; ++ii) {
      yield {
        v0: {
          pos: positions[0+ii*4],
          nor: normals[ii],
          tan: tangents[ii],
          tex: [texcoords[0+ii*4]],
          col
        },
        v1: {
          pos: positions[1+ii*4],
          nor: normals[ii],
          tan: tangents[ii],
          tex: [texcoords[1+ii*4]],
          col
        },
        v2: {
          pos: positions[2+ii*4],
          nor: normals[ii],
          tan: tangents[ii],
          tex: [texcoords[2+ii*4]],
          col
        }
      };
      yield {
        v0: {
          pos: positions[1+ii*4],
          nor: normals[ii],
          tan: tangents[ii],
          tex: [texcoords[1+ii*4]],
          col
        },
        v1: {
          pos: positions[0+ii*4],
          nor: normals[ii],
          tan: tangents[ii],
          tex: [texcoords[0+ii*4]],
          col
        },
        v2: {
          pos: positions[3+ii*4],
          nor: normals[ii],
          tan: tangents[ii],
          tex: [texcoords[3+ii*4]],
          col
        }
      };
    }
  }

  return StaticMeshData.fromTriangles(12, triangleIterator());
})();

/**
 * 
 * @param {number} numLat 
 * @param {number} numLon 
 */
export const createSphereGeometry = (numLat, numLon) => {
  const col = [Vec4.create(0,0,0,1)];
  function pointAt(lat, lon) {
    const latAngle0 = lat/numLat * Math.PI - Math.PI;
    const lonAngle0 = lon/numLon * 2 * Math.PI;

    const y = Math.cos(latAngle0);
    const x = Math.sin(latAngle0) * Math.cos(lonAngle0);
    const z = Math.sin(latAngle0) * Math.sin(lonAngle0);

    return Vec3.create(x, y, z);
  }
  const iterator = (function* () {
    for (let lat = 0; lat < numLat; ++lat) {
      for (let lon = 0; lon < numLon; ++lon) {
        const p0 = pointAt(lat, lon);
        const p1 = pointAt(lat+1, lon);
        const p2 = pointAt(lat+1, lon+1);
        const p3 = pointAt(lat, lon+1);
        
        const texMin = Vec2.create(1-lon/numLon - 0.25, 1-lat/numLat);
        const texMax = Vec2.create(1-(lon+1)/numLon - 0.25,1-(lat+1)/numLat);

        const v0 = {
          pos: p0,
          nor: p0,
          tan: Vec3.create(0,1,0),
          tex: [texMin],
          col
        };
        const v1 = {
          pos: p1,
          nor: p1,
          tan: Vec3.create(0,1,0),
          tex: [Vec2.create(texMin.x,texMax.y)],
          col
        }
        const v2 = {
          pos: p2,
          nor: p2,
          tan: Vec3.create(0,1,0),
          tex: [texMax],
          col
        };
        const v3 = {
          pos: p3,
          nor: p3,
          tan: Vec3.create(0,1,0),
          tex: [Vec2.create(texMax.x,texMin.y)],
          col
        }
        yield {
          v0,
          v1,
          v2
        }

        if (lat !== 0 && lat !== numLat - 1) {
          yield {
            v0: v0,
            v1: v2,
            v2: v3
          }
        }
      }
    }
  })();
  const numTriangles = (numLat-1)*numLon*2;
  return StaticMeshData.fromTriangles(numTriangles, iterator);
}