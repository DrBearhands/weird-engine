// @ts-check

import { Vec2 } from "../../Data/LinearAlgebra.js";
import { squareArrayIndex } from "../../utils.js";
import { Heightmap, bilinearHeight, bilinearSlope, pixelSlope2 } from "./Heightmap.js";

const slope = new Vec2();
const p = new Vec2();

export default class HeightMapTerrainData {
  /** @readonly @type {Float32Array} */
  heights;
  /** @readonly @type {Float32Array} */
  normals;
  /** @readonly @type {Uint32Array} */
  indices;
  /** @readonly @type {number} */
  size;
  /** @readonly @type {number} */
  sqrtVertexCount;

  /**
   * Number of vertices equal to grid size of map. This is a little odd as a 
   * 1x1 heightmap should be a flat plane rather than a vertex. Might lead to 
   * errors when joining multiple tiles due to off-by-1 errors.
   * @param {Heightmap} heightmap, must be square
   * @param {number} sqrtVertexCount
   */
  constructor(heightmap, sqrtVertexCount = heightmap.sqrtPointCount) {
    if (sqrtVertexCount < heightmap.sqrtPointCount) {
      throw "Cannot downsample heightmap"
    }
    this.sqrtVertexCount = sqrtVertexCount;
    this.size = heightmap.worldSize
    const maxHeight = heightmap.maxHeight;

    const vertexCount = sqrtVertexCount*sqrtVertexCount;
    const normals = new Float32Array(vertexCount*3);
    if (vertexCount === heightmap.points.length) 
    {
      const heights = new Float32Array(heightmap.points);
      for (let ii = 0; ii < heights.length; ++ii) {
        heights[ii] *= maxHeight;
      }

      for (let x = 1; x < sqrtVertexCount-1; ++x) {
        for (let z = 1; z < sqrtVertexCount-1; ++z) {

          pixelSlope2(slope, heightmap, x, z);
          
          const fx = slope[0];
          const fz = slope[1];
          
          const l = Math.sqrt(fx*fx + 1.0 + fz*fz);
          
          const ii = squareArrayIndex(sqrtVertexCount, x, z)*3;

          normals[ii+0] = fx/l;
          normals[ii+1] = 1/l;
          normals[ii+2] = fz/l;
        }
      }
      this.heights = heights;
    } 
    else 
    {
      const heights = new Float32Array(vertexCount);
      const maxIndex = sqrtVertexCount-1;
      for (let ix = 0; ix < sqrtVertexCount; ++ix) {
        for (let iz = 0; iz < sqrtVertexCount; ++iz) {
          Vec2.set(p, ix / maxIndex, iz / maxIndex);

          const ii = squareArrayIndex(sqrtVertexCount, ix, iz);
          heights[ii] = bilinearHeight(heightmap, p);
        }
      }

      for (let ix = 0; ix < sqrtVertexCount; ++ix) {
        for (let iz = 0; iz < sqrtVertexCount; ++iz) {
          Vec2.set(p, ix / maxIndex, iz / maxIndex);
          bilinearSlope(slope, heightmap, p);
          
          const fx = slope[0];
          const fz = slope[1];
          
          const l = Math.sqrt(fx*fx + 1.0 + fz*fz);
          
          const ii = squareArrayIndex(sqrtVertexCount, ix, iz)*3;

          normals[ii+0] = fx/l;
          normals[ii+1] = 1/l;
          normals[ii+2] = fz/l;
        }
      }
      this.heights = heights;
    }
    
    this.normals = normals;
  }
}