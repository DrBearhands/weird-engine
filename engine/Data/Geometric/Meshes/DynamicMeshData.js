// @ts-check

import StaticMeshData from "./StaticMesh.js";
export {POINTS, LINES, LINE_LOOP, LINE_STRIP, TRIANGLES, TRIANGLE_STRIP, TRIANGLE_FAN} from "./StaticMesh.js";

export default class DynamicMeshData extends StaticMeshData {
  /**
   * @type {Array<Uint16Array>}
   */
  joints;
  /**
   * @type {Array<Float32Array>}
   */
  weights;
}