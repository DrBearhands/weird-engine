// @ts-check

import { Vec2, Vec3, Vec4 } from "../../../Data/LinearAlgebra.js";
import { Vec2Array, Vec3Array, Vec4Array } from "../../VectorArrays.js";

export const POINTS  = 0;
export const LINES = 1;
export const LINE_LOOP = 2;
export const LINE_STRIP = 3;
export const TRIANGLES = 4;
export const TRIANGLE_STRIP = 5;
export const TRIANGLE_FAN = 6;

/**
 * @typedef { POINTS | LINES | LINE_LOOP | LINE_STRIP | TRIANGLES | TRIANGLE_STRIP | TRIANGLE_FAN } DRAW_MODE
 */

export default class StaticMeshData {
  /**
   * @type {Uint32Array}
   */
  indices;
  /**
   * @type {Vec3Array}
   */
  positions;
  /**
   * @type {Vec3Array}
   */
  normals;
  /**
   * @type {Vec4Array}
   */
  tangents;
  /**
   * @type {Vec2Array[]}
   */
  texcoords;
  /**
   * @type {Vec4Array[]}
   */
  colors;
  /**
   * @type { DRAW_MODE }
   */
  mode;

  /**
   * 
   * @param {number} numVerts 
   * @param {number} numIndices 
   */
  constructor(numVerts, numIndices) {
    this.indices=new Uint32Array(numIndices);
    this.positions=new Float32Array(numVerts*3);
    this.normals=new Float32Array(numVerts*3);
    this.tangents=new Float32Array(numVerts*4);
    this.texcoords=[new Float32Array(numVerts*2)];
    this.colors=[new Float32Array(numVerts*4)];
    this.mode=TRIANGLES;
  }

  /**
   * @param {number} numTriangles
   * @param {Iterable<Triangle>} iterable
   */
  static fromTriangles(numTriangles, iterable) {
    
    const vertexIterable = (function*() {
      for (const triangle of iterable) {
        yield triangle.v0;
        yield triangle.v1;
        yield triangle.v2;
      }
    })();

    return StaticMeshData.fromVertices(numTriangles*3, vertexIterable);
  }

  /**
   * 
   * @param {number} numVertices 
   * @param {Iterable<Vertex>} iterable 
   */
  static fromVertices(numVertices, iterable) {
    const staticMeshData = new StaticMeshData(numVertices, numVertices);

    let vertexIdx = 0;
    for (const vertex of iterable) {
      staticMeshData.indices[vertexIdx] = vertexIdx;
      Vec3Array.setAt(staticMeshData.positions, vertexIdx, vertex.pos);
      Vec3Array.setAt(staticMeshData.normals, vertexIdx, vertex.nor);
      Vec4Array.setAt(staticMeshData.tangents, vertexIdx, vertex.tan);
      for (let ii = 0; ii < vertex.tex.length; ++ii) {
        if (staticMeshData.texcoords[ii] === undefined)
          staticMeshData.texcoords[ii] = new Vec2Array(numVertices);
        Vec2Array.setAt(staticMeshData.texcoords[ii], vertexIdx, vertex.tex[ii]);
      }
      for (let ii = 0; ii < vertex.col.length; ++ii) {
        if (staticMeshData.colors[ii] === undefined)
          staticMeshData.colors[ii] = new Vec4Array(numVertices);
        Vec4Array.setAt(staticMeshData.colors[ii], vertexIdx, vertex.col[ii]);
      }

      vertexIdx++;
    }

    return staticMeshData;
  }
}

export class Triangle {
  v0 = new Vertex();
  v1 = new Vertex();
  v2 = new Vertex();
}

export class Vertex {
  pos = new Vec3();
  nor = new Vec3();
  tan = new Vec4();
  tex = [new Vec2()];
  col = [new Vec4()];
}

/**
 * 
 * @param {StaticMeshData} staticMeshData 
 * @param {number} index
 * @returns {Vertex}
 */
export function getVertex(staticMeshData, index) {
  const out = new Vertex();
  Vec3Array.getAt(out.pos, staticMeshData.positions, index);
  Vec3Array.getAt(out.nor, staticMeshData.normals, index);
  Vec4Array.getAt(out.tan, staticMeshData.tangents, index);
  for (let ii = 0; ii < staticMeshData.texcoords.length; ++ii) {
    const tex = out.tex[ii] = new Vec2();
    Vec2Array.getAt(tex, staticMeshData.texcoords[ii], index);
  }
  for (let ii = 0; ii < staticMeshData.colors.length; ++ii) {
    const col = out.col[ii] = new Vec4();
    Vec4Array.getAt(col, staticMeshData.colors[ii], index);
  }
  return out;
}


/**
 * @param {StaticMeshData} staticMeshData
 * @param {Triangle} [triangle]
 */
export function* triangles(staticMeshData, triangle = new Triangle()) {
  const indices = staticMeshData.indices;

  for (let ii = 0; ii < indices.length; ii+=3) {
    triangle.v0 = getVertex(staticMeshData, ii);
    triangle.v1 = getVertex(staticMeshData, ii+1);
    triangle.v2 = getVertex(staticMeshData, ii+2);

    yield triangle;
  }
}