// @ts-check

export default class SetExtra {

  /**
   * @template T
   * @param {Set<T>} a 
   * @param {Set<T>} b 
   */
  static union(a, b) {
    const out = new Set();
    for (const t of a) {
      out.add(t);
    }
    for (const t of b) {
      out.add(t);
    }
    return out;
  }
}