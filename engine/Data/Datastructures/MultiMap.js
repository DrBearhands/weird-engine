// @ts-check

/**
 * @template K
 * @template V
 */
export class MultiMap {
  /**
   * @type {Map<K, V[]>}
   */
  #map = new Map();
  
  /**
   * @param {K} k 
   * @param {V} v 
   */
  add(k, v) {
    const arr = this.#map.get(k);
    if (arr) {
      arr.push(v);
    } else {
      const arr = [v];
      this.#map.set(k, arr);
    }
  }

  /**
   * @param {K} k
   * @return {V[] | undefined}
   */
  get(k) {
    return this.#map.get(k);
  }

  /**
   * @returns {Iterator<[K, V[]]>}
   */
  *[Symbol.iterator]() {
    for (const [k, vs] of this.#map) {
      yield [k, vs]
    }
  }
}