// @ts-check

/**
 * @template K
 * @template V
 */
export default class DefaultMap {
  /** @type {Map<K, V>} */
  #map = new Map();
  
  #makeDefault;

  /**
   * @param {(k : K) => V} makeDefault 
   */
  constructor(makeDefault) {
    this.#makeDefault = makeDefault;
  }

  /**
   * @param {K} k 
   */
  get(k) {
    const v = this.#map.get(k);
    if (v === undefined) {
      const new_v = this.#makeDefault(k);
      this.#map.set(k, new_v);
      return new_v;
    }
    return v;
  }

  /**
   * @param {K} k 
   * @param {V} v 
   */
  set(k, v) {
    this.#map.set(k, v);
  }

  /**
   * @param {K} k 
   */
  delete(k) {
    this.#map.delete(k);
  }

  [Symbol.iterator]() {
    return this.#map.entries();
  }
}