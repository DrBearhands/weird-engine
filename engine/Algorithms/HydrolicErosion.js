// @ts-check

import {Heightmap, bilinearHeight, bilinearIndices, exactSlope, nearestHeight, nearestIndex, worldPosition, } from "../Data/Heightmap.js";
import ivec2 from "../Math/ivec2.js";
import Vec2 from "../Math/Vec2.js";
import Vec3 from "../Math/Vec3.js";
import { squareArrayIndex } from "../utils.js";

const dh0 = new Vec2();
const dh1 = new Vec2();

const minSlope = 0.05;
const Kq = 10;
const Kw = 0.001; //water evaporation
const g = 20; // gravity / acceleration
const Kd = 0.1;
const Kr = 0.9;

const d = new Vec2();
const p1 = new Vec2();

const nearestErode = (() => {
  const ip = ivec2.createZeroes();
  /**
   * 
   * @param {Heightmap} heightmap 
   * @param {Vec2} p 
   * @param {number} v 
   */
  return function (heightmap, p, v) {
    nearestIndex(ip, heightmap, p);
    const sqrtPointCount = heightmap.sqrtPointCount;
    const points = heightmap.points;
    const ii = squareArrayIndex(sqrtPointCount, ip[0], ip[1]);
    const cellVolume = heightmap.cellVolume;
    points[ii] -= v / cellVolume;
  }
})();

const bilinearErode = (() => {

  const ip = ivec2.createZeroes();
  const w = new Vec2();

  /**
   * @param {Heightmap} heightmap 
   * @param {Vec2} p location to erode at
   * @param {number} v volume to erode
   */
  return function (heightmap, p, v) {
    bilinearIndices(ip, w, heightmap, p);
    const sqrtPointCount = heightmap.sqrtPointCount;

    const i00 = squareArrayIndex(sqrtPointCount, ip[0], ip[1]);
    const i10 = squareArrayIndex(sqrtPointCount, ip[0]+1, ip[1]);
    const i01 = squareArrayIndex(sqrtPointCount, ip[0], ip[1]+1);
    const i11 = squareArrayIndex(sqrtPointCount, ip[0]+1, ip[1]+1);

    const cellVolume = heightmap.cellVolume;
  
    const a = v / cellVolume;
    const points = heightmap.points;
    const wx = w[0];
    const wz = w[1];

    points[i00] -= a * (1-wx) * (1-wz);
    points[i10] -= a * wx * (1-wz);
    points[i01] -= a * (1-wx) * wz;
    points[i11] -= a * wx * wz;
  }
})();

const nearestDeposit = (() => {
  const ip = ivec2.createZeroes();
  /**
   * 
   * @param {Heightmap} heightmap 
   * @param {Vec2} p 
   * @param {number} v 
   */
  return function (heightmap, p, v) {
    nearestIndex(ip, heightmap, p);
    const sqrtPointCount = heightmap.sqrtPointCount;
    heightmap.points[squareArrayIndex(sqrtPointCount, ip[0], ip[1])] += v / heightmap.cellVolume;
  }
})();

const bilinearDeposit = (() => {
  const ip = ivec2.createZeroes();
  const w = new Vec2();
    /**
   * @param {Heightmap} heightmap 
   * @param {Vec2} p location to deposit at
   * @param {number} v volume to deposit
   */
  return function (heightmap, p, v) {
    bilinearIndices(ip, w, heightmap, p);
    const sqrtPointCount = heightmap.sqrtPointCount;
    const i00 = squareArrayIndex(sqrtPointCount, ip[0], ip[1]);
    const i10 = squareArrayIndex(sqrtPointCount, ip[0]+1, ip[1]);
    const i01 = squareArrayIndex(sqrtPointCount, ip[0], ip[1]+1);
    const i11 = squareArrayIndex(sqrtPointCount, ip[0]+1, ip[1]+1);

    const a = v / heightmap.cellVolume;
    const points = heightmap.points;
    const wx = w[0];
    const wz = w[1];
    points[i00] += a * (1-wx) * (1-wz);
    points[i10] += a * wx * (1-wz);
    points[i01] += a * (1-wx) * wz;
    points[i11] += a * wx * wz;
  }
})();

const bNoInterpolate = false;

const getHeight = bNoInterpolate ? nearestHeight : bilinearHeight;
const slope = bNoInterpolate ? exactSlope : exactSlope;
const deposit = bNoInterpolate ? nearestDeposit : bilinearDeposit;
const erode = bNoInterpolate ? nearestErode : bilinearErode;

export class Raindrop {
  heightmap;
  positionHeightmap = new Vec2();
  velocity = new Vec2();
  s = 0.0;
  w = 0.1;

  /**
   * @param {Heightmap} heightmap 
   */
  constructor(heightmap) {
    this.heightmap = heightmap;
    this.positionHeightmap[0] = Math.random();
    this.positionHeightmap[1] = Math.random();
    this.previousHeight = getHeight(this.heightmap, this.positionHeightmap);
  }

  /**
   * 
   * @param {Vec3} out 
   */
  position(out) {
    worldPosition(out, this.heightmap, this.positionHeightmap);
  }

  /**
   * @returns {boolean} false if drop stopped
   */
  advance() {
    const heightmap = this.heightmap;
    const stepSize = 1.0 / heightmap.sqrtPointCount;
    const p0 = this.positionHeightmap;
    slope(dh0, heightmap, p0);
    Vec2.scale(this.velocity, this.velocity, 0.8);
    Vec2.scaleAndAdd(this.velocity, this.velocity, dh0, 0.01);

    
    Vec2.normalize(d, this.velocity);
    const v = Vec2.length(this.velocity);
    Vec2.scaleAndAdd(p1, p0, d, stepSize);
    
    if (!Vec2.inBounds(p1, 0, 1)) {
      deposit(heightmap, p0, this.s);
      return false;
    }

    const h0 = getHeight(heightmap, p0);
    const h1 = getHeight(heightmap, p1);

    if (h1 >= h0) {

      const ds = (h1-h0) * heightmap.cellVolume;

      if (ds >= this.s) {
        deposit(heightmap, p0, this.s);
        return false;
      } else {
        deposit(heightmap, p0, ds);
        this.s -= ds;
        Vec2.set(this.velocity, 0, 0);
        return true;
      }
    }

    const slope0 = Vec2.length(dh0);
    const w = this.w = Math.max(this.w - Kw, 0);
    if (w === 0) return false;
    const q = Math.max(slope0, minSlope)*v*w*Kq;

    let ds = this.s-q;

    if (ds >= 0) {
      ds *= Kd;
      deposit(heightmap, p0, ds);
    } else {
      const ds_max = (h0-h1)*heightmap.cellVolume;
      ds = Math.min(ds*Kr, ds_max);
      erode(heightmap, p0, -ds);
    }
    this.s -= ds;

    Vec2.copy(p0, p1);
    
    return true;
  }
}

/**
 * 
 * @param {Heightmap} heightmap
 * @param {number} steps
 * @param {number} iterations 
 */
export function runErosion(heightmap, steps, iterations) {
  for (let ii = 0; ii < iterations; ++ii) {
    const raindrop = new Raindrop(heightmap);
    for (let jj = 0; jj < steps; ++jj) {
      if (!raindrop.advance())
        break;
    }
  }
}
