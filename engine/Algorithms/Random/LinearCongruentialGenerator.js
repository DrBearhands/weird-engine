// @ts-check

// https://en.wikipedia.org/wiki/Linear_congruential_generator
export class LinearCongruentialGenerator {
  #m;
  #a;
  #c;
  #X;

  /**
   * @param {number} seed
   * @param {number} [m] 
   * @param {number} [a] 
   * @param {number} [c]
   */
  constructor(seed, m = 0x80000000, a = 1103515245, c = 1103515245) {
    this.#m = m;
    this.#a = a;
    this.#c = c;
    this.#X = seed;
  }

  generate() {
    this.#X = (this.#a * this.#X + this.#c) % this.#m;
    return this.#X;
  }
}