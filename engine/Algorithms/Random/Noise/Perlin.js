// @ts-check

import vec2array from "../../../Data/VectorArrays/Vec2Array.js";
import { Vec2 } from "../../../Data/LinearAlgebra.js";
import { interpolateCubic } from "../../../Data/Numbers.js";


/**
 * Source: https://www.youtube.com/watch?v=kCIaHqb60Cw
 * @param {Vec2} out
 * @param {number} ix 
 * @param {number} iy 
 */
function fakeRandomGradient(out, ix, iy) {
  const w = 8 * 32;
  const s = w / 2; 
  let a = ix | 0, b = iy | 0;
  a = (a * 3284157443) | 0;

  b ^= a << s | a >> w - s;
  b = (b * 1911520717) | 0;

  a ^= b << s | b >> w - s;
  a = (a * 2048419325) | 0;

  const random = a * (3.14159265 / ~(0xEFFFFFFF));

  // Create the vector from the angle
  Vec2.set(out, Math.sin(random), Math.cos(random));
}

export const fakeRandomGradientGrid = (() => {
  const gradient = new Vec2();

  /**
   * @param {number} xsize
   * @param {number} ysize
   */
  return (xsize, ysize) => {
    const result = vec2array.createZeroes(xsize*ysize);
    for (let ii = 0; ii < xsize*ysize; ++ii) {
      const x = ii / ysize;
      const y = ii % ysize;
      fakeRandomGradient(gradient, x, y);
      vec2array.setAt(result, ii, gradient);
    }
    return result;
  };
})();

/**
 * 
 * @param {Vec2} out 
 */
function randomGradient(out) {
  const random = Math.random()*2*3.14159265;
  Vec2.set(out, Math.cos(random), Math.sin(random));
}

export const randomGradientGrid = (() => {
  const gradient = new Vec2();

  /**
   * @param {number} xsize
   * @param {number} ysize
   */
  return (xsize, ysize) => {
    const result = vec2array.createZeroes(xsize*ysize);
    for (let ii = 0; ii < xsize*ysize; ++ii) {
      randomGradient(gradient);
      vec2array.setAt(result, ii, gradient);
    }
    return result;
  }
})();


const dotGridGradient = (() => {
  const gradient = new Vec2();
  /**
   * 
   * @param {number} ix 
   * @param {number} iy 
   * @param {number} x 
   * @param {number} y
   * @param {vec2array} gradients
   * @param {number} gY the number of points in the gradient map in y-direction
   */
  return function(ix, iy, x, y, gradients, gY) {
    vec2array.getAt(gradient, gradients, ix*gY+iy)
  
    const dx = x - ix;
    const dy = y - iy;
  
    return dx * gradient[0] + dy * gradient[1];
  }

})();

/**
 * 
 * @param {number} x 
 * @param {number} y 
 * @param {vec2array} gradients
 * @param {number} gradientsGridSize
 */
export function perlinFromGradients(x, y, gradients, gradientsGridSize) {
  const x0 = Math.floor(x);
  const y0 = Math.floor(y);
  const x1 = x0+1;
  const y1 = y0+1;

  const sx = x - x0;
  const sy = y - y0;

  const g0 = dotGridGradient(x0, y0, x, y, gradients, gradientsGridSize);
  const g1 = dotGridGradient(x1, y0, x, y, gradients, gradientsGridSize);
  const ix0 = interpolateCubic(g0, g1, sx);
  const g2 = dotGridGradient(x0, y1, x, y, gradients, gradientsGridSize);
  const g3 = dotGridGradient(x1, y1, x, y, gradients, gradientsGridSize);
  const ix1 = interpolateCubic(g2, g3, sx);
  
  return interpolateCubic(ix0, ix1, sy);
}

/**
 * @param {number} mapSize
 * @param {number} frequency
 * @returns {Float32Array} of size mapSize*mapSize
 */
export function perlinNoiseMap(mapSize, frequency) {
  const gradientMapSize = (frequency+1);
  const gradients = randomGradientGrid(gradientMapSize, gradientMapSize);

  const perlinMap = new Float32Array(mapSize*mapSize);
  for (let ix = 0; ix < mapSize; ++ix) {
    for (let iy = 0; iy < mapSize; ++iy) {
      const x = ix / mapSize * frequency;
      const y = iy / mapSize * frequency;
      const h = perlinFromGradients(x, y, gradients, gradientMapSize)
      perlinMap[ix*mapSize+iy] = h;
    }
  }
  
  return perlinMap;
}