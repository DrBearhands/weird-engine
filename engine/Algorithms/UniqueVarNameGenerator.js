// @ts-check

import DefaultMap from "../Data/Datastructures/DefaultMap.js";

/**
 * 
 * @param {string} str
 * @param {string} c
 * @param {number} n 
 */
function leftPad(str, c, n) {
  return c.repeat(Math.max(0, n - str.length)) + str;
}

export class UniqueVarNameGenerator {

  /**
   * @type {DefaultMap<string, number>}
   */
  #counters = new DefaultMap(() => 0);

  /**
   * @param {string} [base] 
   * @returns {string}
   */
  generate(base = "v") {
    const counter = this.#counters.get(base);
    const newName = `${base}_${leftPad((counter).toString(), '0', 4)}`;
    this.#counters.set(base, counter +1);
    return newName;
  }
}

export const nameGenerator = new UniqueVarNameGenerator();