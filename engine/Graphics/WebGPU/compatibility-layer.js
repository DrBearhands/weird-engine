// Declares some window-level values so importing modules does not throw exceptions. Import this first!

if (!window.GPUShaderStage) { 
  window.GPUShaderStage = {
    /** @readonly */ prototype: {
      __brand: "GPUShaderStage",
      /** @readonly */ VERTEX: 0x1,
      /** @readonly */ FRAGMENT: 0x2,
      /** @readonly */ COMPUTE: 0x4
    },
    /** @readonly */ VERTEX: 0x1,
    /** @readonly */ FRAGMENT: 0x2,
    /** @readonly */ COMPUTE: 0x4
  }
}

if (!window.GPUBufferUsage) {
  window.GPUBufferUsage = {
      prototype: {
        /** @readonly */ __brand: "GPUBufferUsage",
        /** @readonly */ MAP_READ: 0x0001,
        /** @readonly */ MAP_WRITE: 0x0002,
        /** @readonly */ COPY_SRC: 0x0004,
        /** @readonly */ COPY_DST: 0x0008,
        /** @readonly */ INDEX: 0x0010,
        /** @readonly */ VERTEX: 0x0020,
        /** @readonly */ UNIFORM: 0x0040,
        /** @readonly */ STORAGE: 0x0080,
        /** @readonly */ INDIRECT: 0x0100,
        /** @readonly */ QUERY_RESOLVE: 0x0200,
      },
      /** @readonly */ MAP_READ: 0x0001,
      /** @readonly */ MAP_WRITE: 0x0002,
      /** @readonly */ COPY_SRC: 0x0004,
      /** @readonly */ COPY_DST: 0x0008,
      /** @readonly */ INDEX: 0x0010,
      /** @readonly */ VERTEX: 0x0020,
      /** @readonly */ UNIFORM: 0x0040,
      /** @readonly */ STORAGE: 0x0080,
      /** @readonly */ INDIRECT: 0x0100,
      /** @readonly */ QUERY_RESOLVE: 0x0200,
  }
}