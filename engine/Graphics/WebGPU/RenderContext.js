// @ts-check

import { device, preferredCanvasFormat } from "./WebGPUInit.js";


export default class RenderContext {
  /** @readonly */
  canvas;
  /** @readonly */
  context;
  
  /**
   * @param {HTMLCanvasElement | OffscreenCanvas} canvas 
  */
 constructor(canvas) {
   this.canvas = canvas;
   /** @type {GPUCanvasContext | null} */
   const context = canvas.getContext("webgpu");
   if (!context) throw "failed to get context";
   
   this.context = context
   
   context.configure({
     device: device,
     format: preferredCanvasFormat,
     alphaMode: "premultiplied"
    });
  }

  /** @readonly */
  static depthTextureFormat = "depth24plus";
  
  /** @type {GPUTexture | undefined} */
  #depthTexture = undefined;

  /** @type {GPUTextureView} */
  #depthView;
  /** @type {GPUTextureView} */
  #colorView;

  resizeToCanvas() {
    const canvas = this.canvas;
    if (canvas instanceof HTMLCanvasElement) {
      const renderWidth = canvas.clientWidth * devicePixelRatio;
      const renderHeight = canvas.clientHeight * devicePixelRatio;
      
      if (canvas.width !== renderWidth || canvas.height !== renderHeight) {
        canvas.width = renderWidth;
        canvas.height = renderHeight;
        this.#depthTexture?.destroy();
        this.#depthTexture = undefined;
      }
    }

    if (this.#depthTexture === undefined) {
      this.#depthTexture = device.createTexture({
        dimension: "2d",
        format: RenderContext.depthTextureFormat,
        size: [canvas.width, canvas.height, 1],
        usage: GPUTextureUsage.RENDER_ATTACHMENT
      });
      this.#depthView = this.#depthTexture.createView();
      this.#colorView = this.context.getCurrentTexture().createView();
    }
  }

  get depthView() {
    return this.#depthView;
  }

  get colorView() {
    return this.#colorView;
  }
}