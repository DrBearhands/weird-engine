// @ts-check

import { Mat3 } from "../../Data/LinearAlgebra.js";
import { SizeOf } from "./wgsl.js";

/**
 * @param {GPUDevice} device
 * @param {GPUBuffer} buffer
 * @param {Mat3} m 
 */
export function bufferMat3(device, buffer, m) {
  const gpu_mat3_buffer = new Float32Array(12);
  gpu_mat3_buffer[0] = m[0];
  gpu_mat3_buffer[1] = m[1];
  gpu_mat3_buffer[2] = m[2];

  gpu_mat3_buffer[4] = m[3];
  gpu_mat3_buffer[5] = m[4];
  gpu_mat3_buffer[6] = m[5];

  gpu_mat3_buffer[8] = m[6];
  gpu_mat3_buffer[9] = m[7];
  gpu_mat3_buffer[10]= m[8];
  device.queue.writeBuffer(buffer, 0, gpu_mat3_buffer);
}

/**
 * 
 * @param {GPUDevice} device 
 * @param {import("./wgsl.js").WGSLSizedType} t 
 * @returns {GPUBuffer}
 */
export function createUniformBuffer(device, t) {
  const buffer = device.createBuffer({
    size: SizeOf(t),
    usage: GPUBufferUsage.COPY_DST | GPUBufferUsage.UNIFORM
  });
  return buffer;
}