// @ts-check

import { compute_base_src, names } from "../Render3D/Meshes/heightmap-mesh.wgsl.js"
import { nameGenerator } from "../../../Algorithms/UniqueVarNameGenerator.js";
import { hash21, hash22, hash23 } from "../Render3D/ShaderFragments/hashing.wgsl.js";

export const main_function_name = nameGenerator.generate("main");
const cell_volume = nameGenerator.generate("cell_volume");

/**
 * @param {{
 *   heightmap : {
 *     group : number,
 *     binding : number
 *   },
 *   terrain_metadata : {
 *     group : number,
 *     binding : number
 *   },
 *   random_seed : {
 *     group : number,
 *     binding : number
 *   }
 *  }} bindings
 * @returns 
 */
export const src = (bindings) => {
  const terrain_metadata = names.global.terrain_metadata;
  const terrain_heightmap = names.global.terrain_heightmap;
  const resolution = names.terrain_metadata_members.resolution;
  const world_size = names.terrain_metadata_members.world_size;
  const global_id_to_array_index = names.global.global_id_to_array_index;

  const nearest_deposit = nameGenerator.generate("nearest_deposit");
  const nearest_erode = nameGenerator.generate("nearest_erode");
  const nearest_height = nameGenerator.generate("nearest_height");

  const bilinear_deposit = nameGenerator.generate("bilinear_deposit");
  const bilinear_erode = nameGenerator.generate("bilinear_erode");
  const bilinear_height = nameGenerator.generate("bilinear_height");

  const useBilinear = true;

  const erode = useBilinear ? bilinear_erode : nearest_erode;
  const deposit = useBilinear ? bilinear_deposit : nearest_deposit;
  const height = useBilinear ? bilinear_height : nearest_height;

  return /* wgsl */ `

    ${compute_base_src(bindings)}

    ${hash21("hash21")}
    ${hash22("hash22")}
    ${hash23("hash23")}

    const min_slope = 0.05;
    const Kq : f32 = 0.01;
    const Kw : f32 = 0.001; //water evaporation
    const g  : f32 = 20; // gravity / acceleration
    const Kd : f32 = 0.1;
    const Kr : f32= 0.9;

    struct Raindrop {
      p : vec2<f32>,
      v : vec2<f32>,
      s : f32,
      w : f32,
    }

    @group(${bindings.random_seed.group})
    @binding(${bindings.random_seed.binding})
    var<uniform> random_seed : f32;

    var<private> raindrop : Raindrop;

    fn ${cell_volume}() -> f32 {
      let cellWorldSize = ${terrain_metadata}.${world_size} / f32(${terrain_metadata}.${resolution});
      return cellWorldSize*cellWorldSize; 
    }

    @compute
    @workgroup_size(64)
    fn ${main_function_name}(
      @builtin(global_invocation_id) id : vec3<u32>
    ) {
      raindrop.p = hash21(f32(id.x) + random_seed);
      raindrop.v = vec2<f32>(0.0,0.0);
      raindrop.s = 0.0;
      raindrop.w = 1.0;

      var halted = false;
      for (var ii : u32 = 0; ii < 40 && !halted; ii = ii + 1) {
        halted = !advance();
      }
    }

    fn advance() -> bool {
      let stepSize = 1.0 / f32(${terrain_metadata}.${resolution});
      let p0 = raindrop.p;
      let dh0 = slope(p0);
      raindrop.v = raindrop.v * 0.8 + dh0*0.01;
      let d = normalize(raindrop.v);
      let v_ = length(raindrop.v);
      let p1 = p0 + d * stepSize;
      
      if (p1.x < 0.0 || p1.y < 0.0 || p1.x > 1.0 || p1.y > 1.0 ) {
        ${deposit}(p0, raindrop.s);
        return false;
      }

      let h0 = ${height}(p0);
      let h1 = ${height}(p1);

      if (h1 >= h0) {

        let ds = (h1-h0) * ${cell_volume}();

        if (ds >= raindrop.s) {
          ${deposit}(p0, raindrop.s);
          return false;
        } else {
          ${deposit}(p0, ds);
          raindrop.s -= ds;
          raindrop.v = vec2<f32>(0.0, 0.0);
          return true;
        }
      }

      let slope0 = length(dh0);
      raindrop.w = max(raindrop.w - Kw, 0.0);
      if (raindrop.w == 0) {
        return false;
      }
      let q = max(slope0, min_slope)*v_*raindrop.w*Kq;

      var ds : f32 = raindrop.s-q;

      if (ds >= 0.0) {
        ds *= Kd;
        ${deposit}(p0, ds);
      } else {
        let ds_max = (h0-h1)*${cell_volume}();
        ds = min(ds*Kr, ds_max);
        ${erode}(p0, -ds);
      }
      raindrop.s -= ds;

      raindrop.p = p1;
      
      return true;
    }

    fn slope(p : vec2<f32>) -> vec2<f32> {
      let indices = bilinear_indices(p);
      let ip = indices.p;

      let h00 = pixel_height(ip + vec2<u32>(0,0));
      let h10 = pixel_height(ip + vec2<u32>(1,0));
      let h01 = pixel_height(ip + vec2<u32>(0,1));
      let h11 = pixel_height(ip + vec2<u32>(1,1));

      return vec2<f32> (
        h00+h01-h10-h11,
        h00+h10-h01-h11
      );
    };

    fn pixel_height(p : vec2<u32>) -> f32 {
      let max_index = ${terrain_metadata}.${resolution}-1;
      let p_clamped = clamp(p, vec2<u32>(0), vec2<u32>(max_index));
      return ${terrain_heightmap}[${global_id_to_array_index}(p)];
    }

    fn ${nearest_deposit}(p : vec2<f32>, v : f32) {
      ${nearest_erode}(p, -v);
    }

    fn nearest_index(v : vec2<f32>) -> vec2<u32> {
      let max_index = ${terrain_metadata}.${resolution}-1;
      return vec2<u32>(round(v*f32(max_index)));
    }

    fn ${nearest_height}(p : vec2<f32>) -> f32 {
      let ip = nearest_index(p);
      return pixel_height(ip);
    }

    fn ${nearest_erode}(p : vec2<f32>, v : f32) {
      let ip = nearest_index(p);
      let ii = ${global_id_to_array_index}(ip);
      ${terrain_heightmap}[ii] -= v / ${cell_volume}();
    }

    struct BilinearIndices {
      p : vec2<u32>,
      w : vec2<f32>
    }

    fn bilinear_interpolate(w : vec2<f32>, a : f32, b : f32, c : f32, d : f32) -> f32 {
      let wx0 = 1.0-w.x;
      let wy0 = 1.0-w.y;
      let wx1 = w.x;
      let wy1 = w.y;
      return a*wx0*wy0 + b*wx1*wy0 + c*wx0*wy1 + d*wx1*wy1;
    }

    fn bilinear_indices(p : vec2<f32>) -> BilinearIndices {
      let max_index_0 : u32 = u32(${terrain_metadata}.${resolution})-2;
      let max_index_1 : u32 = u32(${terrain_metadata}.${resolution})-1;
      let p_c = clamp(p, vec2<f32>(0.0), vec2<f32>(1.0));
      let f = p_c * f32(max_index_1);
      
      var res : BilinearIndices;
      res.p = min(vec2<u32>(floor(f)), vec2<u32>(max_index_0));
      res.w = fract(f);

      return res;
    }

    fn ${bilinear_height}(p : vec2<f32>) -> f32 {
      let bi = bilinear_indices(p);

      let h00 = pixel_height(bi.p + vec2<u32>(0, 0));
      let h10 = pixel_height(bi.p + vec2<u32>(1, 0));
      let h01 = pixel_height(bi.p + vec2<u32>(0, 1));
      let h11 = pixel_height(bi.p + vec2<u32>(1, 1));
    
      return bilinear_interpolate(bi.w, h00, h10, h01, h11);
    }

    fn ${bilinear_deposit}(p : vec2<f32>, v : f32) {
      ${bilinear_erode}(p, -v);
    }
    
    fn ${bilinear_erode}(p : vec2<f32>, v : f32) {
      let indices = bilinear_indices(p);
      let ip = indices.p;
      let w = indices.w;

      let i00 = ${global_id_to_array_index}(ip);
      let i10 = ${global_id_to_array_index}(ip+vec2<u32>(1, 0));
      let i01 = ${global_id_to_array_index}(ip+vec2<u32>(0, 1));
      let i11 = ${global_id_to_array_index}(ip+vec2<u32>(1, 1));
    
      let a = v / ${cell_volume}();
      let wx = w[0];
      let wz = w[1];

      ${terrain_heightmap}[i00] -= a * (1-wx) * (1-wz);
      ${terrain_heightmap}[i10] -= a * wx * (1-wz);
      ${terrain_heightmap}[i01] -= a * (1-wx) * wz;
      ${terrain_heightmap}[i11] -= a * wx * wz;
    }
  `;
};