//@ts-check

import { compute_base_src, names } from "../Render3D/Meshes/heightmap-mesh.wgsl.js"
import { nameGenerator } from "../../../Algorithms/UniqueVarNameGenerator.js";
import { hash22 } from "../Render3D/ShaderFragments/hashing.wgsl.js";

export const main_function_name = nameGenerator.generate("main");
const random_gradient = nameGenerator.generate("random_gradient");
const perlin = nameGenerator.generate("perlin");
const d2dh = nameGenerator.generate("d2dh");
const num_octaves = nameGenerator.generate("num_octaves");
const min_octave = nameGenerator.generate("min_octave");
const perlin_params = nameGenerator.generate("perlin_params");

/**
 * @param {{
 *   heightmap : {
 *     group : number,
 *     binding : number
 *   },
 *   terrain_metadata : {
 *     group : number,
 *     binding : number
 *   },
 *   perlin_params : {
 *     group : number,
 *     binding : number
 *   }
 * }} bindings
 * @returns {string}
 */
export const src = (bindings) => {
  const terrain_heightmap = names.global.terrain_heightmap;
  const terrain_metadata = names.global.terrain_metadata;
  const resolution = names.terrain_metadata_members.resolution;
  const world_size = names.terrain_metadata_members.world_size;

  const global_id_to_array_index = names.global.global_id_to_array_index;

  return /* wgsl */ `
    ${compute_base_src(bindings)}
    ${hash22("hash22")}


    struct ${d2dh} {
      value : f32,
      derivative : vec2<f32>
    }

    fn ${random_gradient}(in : vec2<f32>) -> vec2<f32>
    {
      return normalize(hash22(in + ${perlin_params}.random_seed)*2.0-1.0);
    }

    // From: https://iquilezles.org/articles/gradientnoise/
    fn ${perlin}( p : vec2<f32> ) -> ${d2dh}
    {
      let i : vec2<f32> = floor( p );
      let f : vec2<f32> = fract( p );

      let u : vec2<f32> = 3*f*f-2*f*f*f;
      let du : vec2<f32> = 6*f - 6*f*f;
      
      let ga : vec2<f32> = ${random_gradient}(i + vec2<f32>(0, 0));
      let gb : vec2<f32> = ${random_gradient}(i + vec2<f32>(1, 0));
      let gc : vec2<f32> = ${random_gradient}(i + vec2<f32>(0, 1));
      let gd : vec2<f32> = ${random_gradient}(i + vec2<f32>(1, 1));
      
      let va : f32 = dot( ga, f - vec2<f32>(0.0,0.0) );
      let vb : f32 = dot( gb, f - vec2<f32>(1.0,0.0) );
      let vc : f32 = dot( gc, f - vec2<f32>(0.0,1.0) );
      let vd : f32 = dot( gd, f - vec2<f32>(1.0,1.0) );

      var res : ${d2dh};

      res.value = va + u.x*(vb-va) + u.y*(vc-va) + u.x*u.y*(va-vb-vc+vd);
      res.derivative = ga + u.x*(gb-ga) + u.y*(gc-ga) + u.x*u.y*(ga-gb-gc+gd) + du * (u.yx*(va-vb-vc+vd) + vec2<f32>(vb,vc) - va);

      return res;
    }

    struct PerlinParams {
      max_height : f32,
      random_seed : vec2<f32>,
    }

    @group(${bindings.perlin_params.group}) @binding(${bindings.perlin_params.binding})
    var<uniform> ${perlin_params} : PerlinParams;
    const ${min_octave} : u32 = 2;
    const ${num_octaves} : u32 = 8;

    @compute
    @workgroup_size(8,1,8)
    fn ${main_function_name}(
      @builtin(global_invocation_id) id : vec3<u32>
    ) {
      let stepfrag = 1.0 / f32(${terrain_metadata}.${resolution});
      let x : f32 = f32(id.x) * stepfrag;
      let z : f32 = f32(id.z) * stepfrag;
      let ii : u32 = ${global_id_to_array_index}(id.xz);
      let ii_height : u32 = ii;
      var h : f32 = 0;

      for (var oct : u32 = 0; oct < ${num_octaves}; oct = oct + 1) {
        let s1 = pow(2.0, f32(oct + ${min_octave}));
        let s2 = pow(2.0, f32(oct));
        let p = vec2<f32>(x, z) * s1;
        let noise = ${perlin}(p);
        h = h + noise.value / s2;
      }
      
      ${terrain_heightmap}[ii_height] = h * ${perlin_params}.max_height;
    }
  `;
};