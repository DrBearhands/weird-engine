//@ts-check
import { compute_base_src, names } from "../Render3D/Meshes/heightmap-mesh.wgsl.js"
import { nameGenerator } from "../../../Algorithms/UniqueVarNameGenerator.js";

export const main_function_name = nameGenerator.generate("main");
/**
 * @param {{
 *   heightmap : {
 *     group : number,
 *     binding : number
 *   },
 *   terrain_metadata : {
 *     group : number,
 *     binding : number
 *   }
 *  }} bindings
 * @returns {string}
 */
export const src = (bindings) => {
  const terrain_metadata = names.global.terrain_metadata;
  const terrain_heightmap = names.global.terrain_heightmap;
  const global_id_to_array_index = names.global.global_id_to_array_index;
  const world_size = names.terrain_metadata_members.world_size;
  const resolution = names.terrain_metadata_members.resolution;

  return /* wgsl */ `
    ${compute_base_src(bindings)}

    @compute
    @workgroup_size(8,1,8)
    fn ${main_function_name}(
      @builtin(global_invocation_id) id : vec3<u32>
    ) {
      
      let ii_west : u32 = ${global_id_to_array_index}(id.xz - vec2<u32>(1, 0));
      let ii_east : u32 = ${global_id_to_array_index}(id.xz + vec2<u32>(1, 0));
      let ii_south : u32 = ${global_id_to_array_index}(id.xz - vec2<u32>(0, 1));
      let ii_north : u32 = ${global_id_to_array_index}(id.xz + vec2<u32>(0, 1));
      
      let y_west = ${terrain_heightmap}[ii_west];
      let y_east = ${terrain_heightmap}[ii_east];
      let y_south = ${terrain_heightmap}[ii_south];
      let y_north = ${terrain_heightmap}[ii_north];

      let gscale = f32(${terrain_metadata}.${resolution}) / ${terrain_metadata}.${world_size} / 2;
      
      let dydx = (y_west - y_east)*gscale;
      let dydz = (y_south - y_north)*gscale;
      
      let n = normalize(vec3<f32>(dydx, 1.0, dydz));
      
      let ii : u32 = ${global_id_to_array_index}(id.xz);
      ${terrain_heightmap}[ii+1] = n.x;
      ${terrain_heightmap}[ii+2] = n.y;
      ${terrain_heightmap}[ii+3] = n.z;
    }
  `;
};