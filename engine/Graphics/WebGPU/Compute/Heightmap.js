// @ts-check

import HeightmapMesh from "../Render3D/Meshes/HeightmapMesh.js";
import { f32, SizeOf, struct, vec } from "../wgsl.js";
import * as computePerlinShader from "./compute-perlin.js";
import * as computeNormalsShader from "./compute-normals.js";
import * as computeErosionShader from "./compute-erosion.js";
import { adapter, device } from "../WebGPUInit.js";


export default class HeightmapCompute {
  /** @readonly */
  computeBindGroup;

  /** @readonly */
  mesh;
  
  /**
   * 
   * @param {HeightmapMesh} mesh 
   * @param {GPUBindGroup} computeBindGroup 
   */
  constructor(mesh, computeBindGroup) {
    this.mesh = mesh;
    this.computeBindGroup = computeBindGroup
  }

  /**
   * @param {HeightmapMesh} mesh
   */
  static create( mesh ) {
    const computeBindGroup = device.createBindGroup({
      label: "Heightmap Compute Bind Group",
      layout: computeBindGroupLayout,
      entries: [
        {
          binding: computeBindings.terrain_metadata.binding,
          resource: {
            buffer: mesh.uniformBuffer
          }
        },
        {
          binding: computeBindings.heightmap.binding, 
          resource : { 
            buffer : mesh.vertexBuffer 
          }
        }
      ]
    });

    return new HeightmapCompute(mesh, computeBindGroup);
  }

  /**
   * 
   * @param {HeightmapCompute} heightmapCompute 
   * @param {number} height
   * @param {number} num_octaves
   */
  static perlinGPU(heightmapCompute, height, num_octaves) {
    device.queue.writeBuffer(
      perlinParamBuffer,
      0,
      new Float32Array([height, Math.random(), Math.random()])
    );

    const encoder = device.createCommandEncoder();
    const pass = encoder.beginComputePass();
    pass.setPipeline(perlinComputePipeline);
    pass.setBindGroup(computeBindings.heightmap.group, heightmapCompute.computeBindGroup);
    pass.setBindGroup(computeBindings.perlin_params.group, perlinBindGroup);
    const n = Math.sqrt(heightmapCompute.mesh.vertexCount)/8;
    pass.dispatchWorkgroups(n, 1, n);
    pass.setPipeline(normalsComputePipeline);
    pass.dispatchWorkgroups(n, 1, n);
    pass.end();
    const commandBuffer = encoder.finish();
    device.queue.submit([commandBuffer]);
  }

  /**
   * 
   * @param {HeightmapCompute} heightmapCompute 
   * @param {number} steps
   */
  static erodeGPU(heightmapCompute, steps) {
    const encoder = device.createCommandEncoder();
    const pass = encoder.beginComputePass();
    const seed = Math.random();
    device.queue.writeBuffer(
      seedBuffer,
      0,
      new Float32Array([seed])
    );
    pass.setPipeline(hydraulicErosionComputePipeline);
    pass.setBindGroup(computeBindings.heightmap.group, heightmapCompute.computeBindGroup);
    pass.setBindGroup(computeBindings.random_seed.group, erosionBindGroup);

    const dispatchSize = Math.min(adapter.limits.maxComputeWorkgroupsPerDimension, steps);
    const lastDispatchSize = steps % adapter.limits.maxComputeWorkgroupsPerDimension;
    const numDispatches = Math.ceil(steps / adapter.limits.maxComputeWorkgroupsPerDimension);
    for (let ii = 0; ii < numDispatches-1; ++ii) {
      pass.dispatchWorkgroups(dispatchSize);
    }
    pass.dispatchWorkgroups(lastDispatchSize);
    pass.setPipeline(normalsComputePipeline);
    const n = Math.sqrt(heightmapCompute.mesh.vertexCount)/8;
    pass.dispatchWorkgroups(n, 1, n);
    pass.end();

    const commandBuffer = encoder.finish();
    device.queue.submit([commandBuffer]);
  }
}

const computeBindings = {
  heightmap: {
    group: 0,
    binding : 0
  },
  terrain_metadata: {
    group: 0,
    binding: 1
  },
  perlin_params : {
    group: 1,
    binding: 0
  },
  random_seed: {
    group: 1,
    binding: 0
  }
}
/**
 * @readonly @type {GPUBindGroupLayout}
 */
const computeBindGroupLayout = device.createBindGroupLayout({
  label: "HeightmapMeshLayout::computeBindGroupLayout",
  entries: [
    {
      binding: computeBindings.terrain_metadata.binding,
      visibility: GPUShaderStage.COMPUTE,
      buffer: {
        type: "uniform",
        minBindingSize: HeightmapMesh.size_terrain_metadata
      }
    },
    {
      binding: computeBindings.heightmap.binding,
      visibility: GPUShaderStage.COMPUTE,
      buffer: {
        type: "storage",
      }
    }
  ]
});

const perlinParamSize = SizeOf(struct(f32, vec(2, f32)));

/**
 * @readonly @type {GPUBindGroupLayout}
 */
const computePerlinBindGroupLayout = device.createBindGroupLayout({
  label: "Compute Perlin",
  entries: [
    {
      binding: computeBindings.perlin_params.binding,
      visibility : GPUShaderStage.COMPUTE,
      buffer: {
        type: "uniform",
        minBindingSize: perlinParamSize
      }
    },
  ]
});

/**
 * @readonly @type {GPUBindGroupLayout}
 */
const computeErosionBindGroupLayout = device.createBindGroupLayout({
  label: "Compute Hydraulic Erosion",
  entries: [
    {
      binding: computeBindings.random_seed.binding,
      visibility: GPUShaderStage.COMPUTE,
      buffer: {
        type: "uniform",
        minBindingSize: SizeOf(f32)
      }
    }
  ]
});;

/**
 * @readonly @type {GPUComputePipeline}
 */
const perlinComputePipeline = device.createComputePipeline({
  label : "Compute Pipeline - Perlin noise",
  layout: device.createPipelineLayout({
    bindGroupLayouts: [
      computeBindGroupLayout,
      computePerlinBindGroupLayout
    ]
  }),
  compute: {
    module: device.createShaderModule({
      label: "Shader Module - Perlin noise",
      code: computePerlinShader.src(computeBindings)
    }),
    entryPoint: computePerlinShader.main_function_name
  }
});;

/**
 * @readonly @type {GPUComputePipeline}
 */
const normalsComputePipeline = device.createComputePipeline({
  label : "Compute Pipeline - terrain normals generation",
  layout: device.createPipelineLayout({
    bindGroupLayouts: [
      computeBindGroupLayout
    ]
  }),
  compute: {
    module: device.createShaderModule({
      label: "Shader module - terrain normals generation",
      code: computeNormalsShader.src(computeBindings)
    }),
    entryPoint: computeNormalsShader.main_function_name
  }
});

/**
 * @readonly @type {GPUComputePipeline}
 */
const hydraulicErosionComputePipeline = device.createComputePipeline({
  label: "Compute Pipeline - hydraulic erosion",
  layout: device.createPipelineLayout({
    bindGroupLayouts: [
      computeBindGroupLayout,
      computeErosionBindGroupLayout
    ]
  }),
  compute: {
    module: device.createShaderModule({
      label: "Shader module - hydraulic erosion",
      code: computeErosionShader.src(computeBindings)
    }),
    entryPoint: computeErosionShader.main_function_name
  }
});

/**
 * @readonly @type {GPUBuffer}
 */
const perlinParamBuffer = device.createBuffer({
  size: perlinParamSize,
  usage: GPUBufferUsage.UNIFORM | GPUBufferUsage.COPY_DST
});

/**
 * @readonly @type {GPUBindGroup}
 */
const perlinBindGroup = device.createBindGroup({
  label: "Perlin Noise Generation Data",
  layout: computePerlinBindGroupLayout,
  entries: [
    {
      binding: computeBindings.perlin_params.binding,
      resource: {
        buffer: perlinParamBuffer
      }
    }
  ]
});

/**
 * @readonly @type {GPUBuffer}
 */
const seedBuffer = device.createBuffer({
  size: SizeOf(f32),
  usage: GPUBufferUsage.UNIFORM | GPUBufferUsage.COPY_DST
});

/**
 * @readonly @type {GPUBindGroup}
 */
const erosionBindGroup = device.createBindGroup({
  label: "Hydraulic Erosion",
  layout: computeErosionBindGroupLayout,
  entries: [
    {
      binding: computeBindings.random_seed.binding,
      resource : {
        buffer: seedBuffer
      }
    }
  ]
});