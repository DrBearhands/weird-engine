// @ts-check
// see https://gpuweb.github.io/gpuweb/wgsl/#byte-size

class T_i32 {
  /** @type {"T_i32"} */
  static type = "T_i32";
  toString() {
    return "i32";
  }
}
class T_u32 {
  /** @type {"T_u32"} */
  static type = "T_u32";
  toString() {
    return "u32";
  }
}
class T_f32 {
  /** @type {"T_f32"} */
  static type = "T_f32";
  toString() {
    return "f32";
  }
}
class T_f16 {
  /** @type {"T_f16"} */
  static type = "T_f16";
  toString() {
    return "f16";
  }
}

/**
 * @typedef { T_i32 | T_u32 | T_f32 | T_f16 } PrimitiveWGSLSizedType
 */

/**
 * 
 * @param {PrimitiveWGSLSizedType} T 
 */
class T_atomic {
  static type = "T_atomic";
  /** @type {PrimitiveWGSLSizedType} */
  innerType;
  toString() {
    return `atomic<${this.innerType.toString()}>`;
  }

  /**
   * @param {PrimitiveWGSLSizedType} T 
   */
  constructor(T) {
    this.innerType = T;
  }
}

/**
 * @typedef { 2 | 3 | 4 } size
 */

/**
 * 
 * @param {size} n 
 * @param {PrimitiveWGSLSizedType} T 
 * @returns 
 */
class T_vec {
  static type = "T_vec";
  /** @type {PrimitiveWGSLSizedType} */
  innerType;
  /** @type {size} */
  n;

  /**
   * 
   * @param {size} n 
   * @param {PrimitiveWGSLSizedType} T 
   */
  constructor(n, T) {
    this.n = n;
    this.innerType = T;
  }

  toString() {
    return `vec${this.n}<${this.innerType.toString()}>`;
  }
}

/**
 * 
 * @param {size} C 
 * @param {size} R
 * @param {PrimitiveWGSLSizedType} T 
 * @returns 
 */
class T_mat {
  static type = "T_mat";
  /** @type {size} */
  C;
  /** @type {size} */
  R;
  /** @type {PrimitiveWGSLSizedType} */
  innerType;

  /**
   * @param {size} C 
   * @param {size} R 
   * @param {PrimitiveWGSLSizedType} T 
   */
  constructor(C, R, T) {
    this.C = C;
    this.R = R;
    this.innerType = T;
  }

  toString() {
    return `mat${this.C}x${this.R}<${this.innerType.toString()}>`;
  }
}



class T_struct {
  static type = "T_struct";

  /**
   * @type {Array<WGSLSizedType>}
   */
  members;
  /**
   * 
   * @param  {...WGSLSizedType} members 
   */
  constructor(...members) {
    this.members = members;
  }

  toString() {
    throw "Cannot convert struct descriptor to string";
  }
}

class T_array {
  static type = "T_array";

  /**
   * @type {WGSLSizedType}
   */
  E;

  /**
   * @type {number | undefined}
   */
  N;

  /**
   * 
   * @param {WGSLSizedType} E 
   * @param {number} [N] 
   */
  constructor(E, N) {
    this.E = E;
    this.N = N;
  }

  toString() {
    return `array<${this.E}, ${this.N}>`;
  }
}

/**
 * @typedef { PrimitiveWGSLSizedType | T_atomic | T_vec | T_mat | T_struct | T_array } WGSLSizedType
 */

class T_texture_2d {
  static type = "T_texture_2d";

  sampleType;

  /**
   * 
   * @param {PrimitiveWGSLSizedType} sampleType 
   */
  constructor(sampleType) {
    this.sampleType = sampleType;
  }

  toString() {
    return `texture_2d<${this.sampleType.toString()}>`;
  }
}

class T_texture_cube {
  static type = "T_texture_cube";
  sampleType;

  /**
   * @param {PrimitiveWGSLSizedType} sampleType 
   */
  constructor(sampleType) {
    this.sampleType = sampleType;
  }

  toString() {
    return `texture_cube<${this.sampleType.toString()}>`;
  }
}

class T_sampler {
  static type = "T_sampler";

  toString() {
    return `sampler`;
  }
}

export const i32 = new T_i32();
export const u32 = new T_u32();
export const f32 = new T_f32();
export const f16 = new T_f16();
/**
 * @param {size} n 
 * @param {PrimitiveWGSLSizedType} T 
 */
export const vec = (n, T) => new T_vec(n, T);
/**
 * @param {size} C 
 * @param {size} R 
 * @param {PrimitiveWGSLSizedType} T 
 */
export const mat = (C, R, T) => new T_mat(C, R, T);
/**
 * @param  {...WGSLSizedType} M 
 */
export const struct = (...M) => {
  return new T_struct(...M);
};
/**
 * @param {WGSLSizedType} E 
 * @param {number} [N]
 * @returns 
 */
export const array = (E, N) => new T_array(E, N);

/**
 * @param {PrimitiveWGSLSizedType} T 
 * @returns 
 */
export const texture_2d = (T) => new T_texture_2d(T);
/**
 * @param {PrimitiveWGSLSizedType} T 
 * @returns 
 */
export const texture_cube = (T) => new T_texture_cube(T);

/**
 * @typedef {WGSLSizedType | T_texture_2d | T_texture_cube | T_sampler} WGSLType
 */

export const sampler = new T_sampler();
/**
 * 
 * @param {WGSLSizedType} T 
 * @returns {number}
 */
export function AlignOf(T) {
  if ( T instanceof T_i32 ||
       T instanceof T_u32 ||
       T instanceof T_f32
     ) 
  {
    return 4;
  }
  if ( T instanceof T_f16 ) 
  {
    return 2;
  }
  if ( T instanceof T_atomic ) 
  {
    return 4;
  }
  if ( T instanceof T_vec ) {
    if (T.n === 2) {
      if ( T.innerType instanceof T_i32 ||
           T.innerType instanceof T_u32 ||
           T.innerType instanceof T_f32 )
      {
        return 8;
      }
      if ( T.innerType instanceof T_f16 ) {
        return 4;
      }
    }
    if ( T.n === 3 || T.n === 4 ) {
      if ( T.innerType instanceof T_i32 ||
        T.innerType instanceof T_u32 ||
        T.innerType instanceof T_f32 )
      {
        return 16;
      }
      if ( T.innerType instanceof T_f16 ) {
        return 8;
      }
    }
  }
  if ( T instanceof T_mat ) {
    return AlignOf(new T_vec(T.R, T.innerType));
  }
  if ( T instanceof T_struct ) {
    return Math.max(...T.members.map((M) => AlignOf(M)));
  }
  if ( T instanceof T_array ) {
    return AlignOf(T.E);
  }

  console.error("Unknown type", T)
  throw `Unknown type passed to AlignOf`;
}


/**
 * https://gpuweb.github.io/gpuweb/wgsl/#roundup
 * @param {number} k 
 * @param {number} n 
 * @returns {number}
 */
function roundUp(k, n) {
  return Math.ceil(n / k) * k;
}

/**
 * https://gpuweb.github.io/gpuweb/wgsl/#structure-member-layout
 * @param {T_struct} S 
 * @param {number} i
 * @returns 
 */
function OffsetOfMember(S, i) {
  if (i === 1) {
    return 0;
  }
  return roundUp(AlignOfMember(S, i), OffsetOfMember(S, i-1) + SizeOfMember(S, i-1));
}

/**
 * 
 * @param {T_struct} S 
 * @param {number} i first index is 1!
 * @returns {number}
 */
function AlignOfMember(S, i) {
  return AlignOf(S.members[i-1]);
}

/**
 * 
 * @param {T_struct} S 
 * @param {number} i first index is 1!
 * @returns {number}
 */
function SizeOfMember(S, i) {
  return SizeOf(S.members[i-1]);
}

/**
 * 
 * @param {WGSLSizedType} T 
 * @returns {number}
 */
export function SizeOf(T) {
  if ( T instanceof T_i32 ||
       T instanceof T_u32 ||
       T instanceof T_f32
     ) 
  {
    return 4;
  }
  if ( T instanceof T_f16 ) 
  {
    return 2;
  }
  if ( T instanceof T_atomic ) 
  {
    return 4;
  }
  if ( T instanceof T_vec ) {
    if (T.n === 2) {
      if ( T.innerType instanceof T_i32 ||
           T.innerType instanceof T_u32 ||
           T.innerType instanceof T_f32 )
      {
        return 8;
      }
      if ( T.innerType instanceof T_f16 ) {
        return 4;
      }
    }
    if ( T.n === 3 ) {
      if ( T.innerType instanceof T_i32 ||
           T.innerType instanceof T_u32 ||
           T.innerType instanceof T_f32 )
      {
        return 12;
      }
      if ( T.innerType instanceof T_f16 ) {
        return 6;
      }
    }
    if ( T.n === 4 ) {
      if ( T.innerType instanceof T_i32 ||
           T.innerType instanceof T_u32 ||
           T.innerType instanceof T_f32 )
      {
        return 16;
      }
      if ( T.innerType instanceof T_f16 ) {
        return 8;
      }
    }
  }
  if ( T instanceof T_mat ) {
    return SizeOf(new T_array(new T_vec(T.R, T.innerType), T.C));
  }
  if ( T instanceof T_struct ) {
    const S = T;
    const N = S.members.length;
    const justPastLastMember = OffsetOfMember(S, N) + SizeOfMember(S, N);
    return roundUp(AlignOf(S), justPastLastMember);
  }
  if ( T instanceof T_array ) {
    if (T.N === undefined) {
      throw `Cannot give compile-time size for dynamically-sized T_array`;
    }
    return T.N * roundUp(AlignOf(T.E), SizeOf(T.E));
  }

  console.error("Unknown type", T)
  throw `Unknown type passed to AlignOf`;
}
