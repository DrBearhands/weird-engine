// @ts-check

import { TextureData } from "../../Data/Geometric/TextureData.js";
import { device } from "./WebGPUInit.js";


/**
 * 
 * @param {TextureData} textureData 
 * @param {boolean} [linear] 
 * @returns 
 */
export function textureFromData(textureData, linear = false) {
  const data = textureData.data;
  const {format, bytesPerPixel} = formatForTexture(textureData, linear);
  const isImage = data instanceof ImageBitmap;
  const usage = GPUTextureUsage.TEXTURE_BINDING | GPUTextureUsage.COPY_DST | (isImage ? GPUTextureUsage.RENDER_ATTACHMENT : 0)

  const width = textureData.xRes;
  const height = textureData.yRes;
  const texture = device.createTexture({
    dimension: "2d",
    format,
    size: [width, height, 1],
    usage
  });

  if (isImage) {
    device.queue.copyExternalImageToTexture({source : data}, { texture }, {width, height});
  }
  else {
    device.queue.writeTexture({ texture }, data, { bytesPerRow : width * bytesPerPixel}, { width, height})
  }
  
  return texture;
}

/**
 * 
 * @param {TextureData} textureData 
 * @param {boolean} [linear]
 * @returns {{bytesPerPixel : number, format : GPUTextureFormat}}
 */
function formatForTexture(textureData, linear) {
  const data = textureData.data;
  if (data instanceof ImageBitmap) {
    return { bytesPerPixel : 4, format : linear ? "rgba8unorm" : "rgba8unorm-srgb"};
  }
  if (data instanceof Uint8Array) {
    return { bytesPerPixel : 4, format : linear ? "rgba8unorm" : "rgba8unorm-srgb"};
  }
  if (data instanceof Int8Array) {
    return { bytesPerPixel : 4, format : "rgba8snorm"};
  }
  if (data instanceof Float16Array) {
    return { bytesPerPixel : 8, format : "rgba16float"};
  }
  throw "Unkown data type in TextureData";
}