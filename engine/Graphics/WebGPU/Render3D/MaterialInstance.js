// @ts-check

import Material from "./Material.js";

export default class MaterialInstance {
  /** @readonly @type {GPUBindGroup} */
  bindGroup;
  /** @readonly @type {Material} */
  material;

  /**
   * @param {Material} material
   * @param {GPUBindGroup} bindGroup 
   */
  constructor(material, bindGroup) {
    this.bindGroup = bindGroup;
    this.material = material;
  }
}