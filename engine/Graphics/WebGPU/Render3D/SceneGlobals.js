// @ts-check

import { device } from "../WebGPUInit.js";
import Camera from "./Camera.js";
import CubeMap from "./CubeMap.js";
import PBR from "./PBR.js";

export default class SceneGlobals {
  /** @readonly */
  bindGroup;
  /** @readonly */
  cameraBuffer;
  /** @readonly */
  environmentMap;

  /**
   * @param {CubeMap | undefined} environmentMap
   * @param {GPUBindGroup} bindGroup 
   * @param {GPUBuffer} cameraBuffer 
   */
  constructor(environmentMap, bindGroup, cameraBuffer) {
    this.environmentMap = environmentMap;
    this.bindGroup = bindGroup;
    this.cameraBuffer = cameraBuffer;
  }
  /** @readonly */
  static bindGroupLayout = device.createBindGroupLayout({
    label: "Scene Globals",
    entries: [
      {
        binding: 0,
        visibility: GPUShaderStage.VERTEX | GPUShaderStage.FRAGMENT,
        buffer: {
          type: "uniform",
          minBindingSize: Camera.byteSize
        }
      },
      {
        binding: 1,
        visibility: GPUShaderStage.FRAGMENT,
        texture: {
          sampleType: "float",
          viewDimension: "cube"
        }
      },
      {
        binding: 2,
        visibility: GPUShaderStage.FRAGMENT,
        sampler: {
          type: "filtering"
        }
      },
      {
        binding: 3,
        visibility: GPUShaderStage.FRAGMENT,
        texture: {
          sampleType: "float",
          viewDimension: "cube"
        }
      },
      {
        binding: 4,
        visibility: GPUShaderStage.FRAGMENT,
        sampler: {
          type: "filtering"
        }
      },
      {
        binding: 5,
        visibility: GPUShaderStage.FRAGMENT,
        texture: {
          sampleType: "float",
          viewDimension: "2d"
        }
      },
      {
        binding: 6,
        visibility: GPUShaderStage.FRAGMENT,
        sampler: {
          type: "filtering"
        }
      }
    ],
  });

  /**
   * 
   * @param {CubeMap} [environmentMap] 
   */
  static create(environmentMap) {
    const sampler = device.createSampler({
      magFilter : "linear",
      minFilter : "linear",
      mipmapFilter : "linear"
    });

    const smallDimensions = {width:128,height:128};
    const { irradianceTexture, prefilteredEnvironmentTexture} = (() => {
      if (environmentMap) {
        return {
          irradianceTexture: PBR.environmentToIrradiance(environmentMap, smallDimensions),
          prefilteredEnvironmentTexture : PBR.prefilterEnvironmentMap(environmentMap, smallDimensions)
        }
      } else {
        return {
          irradianceTexture : PBR.defaultIrradiance(),
          prefilteredEnvironmentTexture: PBR.defaultPrefilterEnvironmentMap()
        }
      }
    })();
    
    const prefilteredEnvironmentTextureView = prefilteredEnvironmentTexture.createView({dimension: "cube"});
    const irradianceTextureView = irradianceTexture.createView({dimension: "cube"});
    const BRDFIntegrationTexture = PBR.BRDFIntegrationTexture;
    const BRDFIntegrationTextureView = BRDFIntegrationTexture.createView();


    const cameraBuffer = device.createBuffer({
      size: Camera.byteSize,
      usage: GPUBufferUsage.UNIFORM | GPUBufferUsage.COPY_DST
    });
  
    const bindGroup = device.createBindGroup({
      label: "Scene Globals",
      layout: SceneGlobals.bindGroupLayout,
      entries: [
        {
          binding: 0,
          resource: {
            buffer: cameraBuffer
          }
        },
        {
          binding: 1,
          resource: irradianceTextureView
        },
        {
          binding: 2,
          resource: sampler
        },
        {
          binding: 3,
          resource: prefilteredEnvironmentTextureView
        },
        {
          binding: 4,
          resource: sampler
        },
        {
          binding: 5,
          resource: BRDFIntegrationTextureView
        },
        {
          binding: 6,
          resource: sampler
        }
      ]
    });
  
    return new SceneGlobals(environmentMap, bindGroup, cameraBuffer);
  }
}
