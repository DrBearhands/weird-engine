// @ts-check

import { write_cubemap_vertex_src } from "./CubeMap.wgsl.js";
import { BRDF_integration_map_src, environment_to_irradiance_src, prefilter_environment_map } from "./PBR.wgsl.js";
import CubeMap from "./CubeMap.js";
import { f32, SizeOf } from "../wgsl.js";
import { device } from "../WebGPUInit.js";

const irradianceMapTextureFormat = "rgba16float";
const prefilteredEnvironmentMapTextureFormat = "rgba16float";
const numSidesCube = 6;
const prefilteredEnvironmentMapMipLevelCount = 5;
const BRDFIntegrationTextureFormat = "rg16float";

const cubemapBindGroupLayout = device.createBindGroupLayout({
  label: "Equirectangular to Cube",
  entries: [
    {
      binding: 0,
      visibility: GPUShaderStage.FRAGMENT,
      texture: {
        multisampled: false,
        sampleType: "float",
        viewDimension: "cube"
      }
    },
    {
      binding: 1,
      visibility: GPUShaderStage.FRAGMENT,
      sampler: {
        type: "filtering"
      }
    }
  ]
});

const irradiancePipeline = device.createRenderPipeline({
  label: "Irradiance",
  layout: device.createPipelineLayout({
    label: "Environment to Irradiance",
    bindGroupLayouts : [cubemapBindGroupLayout, CubeMap.matrixBindGroupLayout],
  }),
  vertex: {
    module: device.createShaderModule({
      code: write_cubemap_vertex_src,
    }),
    entryPoint: 'main_vertex',
    buffers: []
  },
  fragment: {
    module: device.createShaderModule({
      code: environment_to_irradiance_src,
    }),
    entryPoint: 'main_fragment',
    targets: [
      {
        format: irradianceMapTextureFormat,
      },
    ],
  },
  primitive: {
    cullMode: 'none',
    topology: 'triangle-strip',
  }
});
const irradianceSampler = device.createSampler();

const roughnessBindGroupLayout = device.createBindGroupLayout({
  label : "Prefilter Environment Map - Roughness",
  entries: [
    {
      binding: 0,
      visibility: GPUShaderStage.FRAGMENT,
      buffer: {
        type: "uniform",
        minBindingSize: SizeOf(f32)
      }
    }
  ]
});

const roughnessBuffer = device.createBuffer({
  size : SizeOf(f32),
  usage : GPUBufferUsage.COPY_DST | GPUBufferUsage.UNIFORM
});

const roughnessBindGroup = device.createBindGroup({
  label : "Prefilter Environment Map - Roughness",
  layout: roughnessBindGroupLayout,
  entries: [
    {
      binding: 0,
      resource:
      {
        buffer: roughnessBuffer
      }
    }
  ]
});

const prefilteringPipeline = device.createRenderPipeline({
  label: "Prefilter Environment Map",
  layout: device.createPipelineLayout({
    label: "Prefilter Environment Map",
    bindGroupLayouts : [cubemapBindGroupLayout, CubeMap.matrixBindGroupLayout, roughnessBindGroupLayout],
  }),
  vertex: {
    module: device.createShaderModule({
      code: write_cubemap_vertex_src,
    }),
    entryPoint: 'main_vertex',
    buffers: []
  },
  fragment: {
    module: device.createShaderModule({
      code: prefilter_environment_map,
    }),
    entryPoint: 'main_fragment',
    targets: [
      {
        format: prefilteredEnvironmentMapTextureFormat,
      },
    ],
  },
  primitive: {
    cullMode: 'none',
    topology: 'triangle-strip',
  }
});

const BRDFIntegrationPipeline = device.createRenderPipeline({
  label: "BRDF Integration Pipeline",
  layout: device.createPipelineLayout({
    label: "BRDF Integration Pipeline",
    bindGroupLayouts: []
  }),
  vertex: {
    module: device.createShaderModule({
      code: BRDF_integration_map_src,
    }),
    entryPoint: "main_vertex",
    buffers: []
  },
  fragment: {
    module: device.createShaderModule({
      code: BRDF_integration_map_src
    }),
    entryPoint: "main_fragment",
    targets: [
      {
        format: BRDFIntegrationTextureFormat,
      },
    ],
  },
  primitive: {
    cullMode: 'none',
    topology: 'triangle-strip',
  }
});


const blackCubeData = new Float16Array([0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1]);

export default class PBR {

  /**
   * @param {CubeMap} environmentMap
   * @param {{width: number, height: number}} dimensions
   */
  static environmentToIrradiance(environmentMap, dimensions) {

    device.pushErrorScope("validation");
    const commandEncoder = device.createCommandEncoder();
    const matrixBindGroups = CubeMap.matrixBindGroups;

    const irradianceTextureCube = device.createTexture({
      dimension: "2d",
      format: irradianceMapTextureFormat,
      size: [dimensions.width, dimensions.height, 6],
      usage : GPUTextureUsage.TEXTURE_BINDING | GPUTextureUsage.COPY_DST | GPUTextureUsage.RENDER_ATTACHMENT
    });

    for (let side = 0; side < numSidesCube; ++side) {

      const passEncoder = commandEncoder.beginRenderPass({
        colorAttachments: [
          {
            clearValue: {r : 0, g:0, b: 0, a:1},
            loadOp: "clear",
            storeOp: "store",
            view: irradianceTextureCube.createView({
              dimension: '2d',
              baseArrayLayer: side,
              arrayLayerCount: 1
            }),
          },
        ]
      });
      passEncoder.setPipeline(irradiancePipeline);
      passEncoder.setBindGroup(0, environmentMap.bindGroup);
      passEncoder.setBindGroup(1, matrixBindGroups[side]);
      passEncoder.draw(4);
      passEncoder.end();
    }
    device.queue.submit([commandEncoder.finish()]);
    device.popErrorScope().then((err) => {
      if (err) {
        console.error("Error in PBR.environmentToIrradiance");
        console.error(err);
      }
    });

    return irradianceTextureCube;
  }

  static defaultIrradiance() {
    const res = device.createTexture({
      dimension: "2d",
      format: irradianceMapTextureFormat,
      size: [1, 1, 6],
      usage : GPUTextureUsage.TEXTURE_BINDING | GPUTextureUsage.COPY_DST | GPUTextureUsage.RENDER_ATTACHMENT
    });
    device.queue.writeTexture({texture: res, mipLevel: 0}, blackCubeData, {bytesPerRow: 8, rowsPerImage:1}, [1,1,6])
    return res;
  }
  /**
   * @todo: create 6 roughness bind groups and never write to them again
   * @param {CubeMap} environmentMap 
   * @param {{width: number, height: number}} dimensions 
   */
  static prefilterEnvironmentMap(environmentMap, dimensions) {
    const roughness = new Float32Array(1);
    const matrixBindGroups = CubeMap.matrixBindGroups;

    device.pushErrorScope("validation");

    const textureCube = device.createTexture({
      dimension: "2d",
      format: irradianceMapTextureFormat,
      size: [dimensions.width, dimensions.height, 6],
      usage : GPUTextureUsage.TEXTURE_BINDING | GPUTextureUsage.COPY_DST | GPUTextureUsage.RENDER_ATTACHMENT,
      mipLevelCount : prefilteredEnvironmentMapMipLevelCount
    });

    for (let side = 0; side < numSidesCube; ++side) {
      for (let mipLevel = 0; mipLevel < prefilteredEnvironmentMapMipLevelCount; ++mipLevel) {
        roughness[0] = mipLevel / prefilteredEnvironmentMapMipLevelCount;

        device.queue.writeBuffer(roughnessBuffer, 0, roughness);

        const commandEncoder = device.createCommandEncoder({
          label: "Prefilter Environment Map"
        });

        const passEncoder = commandEncoder.beginRenderPass({
          colorAttachments: [
            {
              clearValue: {r : 0, g:0, b: 0, a:1},
              loadOp: "clear",
              storeOp: "store",
              view: textureCube.createView({
                dimension: '2d',
                baseArrayLayer: side,
                arrayLayerCount: 1,
                baseMipLevel: mipLevel,
                mipLevelCount: 1
              }),
            },
          ]
        });
        passEncoder.setPipeline(prefilteringPipeline);
        passEncoder.setBindGroup(0, environmentMap.bindGroup);
        passEncoder.setBindGroup(1, matrixBindGroups[side]);
        passEncoder.setBindGroup(2, roughnessBindGroup);
        passEncoder.draw(4);
        passEncoder.end();
        device.queue.submit([commandEncoder.finish()]);
      }

    }

    device.popErrorScope().then(err => {
      if (err) {
        console.error("Error in PBR.prefilterEnvironmentMap()");
        console.error(err);
      }
    });

    return textureCube;
  }

  static defaultPrefilterEnvironmentMap() {
    const res = device.createTexture({
      dimension: "2d",
      format: irradianceMapTextureFormat,
      size: [1, 1, 6],
      usage : GPUTextureUsage.TEXTURE_BINDING | GPUTextureUsage.COPY_DST | GPUTextureUsage.RENDER_ATTACHMENT,
      mipLevelCount : 1
    });
    device.queue.writeTexture({texture: res}, blackCubeData, {bytesPerRow: 8, rowsPerImage:1}, [1,1,6]);
    return res;
  }

  /** @readonly */
  static BRDFIntegrationTexture = (() => {
    const texture = device.createTexture({
      dimension: "2d",
      format: BRDFIntegrationTextureFormat,
      size: [512, 512, 1],
      usage : GPUTextureUsage.TEXTURE_BINDING | GPUTextureUsage.COPY_DST | GPUTextureUsage.RENDER_ATTACHMENT,
    });

    const commandEncoder = device.createCommandEncoder({
      label: "BRDF Integration Texture"
    });

    const passEncoder = commandEncoder.beginRenderPass({
      colorAttachments: [
        {
          clearValue: {r : 1, g:0, b: 0, a:1},
          loadOp: "clear",
          storeOp: "store",
          view: texture.createView(),
        },
      ]
    });
    passEncoder.setPipeline(BRDFIntegrationPipeline);
    passEncoder.draw(4);
    passEncoder.end();
    device.queue.submit([commandEncoder.finish()]);

    return texture;
  })();
}