// @ts-check

import MaterialInstance from "../MaterialInstance.js";
import HeightmapMesh from "./HeightmapMesh.js";

export default class HeightmapMeshInstance {
  heightmapMesh;
  /** @type {MaterialInstance?} */
  materialInstance;

  /**
   * @param {HeightmapMesh} heightmapMesh 
   */
  constructor(heightmapMesh) {
    this.heightmapMesh = heightmapMesh;
  }
}