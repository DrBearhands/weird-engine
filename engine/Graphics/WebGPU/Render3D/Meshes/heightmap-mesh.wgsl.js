// @ts-check

import { nameGenerator } from "../../../../Algorithms/UniqueVarNameGenerator.js";
import * as ShadersBase from "../ShaderFragments/shaders-base.wgsl.js";

export const names = {
  /** @readonly */
  global : {
    /** @readonly */
    TerrainMetaData : nameGenerator.generate("TerrainMetaData"),
    /** @readonly */
    terrain_metadata : nameGenerator.generate("terrain_metadata"),
    /** @readonly */
    terrain_heightmap : nameGenerator.generate("terrain_heightmap"),
    /** @readonly */
    calc_terrain_position : nameGenerator.generate("calc_terrain_position"),
    /** @readonly */
    global_id_to_array_index : nameGenerator.generate("global_id_to_array_index")
  },
  /** @readonly */
  terrain_metadata_members : {
    /** @readonly */
    world_size : "world_size",
    /** @readonly */
    resolution : "resolution"
  }
}

/**
 * @param {{
 *   group : number,
 *   binding : number
 * }} bindings
 * @returns {string}
 */
const terrain_uniforms_src = (bindings) => {
  const TerrainMetaData = names.global.TerrainMetaData;
  const terrain_metadata = names.global.terrain_metadata;
  const group = bindings.group;
  const binding = bindings.binding;
  return /* wgsl */ `
    struct ${TerrainMetaData} {
      ${names.terrain_metadata_members.world_size} : f32,
      ${names.terrain_metadata_members.resolution} : f32,
    }

    @group(${group}) @binding(${binding}) 
    var<uniform> ${terrain_metadata} : ${TerrainMetaData};
  `;
}

/**
 * @param {{
 *   group_object : number,
 *   binding_terrain_metadata : number
 * }} bindings
 * @returns {string}
 */
const render_base_src = (bindings) => {
  const terrain_metadata = names.global.terrain_metadata;
  const calc_terrain_position = names.global.calc_terrain_position;

  const world_size = names.terrain_metadata_members.world_size;
  const resolution = names.terrain_metadata_members.resolution;
  
  return /* wgsl */ `
    ${terrain_uniforms_src({group : bindings.group_object, binding : bindings.binding_terrain_metadata})}

    fn ${calc_terrain_position}(index : u32, height : f32) -> vec3<f32> {
      let size = ${terrain_metadata}.${world_size};
      let vert_count = ${terrain_metadata}.${resolution};

      // Note, module and integer OPS in general are slow
      let x_vertex_index : f32 = floor(f32(index) / vert_count);
      let x_position = x_vertex_index * (size / vert_count);
      let z_vertex_index : f32 = f32(index) - (x_vertex_index*vert_count);
      let z_position = z_vertex_index * (size / vert_count);

      let position : vec3<f32> = vec3<f32>(x_position, height, z_position);

      return position;
    }

    fn calc_tangent(n : vec3<f32>) -> vec4<f32> {
      return vec4<f32>(normalize(cross(n, vec3<f32>(0.0, 0.0, 1.0))), 1.0);
    }
`
};

/**
 * @param {{
 *   heightmap : {
 *     group : number,
 *     binding : number
 *   },
 *   terrain_metadata : {
 *     group : number,
 *     binding : number
 *   }
 *  }} bindings
 * @returns {string}
 */
export const compute_base_src = (bindings) => {
  const terrain_heightmap = names.global.terrain_heightmap;
  const terrain_metadata = names.global.terrain_metadata;
  const global_id_to_array_index = names.global.global_id_to_array_index;
  const resolution = names.terrain_metadata_members.resolution;
  
  
  return /* wgsl */ `
    ${terrain_uniforms_src(bindings.terrain_metadata)}

    @group(${bindings.heightmap.group}) @binding(${bindings.heightmap.binding})
    var<storage, read_write> ${terrain_heightmap} : array<f32>;

    fn ${global_id_to_array_index}(id : vec2<u32>) -> u32 {
      return (id.x * u32(${terrain_metadata}.${resolution}) + id.y) * 4;
    }
`};



/**
 * @param {{
*   group_object : number,
*   binding_terrain_metadata : number,
* }} bindings
* @returns 
*/
export const src_normals = (bindings) => {
 const calc_terrain_position = names.global.calc_terrain_position;
 return /* wgsl */ `
   ${render_base_src(bindings)}

   @vertex
   fn main_vertex(
     @builtin(vertex_index) vertex_index : u32,
     @builtin(instance_index) instance_index : u32,
     @location(0) height : f32,
     @location(1) normal : vec3<f32>
   ) -> ${ShadersBase.names.VertexOutput} {
     var world_position = vec4<f32>(${calc_terrain_position}(instance_index, height), 1.0);
     let tangent = calc_tangent(normal);

     if (vertex_index == 1) {
       world_position += vec4(normal, 0.0) * 0.1;
     }
     if (vertex_index == 3) {
       world_position += vec4(tangent.xyz, 0.0) * 0.1;
     }
     if (vertex_index == 5) {
       let bitangent = cross(normal, tangent.xyz) * tangent.w;
       world_position += vec4(bitangent, 0.0) * 0.1;
     }
     var vertex_color = vec4<f32>(0.0, 1.0, 0.0, 1.0);
     if (vertex_index < 4) {
       vertex_color = vec4<f32>(1.0, 0.0, 0.0, 1.0);
     }
     if (vertex_index < 2) {
       vertex_color = vec4<f32>(0.0, 0.0, 1.0, 1.0);
     }

     ${ShadersBase.writeOutputs("world_position", "normal", "tangent", "vec2<f32>(0.0, 0.0)", "vertex_color")}
   }
 `;
};


/**
 * @param {{
*   group_object : number,
*   binding_terrain_metadata : number,
* }} bindings
* @returns 
*/
export const src = (bindings) => {
 const calc_terrain_position = names.global.calc_terrain_position;

 return /* wgsl */ `
   ${render_base_src(bindings)}

   @vertex
   fn main_vertex(
     @builtin(vertex_index) index : u32,
     @location(0) height : f32,
     @location(1) normal : vec3<f32>,
   ) -> ${ShadersBase.names.VertexOutput} {
     
     let position = vec4<f32>(${calc_terrain_position}(
       index, 
       height
     ), 1.0);

     let tangent = calc_tangent(normal);
     
     ${ShadersBase.writeOutputs("position", "normal", "tangent", "vec2<f32>(0.0, 0.0)", "position*(1.0/250.0)")}
   }
 `;
};