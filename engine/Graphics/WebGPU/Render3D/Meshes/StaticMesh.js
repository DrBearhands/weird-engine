// @ts-check

import { incrementToMultiple, interleave } from "../../../../utils.js";
import StaticMeshData from "../../../../Data/Geometric/Meshes/StaticMesh.js";
import * as ShadersBase from "../ShaderFragments/shaders-base.wgsl.js";
import { f32, mat, SizeOf, struct } from "../../wgsl.js";
import { device } from "../../WebGPUInit.js";
import StaticMeshInstance from "./StaticMeshInstance.js";

import { nameGenerator } from "../../../../Algorithms/UniqueVarNameGenerator.js";

const binding_T = 0;

const byteSize_T = SizeOf(struct(mat(4,4, f32), mat(3,3, f32)));

/**
 * 
 * @param {"vertex" | "instance"} stepMode 
 * @returns {GPUVertexBufferLayout[]}
 */
function defineVertexBuffers(stepMode) {
  return [
    {
      attributes: [
        {
          shaderLocation: 0,
          offset: 0,
          format: "float32x3",
        },
        {
          shaderLocation: 1, // normal
          offset: 12,
          format: "float32x3",
        },
        {
          shaderLocation: 2, // tangent
          offset: 24,
          format: "float32x4"
        },
        {
          shaderLocation: 3, // tex coord
          offset: 40,
          format: "float32x2"
        },
        {
          shaderLocation: 4, // color
          offset: 48,
          format: "float32x4"
        }
      ],
      arrayStride: 64,
      /** @type {"vertex" | "instance"} */
      stepMode,
    },
  ];
}



export default class StaticMesh {
  /** @readonly @type {number} */
  drawCount;
  /** @readonly @type {number} */
  vertexCount;
  /** @readonly @type {GPUBuffer} */
  vertexBuffer;
  /** @readonly @type {GPUBuffer} */
  indexBuffer;

  /**
   * @param {number} drawCount 
   * @param {number} vertexCount 
   * @param {GPUBuffer} vertexBuffer 
   * @param {GPUBuffer} indexBuffer 
   */
  constructor(drawCount, vertexCount, vertexBuffer, indexBuffer) {
    this.drawCount = drawCount;
    this.vertexCount = vertexCount;
    this.vertexBuffer = vertexBuffer;
    this.indexBuffer = indexBuffer;
  }

  /**
   * @param {StaticMeshData} mesh
   * @returns {StaticMesh}
   */
  static create(mesh) {
    const indices = mesh.indices;
    const drawCount = mesh.indices.length;
    
    const {data : vertices, nVerts : vertexCount} = interleave(
      { 
        data : mesh.positions,
        elementSize : 3
      },
      {
        data : mesh.normals,
        elementSize : 3
      },
      {
        data : mesh.tangents,
        elementSize : 4
      },
      {
        data : mesh.texcoords[0],
        elementSize: 2
      },
      {
        data : mesh.colors[0],
        elementSize: 4
      }
    );
    
    const vertexBuffer = device.createBuffer({
      size: incrementToMultiple(vertices.byteLength, 4),
      usage: GPUBufferUsage.VERTEX | GPUBufferUsage.COPY_DST,
    });
    device.queue.writeBuffer(vertexBuffer, 0, vertices);
    
    const indexBuffer = device.createBuffer({
      size: incrementToMultiple(mesh.indices.byteLength, 4),
      usage: GPUBufferUsage.COPY_DST | GPUBufferUsage.INDEX
    });
    device.queue.writeBuffer(indexBuffer, 0, indices);

    return new StaticMesh(drawCount, vertexCount, vertexBuffer, indexBuffer);
  
  }

  /**
   * @returns {StaticMeshInstance}
   */
  createInstance() {
    const buffer = device.createBuffer({
      usage: GPUBufferUsage.UNIFORM | GPUBufferUsage.COPY_DST,
      size: byteSize_T
    });

    const bindGroup = device.createBindGroup({
      layout: StaticMesh.bindGroupLayout,
      entries: [
        {
          binding: binding_T,
          resource: {
            buffer: buffer
          }
        }
      ]
    });

    return new StaticMeshInstance(this, bindGroup, buffer);
  }

  /**
   * 
   * @param {number} group_object
   * @returns {string}
   */
  static createShaderSource(group_object) {
    return shaders_base({group_object, binding_T}, "")
  }

  /**
   * 
   * @param {number} group_object
   * @returns {string}
   */
  static createShaderSourceNormals(group_object) {
    return shaders_base({group_object, binding_T}, /* wgsl */ `
      if (vertexIndex == 1) {
        world_position += vec4(world_normal, 0.0) * 0.1;
      }
      if (vertexIndex == 3) {
        world_position += vec4(world_tangent.xyz, 0.0) * 0.1;
      }
      if (vertexIndex == 5) {
        let bitangent = cross(world_normal, world_tangent.xyz) * world_tangent.w;
        world_position += vec4(bitangent, 0.0) * 0.1;
      }
      vertex_color = vec4<f32>(0.0, 1.0, 0.0, 1.0);
      if (vertexIndex < 4) {
        vertex_color = vec4<f32>(1.0, 0.0, 0.0, 1.0);
      }
      if (vertexIndex < 2) {
        vertex_color = vec4<f32>(0.0, 0.0, 1.0, 1.0);
      }
    `);
  }

  /** @readonly */
  static vertexBuffers = defineVertexBuffers("vertex");
  /** @readonly */
  static vertexBuffers_normals = defineVertexBuffers("instance");
  /** @readonly */
  static bindGroupLayout = device.createBindGroupLayout({
    label: "Static Mesh Bind Group Layout",
    entries: [
      {
        binding: binding_T,
        visibility : GPUShaderStage.VERTEX,
        buffer: {
          type: "uniform",
          minBindingSize: byteSize_T
        }
      }
    ]
  });
}

export const names = {
  global : {
    Transform : nameGenerator.generate("Transform"),
    T : nameGenerator.generate("T"),
  },
  Transform : {
    O : "O",
    N: "N"
  }
}

/**
 * @param {{
*   group_object : number,
*   binding_T : number
* }} bindings 
* @param {string} alterPosition
* @returns {string}
*/
const shaders_base = (bindings, alterPosition) =>
{
 const Transform = names.global.Transform;
 const T = names.global.T;

 return /* wgsl */ `
   
   struct ${Transform} {
     O : mat4x4<f32>,
     N : mat3x3<f32>
   }

   @group(${bindings.group_object})
   @binding(${bindings.binding_T})
   var<uniform> ${T} : ${Transform};



   @vertex
   fn main_vertex(
     @builtin(vertex_index) vertexIndex : u32,
     @location(0) position : vec3<f32>,
     @location(1) normal : vec3<f32>,
     @location(2) tangent : vec4<f32>,
     @location(3) tex_coord : vec2<f32>,
     @location(4) color : vec4<f32>
   ) -> ${ShadersBase.names.VertexOutput} {

     var world_position = ${T}.O * vec4<f32>(position, 1.0);

     var world_normal = ${T}.N * normal;

     var world_tangent = vec4<f32>(${T}.N * tangent.xyz, tangent.w);

     var vertex_color = vec4<f32>(0.0, 0.0, 0.0, 1.0);

     ${alterPosition}

     ${ShadersBase.writeOutputs("world_position", "world_normal", "world_tangent", "tex_coord", "color")}
   }
 `;
}