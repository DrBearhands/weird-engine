// @ts-check

import { incrementToMultiple, interleave } from "../../../../utils.js";
import { F32_BYTES, U32_BYTES } from "../../../../Data/byte-sizes.js";
import { device } from "../../WebGPUInit.js";
import { f32, SizeOf, struct } from "../../wgsl.js";
import HeightmapMeshInstance from "./HeightmapMeshInstance.js";

import { nameGenerator } from "../../../../Algorithms/UniqueVarNameGenerator.js";
import * as ShadersBase from "../ShaderFragments/shaders-base.wgsl.js";

const binding_terrain_metadata = 0;

const type_terrain_metadata = struct(f32, f32);

export default class HeightmapMesh {
  /** @readonly @type {GPUBindGroup} */
  renderBindGroup;
  /** @readonly @type {GPUBuffer} */
  uniformBuffer;
  /** @readonly @type {GPUBuffer} */
  vertexBuffer;
  /** @readonly @type {GPUBuffer} */
  indexBuffer;
  /** @readonly @type {number} */
  drawCount;
  /** @readonly @type {number} */
  vertexCount;

  /**
   * @param {GPUBindGroup} renderBindGroup 
   * @param {GPUBuffer} uniformBuffer 
   * @param {GPUBuffer} vertexBuffer 
   * @param {GPUBuffer} indexBuffer 
   * @param {number} drawCount 
   * @param {number} vertexCount 
   */
  constructor(renderBindGroup, uniformBuffer, vertexBuffer, indexBuffer, drawCount, vertexCount) {
    this.renderBindGroup = renderBindGroup;
    this.uniformBuffer = uniformBuffer;
    this.vertexBuffer = vertexBuffer;
    this.indexBuffer = indexBuffer;
    this.drawCount = drawCount;
    this.vertexCount = vertexCount;
  }

  /** @readonly */
  static size_terrain_metadata = SizeOf(type_terrain_metadata);

  /**
   * @param {number} sqrtNumVerts
   * @param {number} worldSize
   * @returns {HeightmapMesh}
   */
  static alloc (sqrtNumVerts, worldSize) {
    const numVerts = sqrtNumVerts*sqrtNumVerts;
    const vertexBuffer = device.createBuffer({
      size: incrementToMultiple(numVerts*4*F32_BYTES, 4),
      usage: GPUBufferUsage.COPY_DST | GPUBufferUsage.VERTEX | GPUBufferUsage.STORAGE
    });
    const sqrtNumQuads = sqrtNumVerts-1;
    const numIndices = sqrtNumQuads*sqrtNumQuads*6;
    
    const indexBuffer = device.createBuffer({
      size: incrementToMultiple(numIndices*U32_BYTES, 4),
      usage: GPUBufferUsage.COPY_DST | GPUBufferUsage.INDEX
    });
  
    const indices = new Uint32Array(numIndices);
    
    for (let x = 0; x < sqrtNumQuads; ++x) {
      for (let z = 0; z < sqrtNumQuads; ++z) {
        const ii = (x*sqrtNumQuads+z)*6;
        
        const i0 = (x*sqrtNumVerts + z);
        const i1 = (x*sqrtNumVerts + z+1);
        const i2 = ((x+1)*sqrtNumVerts + z);
        const i3 = ((x+1)*sqrtNumVerts + z+1);
        
        indices[ii+0] = i0;
        indices[ii+1] = i1;
        indices[ii+2] = i2;
        
        indices[ii+3] = i1;
        indices[ii+4] = i3;
        indices[ii+5] = i2;
      }
    }
  
    device.queue.writeBuffer(indexBuffer, 0, indices);
  
    const uniformBuffer = device.createBuffer({
      size: F32_BYTES*2,
      usage: GPUBufferUsage.UNIFORM | GPUBufferUsage.COPY_DST
    });
    const terrainUniformData = new Float32Array(2);
    terrainUniformData[0] = worldSize;
    terrainUniformData[1] = Math.sqrt(numVerts);
  
    device.queue.writeBuffer(
      uniformBuffer,
      0,
      terrainUniformData,
    );
  
    const renderBindGroup = device.createBindGroup({
      label: "HeightmapMesh - render",
      layout: HeightmapMesh.bindGroupLayout,
      entries: [
        {
          binding: binding_terrain_metadata,
          resource: {
            buffer: uniformBuffer
          }
        }
      ]
    });
  
    return new HeightmapMesh(renderBindGroup, uniformBuffer, vertexBuffer, indexBuffer, indices.length, numVerts);
  }

  /**
   * 
   * @param {HeightmapMesh} mesh 
   * @param {Float32Array} heights 
   * @param {Float32Array} normals 
   * @param {number} worldSize 
   */
  static setData(mesh, heights, normals, worldSize) {
    const {data: vertices, nVerts: vertexCount} = interleave(
      { data: heights, elementSize: 1 }, 
      { data: normals, elementSize: 3 }
    );

    device.queue.writeBuffer(mesh.vertexBuffer, 0, vertices);

    const terrainUniformData = new Float32Array(2);
    terrainUniformData[0] = worldSize;
    terrainUniformData[1] = Math.sqrt(mesh.vertexCount);

    device.queue.writeBuffer(
      mesh.uniformBuffer,
      0,
      terrainUniformData,
    );
  }

  createInstance() {
    return new HeightmapMeshInstance(this);
  }

  /** @readonly */
  static vertexBufferLayout = [
    {
      attributes: [
        {
          shaderLocation: 0,
          offset: 0,
          /** @type {"float32"} */
          format: "float32",
        },
        {
          shaderLocation: 1, // normal
          offset: 4,
          /** @type {"float32x3"} */
          format: "float32x3",
        },
      ],
      arrayStride: 16,
      /** @type {"vertex"} */
      stepMode: "vertex",
    },
  ];
  
  /** @readonly */
  static vertexBufferLayout_normals = [
    {
      attributes: [
        {
          shaderLocation: 0,
          offset: 0,
          /** @type {"float32"} */
          format: "float32",
        },
        {
          shaderLocation: 1, // normal
          offset: 4,
          /** @type {"float32x3"} */
          format: "float32x3",
        },
      ],
      arrayStride: 16,
      /** @type {"instance"} */
      stepMode: "instance",
    }
  ]
  
  /** @readonly */
  static bindGroupLayout = device.createBindGroupLayout({
    label: "HeightmapMeshLayout::terrainMetadataBindGroupLayout",
    entries: [
      {
        binding: binding_terrain_metadata,
        visibility: GPUShaderStage.VERTEX,
        buffer: {
          type: "uniform",
          minBindingSize: HeightmapMesh.size_terrain_metadata
        }
      }
    ]
  });

  /**
   * 
   * @param {number} group_object
   * @returns {string}
   */
  static shader_src(group_object) {
    return src({
      group_object,
      binding_terrain_metadata
    });
  }

  /**
   * 
   * @param {number} group_object
   * @returns {string}
   */
  static shader_normals_src(group_object) {
    return src_normals({
      group_object,
      binding_terrain_metadata
    })
  }


}


export const names = {
  /** @readonly */
  global : {
    /** @readonly */
    TerrainMetaData : nameGenerator.generate("TerrainMetaData"),
    /** @readonly */
    terrain_metadata : nameGenerator.generate("terrain_metadata"),
    /** @readonly */
    terrain_heightmap : nameGenerator.generate("terrain_heightmap"),
    /** @readonly */
    calc_terrain_position : nameGenerator.generate("calc_terrain_position"),
    /** @readonly */
    global_id_to_array_index : nameGenerator.generate("global_id_to_array_index")
  },
  /** @readonly */
  terrain_metadata_members : {
    /** @readonly */
    world_size : "world_size",
    /** @readonly */
    resolution : "resolution"
  }
}

/**
 * @param {{
 *   group : number,
 *   binding : number
 * }} bindings
 * @returns {string}
 */
const terrain_uniforms_src = (bindings) => {
  const TerrainMetaData = names.global.TerrainMetaData;
  const terrain_metadata = names.global.terrain_metadata;
  const group = bindings.group;
  const binding = bindings.binding;
  return /* wgsl */ `
    struct ${TerrainMetaData} {
      ${names.terrain_metadata_members.world_size} : f32,
      ${names.terrain_metadata_members.resolution} : f32,
    }

    @group(${group}) @binding(${binding}) 
    var<uniform> ${terrain_metadata} : ${TerrainMetaData};
  `;
}

/**
 * @param {{
 *   group_object : number,
 *   binding_terrain_metadata : number
 * }} bindings
 * @returns {string}
 */
const render_base_src = (bindings) => {
  const terrain_metadata = names.global.terrain_metadata;
  const calc_terrain_position = names.global.calc_terrain_position;

  const world_size = names.terrain_metadata_members.world_size;
  const resolution = names.terrain_metadata_members.resolution;
  
  return /* wgsl */ `
    ${terrain_uniforms_src({group : bindings.group_object, binding : bindings.binding_terrain_metadata})}

    fn ${calc_terrain_position}(index : u32, height : f32) -> vec3<f32> {
      let size = ${terrain_metadata}.${world_size};
      let vert_count = ${terrain_metadata}.${resolution};

      // Note, module and integer OPS in general are slow
      let x_vertex_index : f32 = floor(f32(index) / vert_count);
      let x_position = x_vertex_index * (size / vert_count);
      let z_vertex_index : f32 = f32(index) - (x_vertex_index*vert_count);
      let z_position = z_vertex_index * (size / vert_count);

      let position : vec3<f32> = vec3<f32>(x_position, height, z_position);

      return position;
    }

    fn calc_tangent(n : vec3<f32>) -> vec4<f32> {
      return vec4<f32>(normalize(cross(n, vec3<f32>(0.0, 0.0, 1.0))), 1.0);
    }
`
};

/**
 * @param {{
 *   heightmap : {
 *     group : number,
 *     binding : number
 *   },
 *   terrain_metadata : {
 *     group : number,
 *     binding : number
 *   }
 *  }} bindings
 * @returns {string}
 */
export const compute_base_src = (bindings) => {
  const terrain_heightmap = names.global.terrain_heightmap;
  const terrain_metadata = names.global.terrain_metadata;
  const global_id_to_array_index = names.global.global_id_to_array_index;
  const resolution = names.terrain_metadata_members.resolution;
  
  
  return /* wgsl */ `
    ${terrain_uniforms_src(bindings.terrain_metadata)}

    @group(${bindings.heightmap.group}) @binding(${bindings.heightmap.binding})
    var<storage, read_write> ${terrain_heightmap} : array<f32>;

    fn ${global_id_to_array_index}(id : vec2<u32>) -> u32 {
      return (id.x * u32(${terrain_metadata}.${resolution}) + id.y) * 4;
    }
`};



/**
 * @param {{
*   group_object : number,
*   binding_terrain_metadata : number,
* }} bindings
* @returns 
*/
const src_normals = (bindings) => {
 const calc_terrain_position = names.global.calc_terrain_position;
 return /* wgsl */ `
   ${render_base_src(bindings)}

   @vertex
   fn main_vertex(
     @builtin(vertex_index) vertex_index : u32,
     @builtin(instance_index) instance_index : u32,
     @location(0) height : f32,
     @location(1) normal : vec3<f32>
   ) -> ${ShadersBase.names.VertexOutput} {
     var world_position = vec4<f32>(${calc_terrain_position}(instance_index, height), 1.0);
     let tangent = calc_tangent(normal);

     if (vertex_index == 1) {
       world_position += vec4(normal, 0.0) * 0.1;
     }
     if (vertex_index == 3) {
       world_position += vec4(tangent.xyz, 0.0) * 0.1;
     }
     if (vertex_index == 5) {
       let bitangent = cross(normal, tangent.xyz) * tangent.w;
       world_position += vec4(bitangent, 0.0) * 0.1;
     }
     var vertex_color = vec4<f32>(0.0, 1.0, 0.0, 1.0);
     if (vertex_index < 4) {
       vertex_color = vec4<f32>(1.0, 0.0, 0.0, 1.0);
     }
     if (vertex_index < 2) {
       vertex_color = vec4<f32>(0.0, 0.0, 1.0, 1.0);
     }

     ${ShadersBase.writeOutputs("world_position", "normal", "tangent", "vec2<f32>(0.0, 0.0)", "vertex_color")}
   }
 `;
};


/**
 * @param {{
*   group_object : number,
*   binding_terrain_metadata : number,
* }} bindings
* @returns 
*/
const src = (bindings) => {
 const calc_terrain_position = names.global.calc_terrain_position;

 return /* wgsl */ `
   ${render_base_src(bindings)}

   @vertex
   fn main_vertex(
     @builtin(vertex_index) index : u32,
     @location(0) height : f32,
     @location(1) normal : vec3<f32>,
   ) -> ${ShadersBase.names.VertexOutput} {
     
     let position = vec4<f32>(${calc_terrain_position}(
       index, 
       height
     ), 1.0);

     let tangent = calc_tangent(normal);
     
     ${ShadersBase.writeOutputs("position", "normal", "tangent", "vec2<f32>(0.0, 0.0)", "position*(1.0/250.0)")}
   }
 `;
};