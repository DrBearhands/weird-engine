// @ts-check

import HeightmapMesh from "./HeightmapMesh.js";
import StaticMesh from "./StaticMesh.js";

export default class Meshes {
  /** @type {Array<StaticMesh>} */
  static = [];
  /** @type {Array<HeightmapMesh>} */
  heightmap = [];
}

/**
 * @template StaticMeshCase
 * @template HeightmapMeshCase
 */
export class EachMeshType {
  /** @type {StaticMeshCase} */
  static;
  /** @type {HeightmapMeshCase} */
  heightmap;

  /**
   * 
   * @param {{
   *   static : StaticMeshCase,
   *   heightmap : HeightmapMeshCase
   * }} cases
   */
  constructor(cases) {
    this.static = cases.static;
    this.heightmap = cases.heightmap;
  }
}