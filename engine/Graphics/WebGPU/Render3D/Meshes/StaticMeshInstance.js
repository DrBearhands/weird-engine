// @ts-check

import MaterialInstance from "../MaterialInstance.js";
import Transform from "../Transform.js";
import StaticMesh from "./StaticMesh.js";

/**
 * @todo: should not inherit Transform, could have non-transform properties to be shared across multiple transforms. Might not be useful though.
 */
export default class StaticMeshInstance extends Transform {
  bindGroup;
  staticMesh;
  /** @type {MaterialInstance?} */
  materialInstance = null;

  /**
   * @param {StaticMesh} staticMesh
   * @param {GPUBindGroup} bindGroup 
   * @param {GPUBuffer} transformBuffer 
   */
  constructor(staticMesh, bindGroup, transformBuffer) {
    super(transformBuffer)
    this.staticMesh = staticMesh;
    this.bindGroup = bindGroup;
  }
}