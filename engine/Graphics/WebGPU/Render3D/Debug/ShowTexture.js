// @ts-check

/**
 * 
 * @param {GPUDevice} device 
 */
export function showTexture(device, presentationFormat,colorAttachments, texture) {
  const module = device.createShaderModule({
    label: 'our hardcoded textured quad shaders',
    code: /* wgsl */ `
      struct OurVertexShaderOutput {
        @builtin(position) position: vec4f,
        @location(0) texcoord: vec2f,
      };

      @vertex fn vs(
        @builtin(vertex_index) vertexIndex : u32
      ) -> OurVertexShaderOutput {
        let pos = array(
          // 1st triangle
          vec2f(-1.0, -1.0),  // center
          vec2f( 1.0, -1.0),  // right, center
          vec2f(-1.0,  1.0),  // center, top

          // 2st triangle
          vec2f(-1.0,  1.0),  // center, top
          vec2f( 1.0, -1.0),  // right, center
          vec2f( 1.0,  1.0),  // right, top
        );

        var vsOutput: OurVertexShaderOutput;
        let xy = pos[vertexIndex];
        vsOutput.position = vec4f(xy, 0.0, 1.0);
        vsOutput.texcoord = (xy+1.0)/2.0;
        return vsOutput;
      }

      @group(0) @binding(0) var ourSampler: sampler;
      @group(0) @binding(1) var ourTexture: texture_2d<f32>;

      @fragment fn fs(fsInput: OurVertexShaderOutput) -> @location(0) vec4f {
        return textureSample(ourTexture, ourSampler, fsInput.texcoord);
      }
    `,
  });

  const pipeline = device.createRenderPipeline({
    label: 'hardcoded textured quad pipeline',
    layout: 'auto',
    vertex: {
      module,
      entryPoint: "vs"
    },
    fragment: {
      module,
      targets: [{ format: presentationFormat }],
      entryPoint : "fs"
    },
  });

  const sampler = device.createSampler();

  const bindGroup = device.createBindGroup({
    layout: pipeline.getBindGroupLayout(0),
    entries: [
      { binding: 0, resource: sampler },
      { binding: 1, resource: texture.createView() },
    ],
  });

  const encoder = device.createCommandEncoder({
    label: 'render quad encoder',
  });
  const pass = encoder.beginRenderPass({
    label: 'our basic canvas renderPass',
    colorAttachments
  });
  pass.setPipeline(pipeline);
  pass.setBindGroup(0, bindGroup);
  pass.draw(6);  // call our vertex shader 6 times
  pass.end();

  const commandBuffer = encoder.finish();
  device.queue.submit([commandBuffer]);
}