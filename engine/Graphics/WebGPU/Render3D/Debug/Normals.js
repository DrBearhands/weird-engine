// @ts-check
import HeightmapMesh from "../Meshes/HeightmapMesh.js";
import StaticMesh from "../Meshes/StaticMesh.js";
import { binding_camera, normals_group_global, normals_group_object } from "../config.js";
import { globals_src } from "../SceneGlobals.wgsl.js";
import * as VertexOutput from "../ShaderFragments/shaders-base.wgsl.js";
import { device, preferredCanvasFormat } from "../../WebGPUInit.js";
import SceneGlobals from "../SceneGlobals.js";
import RenderContext from "../../RenderContext.js";


const global_src = globals_src({group_global : normals_group_global, binding_camera});

/**
 * @param {string} meshProgram 
 * @returns {string}
 */
function generateShaderCode(meshProgram) {
  return /* wgsl */ `
  ${VertexOutput.vertex_output_src}

  ${global_src}

  ${meshProgram}

  @fragment
  fn main_fragment( @builtin(position) Position : vec4<f32>, 
                    @location(3) color : vec4<f32>
                  ) -> @location(0) vec4<f32> 
  {
    return color;
  }
  `;
}

/**
     * 
     * @param {string} meshName 
     * @param {string} meshShaderSource
     * @param {Iterable<GPUVertexBufferLayout>} meshVertexBuffers
     * @param {GPUBindGroupLayout} [meshBindGroupLayout]
     */
function createPipeline (meshName, meshShaderSource, meshVertexBuffers, meshBindGroupLayout) {

  device.pushErrorScope("validation");
  const bindGroupLayouts = 
    [ 
      SceneGlobals.bindGroupLayout
    ];
  if (meshBindGroupLayout)
    bindGroupLayouts.push(meshBindGroupLayout);

  const pipelineLayout = device.createPipelineLayout({
    label: `${meshName} normals debugger`,
    bindGroupLayouts : bindGroupLayouts,
  });

  const shaderSource = generateShaderCode(meshShaderSource);

  const renderPipeline = device.createRenderPipeline({
    layout: pipelineLayout,
    vertex: {
      module: device.createShaderModule({
        code: shaderSource,
      }),
      entryPoint: 'main_vertex',
      buffers: meshVertexBuffers
    },
    fragment: {
      module: device.createShaderModule({
        code: shaderSource,
      }),
      entryPoint: 'main_fragment',
      targets: [
        {
          format: preferredCanvasFormat,
        },
      ],
    },
    primitive: {
      cullMode: 'none',
      topology: 'line-list',
    },
    depthStencil: {
      depthCompare: "less",
      depthWriteEnabled: true,
      format: RenderContext.depthTextureFormat
    }
  });

  device.popErrorScope().then(err => {
    if (err) {
      console.warn(`Error creating normals shader for ${meshName}`);
      console.warn(err.message);
      console.warn(shaderSource);
    }
  })

  return renderPipeline;

}

export default class NormalsDebugger {
  /** @readonly */
  static heightmapMeshPipeline = createPipeline(
    "Heightmap Mesh", 
    HeightmapMesh.shader_normals_src(normals_group_object),
    HeightmapMesh.vertexBufferLayout_normals,
    HeightmapMesh.bindGroupLayout,
  );
  /** @readonly @type {GPURenderPipeline} */
  static staticMeshPipeline = createPipeline(
    "Static Mesh",
    StaticMesh.createShaderSourceNormals(normals_group_object),
    StaticMesh.vertexBuffers_normals,
    StaticMesh.bindGroupLayout
  );
}