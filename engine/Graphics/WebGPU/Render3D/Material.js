// @ts-check

import HeightmapMesh from "./Meshes/HeightmapMesh.js";
import StaticMesh from "./Meshes/StaticMesh.js";
import { globals_src } from "./SceneGlobals.wgsl.js";
import { EachMeshType } from "./Meshes/AllMeshes.js";
import { binding_camera, group_global, group_material, group_object } from "./config.js";
import * as ShadersBase from "./ShaderFragments/shaders-base.wgsl.js";
import SceneGlobals from "./SceneGlobals.js";
import { device, preferredCanvasFormat } from "../WebGPUInit.js";
import RenderContext from "../RenderContext.js";
import { to_wgsl_source, toUniformInfoWGSL, createBindGroupEntry, createBindGroupLayoutEntry } from "./Material-WebGPU.js";
import { MaterialData } from "../../../Data/Geometric/Materials/MaterialEDSL.js";
import MaterialInstance from "./MaterialInstance.js";


const global_src = globals_src({group_global, binding_camera});

/**
 * 
 * @param {string} meshProgram 
 * @param {string} fragmentProgram 
 * @returns {string}
 */
function generateShaderCode(meshProgram, fragmentProgram) {
  return /* wgsl */ `
  ${ShadersBase.vertex_output_src}

  ${global_src}

  ${meshProgram}

  ${fragmentProgram}
  `;
}

class BindGroupLayouts {
  /** @type {GPUBindGroupLayout} */
  global;
  /** @type {GPUBindGroupLayout} */
  material;
  /** @type {GPUBindGroupLayout | undefined} */
  object;
}

class ShaderSources {
  /** @type {(group_object : number) => string} */
  mesh;
  /** @type {(group_material : number) => string} */
  material;
}

/**
 * @param {BindGroupLayouts} layouts
 * @param {ShaderSources} shaderSources
 * @param {Iterable<GPUVertexBufferLayout>} meshVertexBufferLayouts
 * @param {string} pipelineLabel
 */
function createPipeline(layouts, shaderSources, meshVertexBufferLayouts, pipelineLabel) {
  device.pushErrorScope("validation");

  /** @type {Array<GPUBindGroupLayout>} */
  const bindGroupLayouts = [];
  bindGroupLayouts[group_global] = layouts.global;
  bindGroupLayouts[group_material] = layouts.material;
  if (layouts.object) {
    bindGroupLayouts[group_object] = layouts.object;
  }

  const pipelineLayout = device.createPipelineLayout({
    label: pipelineLabel,
    bindGroupLayouts : bindGroupLayouts,
  });

  const shaderSource = generateShaderCode(shaderSources.mesh(group_object), shaderSources.material(group_material));

  const renderPipeline = device.createRenderPipeline({
    layout: pipelineLayout,
    vertex: {
      module: device.createShaderModule({
        code: shaderSource,
      }),
      entryPoint: 'main_vertex',
      buffers: meshVertexBufferLayouts
    },
    fragment: {
      module: device.createShaderModule({
        code: shaderSource,
      }),
      entryPoint: 'main_fragment',
      targets: [
        {
          format: preferredCanvasFormat,
        },
      ],
    },
    primitive: {
      cullMode: 'back',
      topology: 'triangle-list',
    },
    depthStencil: {
      depthCompare: "less",
      depthWriteEnabled: true,
      format: RenderContext.depthTextureFormat
    }
  });


  device.popErrorScope().then((err) => {
    if (err) {
      console.warn(`Erron when creating pipeline ${pipelineLabel}`);
      console.warn(err.message);
      console.warn(shaderSource);
    }
  });

  return renderPipeline;
}

export default class Material {
  /** @type {EachMeshType<GPURenderPipeline, GPURenderPipeline>} */
  pipelines;

  /**
   * @param {(group_material : number) => string} materialSourceGen
   * @param {GPUBindGroupLayout} materialBindGroupLayout
   * @param {string} materialName
   */
  constructor( materialBindGroupLayout, materialSourceGen, materialName) {
    this.pipelines = new EachMeshType({
      static: createPipeline(
        {
          global: SceneGlobals.bindGroupLayout,
          object: StaticMesh.bindGroupLayout,
          material: materialBindGroupLayout
        },
        {
          mesh: StaticMesh.createShaderSource,
          material: materialSourceGen
        },
        StaticMesh.vertexBuffers,
        `${materialName} - Static Mesh`
      ),
      heightmap: createPipeline(
        {
          global: SceneGlobals.bindGroupLayout,
          object: HeightmapMesh.bindGroupLayout,
          material: materialBindGroupLayout
        },
        {
          mesh: HeightmapMesh.shader_src,
          material: materialSourceGen
        },
        HeightmapMesh.vertexBufferLayout,
        `${materialName} - Heightmap Mesh`
      )
    }
    );
  }

  /**
   * @param {MaterialData} materialData 
   * @return {{material: Material, instance: MaterialInstance}}
   */
  static fromMaterialData(materialData) {
    const uniforms = Object.entries(materialData.uniforms).flatMap(([name, uInfo]) => 
      toUniformInfoWGSL(name, uInfo)
    );
    
    const bindGroupLayout = device.createBindGroupLayout({
      entries: uniforms.map(createBindGroupLayoutEntry)
    });

    const bindGroup = device.createBindGroup({
      layout: bindGroupLayout,
      entries: uniforms.map(createBindGroupEntry)
    });
    const material = new Material(
      bindGroupLayout,
      (group) => to_wgsl_source(materialData, group),
      ""
    );
    return {
      material,
      instance: { material, bindGroup }
    };
  }
}