// @ts-check

import { Mat3, Mat4 } from "../../../Data/LinearAlgebra.js";
import { device } from "../WebGPUInit.js";
import { f32, mat, SizeOf, struct } from "../wgsl.js";

let idCounter = 0;

export default class Transform {
  /** @readonly @type {GPUBuffer} */
  buffer;
  /** @readonly @type {number} */
  id;

  /**
   * 
   * @param {GPUBuffer} buffer 
   */
  constructor(buffer) {
    this.buffer = buffer;
    this.id = idCounter++;
  }

  static byteSize = SizeOf(struct(mat(4,4, f32), mat(3,3, f32)));

  toString() {
    return `[Transform_${this.id}]`;
  }

  /**
   * 
   * @param {Transform} transform 
   * @param {Mat4} globalTransform 
   */
  static updateGPUBuffers(transform, globalTransform) {
    const N = new Mat3();
    Mat4.normalMatrix(N, globalTransform);
    const ON = new Float32Array(16+12);
    for (let ii = 0; ii < 16; ++ii) {
      ON[ii] = globalTransform[ii];
    }
    for (let ii = 0; ii < 3; ++ii) {
      ON[ii*4+16+0] = N[ii*3+0];
      ON[ii*4+16+1] = N[ii*3+1];
      ON[ii*4+16+2] = N[ii*3+2];
    }
    device.queue.writeBuffer(transform.buffer, 0, ON);
  }
}