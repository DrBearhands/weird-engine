// @ts-check
import { nameGenerator } from "../../../../Algorithms/UniqueVarNameGenerator.js";

import Camera from "../Camera.js";

export const names = {
  ...Camera.srcVariableNames,
  VertexOutput : nameGenerator.generate("VertexOutput"),
  position: "position",
  world_position: "world_position",
  normal: "normal",
  tangent: "tangent",
  color: "color",
  tex_coord: "tex_coord"
}

export const vertex_output_src = /* wgsl */ `
  struct ${names.VertexOutput} {
    @builtin(position) ${names.position} : vec4<f32>,
    @location(0) ${names.world_position} : vec3<f32>,
    @location(1) ${names.normal} : vec3<f32>,
    @location(2) ${names.tangent} : vec4<f32>,
    @location(3) ${names.color} : vec4<f32>,
    @location(4) ${names.tex_coord} : vec2<f32>
  }
`

const outputName = nameGenerator.generate("output");
/**
 * 
 * @param {string} positionName 
 * @param {string} normalName 
 * @param {string} tangentName
 * @param {string} texCoordName 
 * @param {string} colorName 
 */
export function writeOutputs(positionName, normalName, tangentName, texCoordName, colorName) {
  const camera = names.global.camera;
  const MVP = names.camera.MVP;

  return /* wgsl */ `
  var ${outputName} : ${names.VertexOutput};
      ${outputName}.${names.position} = ${camera}.${MVP} * ${positionName};
      ${outputName}.${names.world_position} = ${positionName}.xyz;
      ${outputName}.${names.normal} = ${normalName};
      ${outputName}.${names.tangent} = ${tangentName};
      ${outputName}.${names.tex_coord} = ${texCoordName};
      ${outputName}.${names.color} = ${colorName};
      return ${outputName};
  `;
}