// @ts-check


// lifted from https://gist.github.com/davidar/5f9677a0ccfbd63d7a8657ad9af3a856


/**
 * 
 * @param {string} fname 
 * @returns {string}
 */
export function hash21(fname) {
  return /* wgsl */ `
    fn ${fname}(p: f32) -> vec2<f32>
    {
        var p3 = fract(vec3<f32>(p) * vec3<f32>(.1031, .1030, .0973));
        p3 += dot(p3, p3.yzx + 33.33);
        return fract((p3.xx+p3.yz)*p3.zy);
    }
    `
}

/**
 * 
 * @param {string} fname 
 * @returns {string}
 */
export function hash22(fname) {
  return /* wgsl */ `
    fn ${fname}(p: vec2<f32>) -> vec2<f32>
    {
        var p3 = fract(vec3<f32>(p.xyx) * vec3<f32>(.1031, .1030, .0973));
        p3 += dot(p3, p3.yzx+33.33);
        return fract((p3.xx+p3.yz)*p3.zy);
    }
    `
}

/**
 * 
 * @param {string} fname 
 * @returns {string}
 */
export function hash23(fname) {
return /* wgsl */ `fn ${fname}(p: vec3<f32>) -> vec2<f32>
{
  var p3 = fract(p * vec3<f32>(.1031, .1030, .0973));
  p3 += dot(p3, p3.yzx+33.33);
  return fract((p3.xx+p3.yz)*p3.zy);
}`
}