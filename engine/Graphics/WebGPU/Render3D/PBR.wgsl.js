// @ts-check

import { nameGenerator } from "../../../Algorithms/UniqueVarNameGenerator.js";

export const names = {
  pi : nameGenerator.generate("pi"),
  pow2 : nameGenerator.generate("pow2"),
  pow5 : nameGenerator.generate("pow5"),
  TrowbridgeReitzGGX : nameGenerator.generate("TrowbridgeReitzGGX"),
  SchlickGGX : nameGenerator.generate("SchlickGGX"),
  FresnelSchlick : nameGenerator.generate("FresnelSchlick"),
  k_direct : nameGenerator.generate("k_direct"),
  BRDF : "BRDF_direct",//nameGenerator.generate("BRDF"),
  Reinhardt : nameGenerator.generate("Reinhardt"),
  gamma_compresion : nameGenerator.generate("gamma_compresion"),
  Cwhite : nameGenerator.generate("Cwhite"),
  Reinhardt_extended : nameGenerator.generate("Reinhardt_extended")
}

const pi = names.pi;
const pow2 = names.pow2;
const pow5 = names.pow5;
const TrowbridgeReitzGGX = names.TrowbridgeReitzGGX;
const SchlickGGX = names.SchlickGGX;
const FresnelSchlick = names.FresnelSchlick;
const k_direct = names.k_direct;
const k_IBL = names.k_IBL;
const BRDF = names.BRDF;
const Reinhardt = names.Reinhardt;
const gamma_compresion = names.gamma_compresion;
const Cwhite = names.Cwhite;
const Reinhardt_extended = names.Reinhardt_extended;

export const src = /* wgsl */ `
  const ${pi} = 3.141592653589793238462643383279502884197;

  fn ${pow2}(f : f32) -> f32 {
    return f*f;
  }

  fn ${pow5}(f : f32) -> f32 {
    return f*f*f*f*f;
  }

  fn ${TrowbridgeReitzGGX}(n : vec3<f32>, h : vec3<f32>, a : f32) -> f32 {
    let a2 : f32 = a*a;
    let nh : f32 = max(dot(n, h), 0.0);
    return a2 / (${pi} * ${pow2}(nh*nh*(a2-1.0)+1.0));
  }

  fn ${SchlickGGX}(n : vec3<f32>, v : vec3<f32>, k : f32) -> f32 {
    let nv : f32 = max(dot(n, v), 0.0);
    return nv / (nv * (1.0-k) + k); 
  }

  fn ${FresnelSchlick}(h : vec3<f32>, v : vec3<f32>, F0 : vec3<f32>) -> vec3<f32> {
    let hv : f32 = max(dot(h, v), 0.0);
    return F0 + (1.0 - F0)*${pow5}(1.0 - hv);
  }

  fn ${k_direct}(a : f32) -> f32 {
    return ${pow2}(a + 1) / 8.0;
  }

  fn ${k_IBL}(a : f32) -> f32 {
    return ${pow2}(a)/2.0;
  }

  /**
   * Note: v and l must be from pixel to camera and light, respectively
   */
  fn ${BRDF}(n : vec3<f32>, v : vec3<f32>, l : vec3<f32>, albedo : vec3<f32>, roughness : f32, metalness : f32) -> vec3<f32> {
    const black_compression_factor : f32 = 0.99;
    let h : vec3<f32> = normalize((v + l) / 2.0);
    let c : vec3<f32> = albedo * black_compression_factor + (1-black_compression_factor);
    let a : f32 = max(roughness, 0.001);
    let m : f32 = metalness;
    let k : f32 = ${k_direct}(a);
    let F0 : vec3<f32> = mix(vec3<f32>(0.04), c, m);
    let D : f32 = ${TrowbridgeReitzGGX}(n, h, a);
    let G : f32 = ${SchlickGGX}(n, v, k) * ${SchlickGGX}(n, l, k);
    let F : vec3<f32> = ${FresnelSchlick}(h, v, F0);
    let kd : vec3<f32> = (1.0 - F) * (1.0 - m);

    let vn : f32 = max(0.00001, dot(v, n));
    let ln : f32 = max(0.00001, dot(l, n));

    let Ld : vec3<f32> = kd * c / ${pi};
    let Ls : vec3<f32> = D*F*G / (4*vn*ln);
    let i : vec3<f32> = vec3<f32>(1.0) * ln; //TODO: replace with incoming light
    return (Ld + Ls) * i;
  }

  fn getMipLevelFromRoughness(roughness : f32) -> f32 {
    return roughness * 5.0;
  }

  fn PBR_IBL(
    irradiance_texture : texture_cube<f32>, 
    irradiance_sampler : sampler,
    prefiltered_environment_texture : texture_cube<f32>,
    prefiltered_environment_sampler : sampler,
    BRDF_integration_texture : texture_2d<f32>,
    BRDF_integration_sampler : sampler,
    n: vec3<f32>, 
    v: vec3<f32>, 
    albedo : vec3<f32>, 
    metalness : f32,
    roughness : f32,
    ao : f32
  ) -> vec3<f32> {
    let irradiance = textureSample(irradiance_texture, irradiance_sampler, n).rgb;

    let l = reflect(-v, n);

    let nv = max(dot(n, v), 0.0);

    const black_compression_factor : f32 = 0.99;
    let c : vec3<f32> = albedo * black_compression_factor + (1-black_compression_factor);
    let m : f32 = metalness;
    let F0 : vec3<f32> = mix(vec3<f32>(0.04), c, m);
    let F : vec3<f32> =  ${FresnelSchlick}(n, v, F0);
    let kS = F;
    let kD = 1.0 - kS;
    let a = max(roughness, 0.00001);

    let k = ${k_IBL}(a);
    
    let lod : f32 = getMipLevelFromRoughness(roughness);
    let prefiltered_color = textureSampleLevel(prefiltered_environment_texture, prefiltered_environment_sampler, l, lod).rgb;
    let env_BRDF : vec2<f32> = textureSample(BRDF_integration_texture, BRDF_integration_sampler, vec2<f32>(nv, roughness)).rg;
    let indirect_specular : vec3<f32> = prefiltered_color * (F * env_BRDF.x + env_BRDF.y);    

    let diffuse = irradiance * c;
    return (kD * diffuse + indirect_specular) * ao;
  }

  fn ${Reinhardt}(Vin : vec3<f32>) -> vec3<f32> {
    return Vin / (Vin + 1.0);
  }

  const A : f32 = 0.8;

  fn ${gamma_compresion}(Vin : vec3<f32>) -> vec3<f32> {
    return pow(Vin, vec3<f32>(1.0/2.2)) * A;
  }

  const ${Cwhite} : vec3<f32> = vec3<f32>(1.5, 1.5, 1.5);
  fn ${Reinhardt_extended}(Vin : vec3<f32>) -> vec3<f32> {
    return Vin*(1.0 + Vin / (${Cwhite}*${Cwhite})) / (Vin + 1.0);
  }
`

export const environment_to_irradiance_src = /* wgsl */ `
  const PI : f32 = radians(180.0);
  @group(0)
  @binding(0)
  var environ_texture : texture_cube<f32>;
  @group(0)
  @binding(1)
  var environ_sampler : sampler;

  @fragment
  fn main_fragment(@location(0) cube_position : vec3<f32>) -> @location(0) vec4<f32> {
    let n = normalize(cube_position);

    var irradiance : vec3<f32> = vec3<f32>(0.0);
    
    var up : vec3<f32> = vec3<f32>(0.0, 1.0, 0.0);
    let right : vec3<f32> = normalize(cross(up, n));
    up = normalize(cross(n, right));

    const sample_delta = 0.025;
    const phi_max = 2.0 * PI;
    const theta_max = 0.5 * PI;
    const num_horiz_samples = u32(phi_max / sample_delta);
    const num_verti_samples = u32(theta_max / sample_delta);

    for(var phi : f32 = 0; phi < phi_max; phi += sample_delta)
    {
      for(var theta : f32 = 0; theta < theta_max; theta += sample_delta)
      {
        let wi_tangent = vec3<f32>(sin(theta) * cos(phi),  sin(theta) * sin(phi), cos(theta));
        let wi_world = wi_tangent.x * right + wi_tangent.y * up + wi_tangent.z * n; 

        irradiance += textureSample(environ_texture, environ_sampler, wi_world).rgb * cos(theta) * sin(theta);
      }
    }
    irradiance = PI * irradiance * (1.0 / f32(num_horiz_samples * num_verti_samples));
    return vec4<f32>(irradiance, 1.0);
  }
`

const monte_carlo_src = /* wgsl */ `
  const PI : f32 = radians(180.0);

  fn radical_inverse_van_der_Corput(bits : u32) -> f32 
  {
    var _bits = bits;
    _bits = (_bits << 16u) | (_bits >> 16u);
    _bits = ((_bits & 0x55555555u) << 1u) | ((_bits & 0xAAAAAAAAu) >> 1u);
    _bits = ((_bits & 0x33333333u) << 2u) | ((_bits & 0xCCCCCCCCu) >> 2u);
    _bits = ((_bits & 0x0F0F0F0Fu) << 4u) | ((_bits & 0xF0F0F0F0u) >> 4u);
    _bits = ((_bits & 0x00FF00FFu) << 8u) | ((_bits & 0xFF00FF00u) >> 8u);
    return f32(_bits) * 2.3283064365386963e-10;
  }

  fn Hammersley(i : u32, N : u32) -> vec2<f32>
  {
      return vec2<f32>(f32(i)/f32(N), radical_inverse_van_der_Corput(i));
  }

  fn importance_sample_GGX(Xi : vec2<f32>, n : vec3<f32>, roughness : f32) -> vec3<f32>
  {
    let a = roughness*roughness;

    let phi = 2.0 * PI * Xi.x;
    let cosTheta = sqrt((1.0 - Xi.y) / (1.0 + (a*a - 1.0) * Xi.y));
    let sinTheta = sqrt(1.0 - cosTheta*cosTheta);

    let h = vec3<f32>(cos(phi) * sinTheta, sin(phi) * sinTheta, cosTheta);
    
    var up = vec3(1.0, 0.0, 0.0);
    if (abs(n.z) < 0.999) {
      up = vec3(0.0, 0.0, 1.0);
    }
    let tangent   = normalize(cross(up, n));
    let bitangent = cross(n, tangent);
    return normalize(tangent * h.x + bitangent * h.y + n * h.z);
  } 

`;
export const prefilter_environment_map = /* wgsl */ `

  ${src}

  ${monte_carlo_src}

  @group(0)
  @binding(0)
  var environ_texture : texture_cube<f32>;
  @group(0)
  @binding(1)
  var environ_sampler : sampler;

  @group(2)
  @binding(0)
  var<uniform> roughness : f32;

  @fragment
  fn main_fragment(@location(0) cube_position : vec3<f32>) -> @location(0) vec4<f32> {
    let a : f32 = max(roughness, 0.001);
    let n = normalize(cube_position);

    const sample_count : u32 = 4096u;
    var prefiltered_color = vec3<f32>(0.0);
    var total_weight = 0.0;
    for(var ii : u32 = 0u; ii < sample_count; ii += 1u)
    {
      let Xi = Hammersley(ii, sample_count);
      let h = importance_sample_GGX(Xi, n, roughness);
      let l = normalize(2.0 * dot(n, h) * h - n);
      let weight = max(dot(n, l), 0.0);
      if(weight > 0.0)
      {
        prefiltered_color += textureSample(environ_texture, environ_sampler, l).rgb * weight;
        total_weight += weight;
      }
    }
    return vec4<f32>(prefiltered_color / total_weight, 1.0);
  }
`;

export const BRDF_integration_map_src = /* wgsl */ `

  const v00 = vec2<f32>(-1, -1);
  const v01 = vec2<f32>(-1,  1);
  const v10 = vec2<f32>( 1, -1);
  const v11 = vec2<f32>( 1,  1);

  const positions = array<vec2<f32>, 4>(
    v00, v01, v10, v11
  );

  struct VertexOutput {
    @builtin(position) position : vec4<f32>,
    @location(0) tex_coords : vec2<f32>
  }

  @vertex
  fn main_vertex(@builtin(vertex_index) vertex_index : u32) -> VertexOutput {
    let clip_pos = vec4<f32>(positions[vertex_index], 1.0, 1.0);
    var out : VertexOutput;
    out.position = clip_pos;
    out.tex_coords = (clip_pos.xy + 1.0)/2.0;
    return out;
  }

  fn geometry_Schlick_GGX(nv : f32, roughness : f32) -> f32
  {
      let a = roughness;
      let k = (a * a) / 2.0;

      return nv / (nv * (1.0 - k) + k);
  }
  
  fn geometry_Smith(n : vec3<f32>, v : vec3<f32>, l : vec3<f32>, roughness : f32) -> f32
  {
      let nv = max(dot(n, v), 0.0);
      let nl = max(dot(n, l), 0.0);
      let ggx2 = geometry_Schlick_GGX(nv, roughness);
      let ggx1 = geometry_Schlick_GGX(nl, roughness);

      return ggx1 * ggx2;
  } 

  ${monte_carlo_src}


  fn integrate_BRDF(nv : f32, roughness : f32) -> vec2<f32>
  {
    let v = vec3<f32>(sqrt(1.0 - nv*nv), 0.0, nv);

    var scale : f32 = 0.0;
    var bias : f32 = 0.0;

    var n = vec3<f32>(0.0, 0.0, 1.0);

    const sample_count : u32 = 1024u;
    for(var i : u32 = 0u; i < sample_count; i += 1u)
    {
        let Xi = Hammersley(i, sample_count);
        let h  = importance_sample_GGX(Xi, n, roughness);
        let l  = normalize(2.0 * dot(v, h) * h - v);

        let nl = max(l.z, 0.0);
        let nh = max(h.z, 0.0);
        let vh = max(dot(v, h), 0.0);

        if(nl > 0.0)
        {
            let G = geometry_Smith(n, v, l, roughness);
            let G_Vis = (G * vh) / (nh * nv);
            let Fc = pow(1.0 - vh, 5.0);

            scale += (1.0 - Fc) * G_Vis;
            bias += Fc * G_Vis;
        }
    }
    scale /= f32(sample_count);
    bias /= f32(sample_count);
    return vec2<f32>(scale, bias);
  }

  @fragment
  fn main_fragment(@location(0) tex_coords : vec2<f32>) -> @location(0) vec2<f32> {
    let integratedBRDF = integrate_BRDF(tex_coords.x, tex_coords.y);
    return integratedBRDF;
  }
`