// @ts-check

import { device } from "../WebGPUInit.js";

import { write_cubemap_vertex_src, equirectangular_to_cube_src } from "./CubeMap.wgsl.js"
import { f32, mat, SizeOf } from "../wgsl.js";
import { Mat3 } from "../../../Data/LinearAlgebra.js";
import { bufferMat3, createUniformBuffer } from "../WebGPUBuffers.js";

const numSidesCube = 6;

export default class CubeMap {

  /** @readonly */
  textureView;
  /** @readonly */
  bindGroup;

  /**
   * 
   * @param {GPUTextureView} textureView 
   * @param {GPUBindGroup} bindGroup 
   */
  constructor(textureView, bindGroup) {
    this.textureView = textureView;
    this.bindGroup = bindGroup;
  }

  /**
   * 
   * @param {GPUTexture} equirectangularTexture
   * @param {{width: number, height: number}} dimensions
   * @returns {GPUTexture}
   */
  static fromEquirectangular(equirectangularTexture, dimensions) {
    device.pushErrorScope("validation");
    const textureBindGroup = device.createBindGroup({
      label: "EquirectangularToCubemap - sampler",
      layout: equirectangularBindGroupLayout,
      entries: [
        {
          binding: 0,
          resource: equirectangularTexture.createView()
        },
        {
          binding: 1,
          resource: equirectangularSampler
        }
      ]
    });

    const cubeTexture = device.createTexture({
      dimension: "2d",
      format : "rgba16float",
      size: [dimensions.width, dimensions.height, 6],
      usage : GPUTextureUsage.TEXTURE_BINDING | GPUTextureUsage.COPY_DST | GPUTextureUsage.RENDER_ATTACHMENT,
    });

    const commandEncoder = device.createCommandEncoder();

    for (let side = 0; side < numSidesCube; ++side) {
      const passEncoder = commandEncoder.beginRenderPass({
        colorAttachments: [
          {
            clearValue: {r : 0, g:0, b: 0, a:1},
            loadOp: "clear",
            storeOp: "store",
            view: cubeTexture.createView({
              dimension: '2d',
              baseArrayLayer: side,
              arrayLayerCount: 1
            }),
          },
        ]
      });
      passEncoder.setPipeline(equirectangularPipeline);
      passEncoder.setBindGroup(0, textureBindGroup);
      passEncoder.setBindGroup(1, CubeMap.matrixBindGroups[side]);
      passEncoder.draw(4);
      passEncoder.end();
    }
    device.queue.submit([commandEncoder.finish()]);

    device.popErrorScope().then(err => {
      if (err) {
        console.error("error in CubeMap.fromEquirectangular");
        console.error(err);
      }
    });

    return cubeTexture;
  }

  /** @readonly */
  static matrixBindGroupLayout = device.createBindGroupLayout({
    label: "Cubemap Side Matrix",
    entries: [{
      binding: 0,
      visibility: GPUShaderStage.VERTEX,
      buffer: {
        type: "uniform",
        minBindingSize: SizeOf(mat(3, 3, f32))
      }
    }]
  });

  /** @readonly */
  static matrixBindGroups = [Mat3.plusX(), Mat3.minX(), Mat3.plusY(), Mat3.minY(), Mat3.plusZ(), Mat3.minZ()].map(M => {
    const buffer = createUniformBuffer(device, mat(3,3,f32));
    bufferMat3(device, buffer, M);
  
    return device.createBindGroup({
      label: "Equirectangular to Cube - Matrix",
      layout: CubeMap.matrixBindGroupLayout,
      entries: [
        {
          binding: 0,
          resource: {
            buffer
          }
        }
      ]
    });
  });
}

/** @readonly */
const equirectangularBindGroupLayout = device.createBindGroupLayout({
  label: "Equirectangular to Cube",
  entries: [
    {
      binding: 0,
      visibility: GPUShaderStage.FRAGMENT,
      texture: {
        multisampled: false,
        sampleType: "float",
      }
    },
    {
      binding: 1,
      visibility: GPUShaderStage.FRAGMENT,
      sampler: {
        type: "filtering"
      }
    }
  ]
});

const equirectangularSampler = device.createSampler();

const equirectangularPipeline = device.createRenderPipeline({
  layout: device.createPipelineLayout({
    label: "Equirectangular to Cube",
    bindGroupLayouts : [equirectangularBindGroupLayout, CubeMap.matrixBindGroupLayout],
  }),
  vertex: {
    module: device.createShaderModule({
      code: write_cubemap_vertex_src,
    }),
    entryPoint: 'main_vertex',
    buffers: []
  },
  fragment: {
    module: device.createShaderModule({
      code: equirectangular_to_cube_src,
    }),
    entryPoint: 'main_fragment',
    targets: [
      {
        format: "rgba16float",
      },
    ],
  },
  primitive: {
    cullMode: 'none',
    topology: 'triangle-strip',
  }
});