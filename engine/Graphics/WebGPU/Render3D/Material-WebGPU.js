// @ts-check

import { MaterialData } from "../../../Data/Geometric/Materials/MaterialEDSL.js";
import * as eDSL from "../../../Data/Geometric/Materials/MaterialEDSL.js";

import {src as pbr_src, names as pbr_names} from "./PBR.wgsl.js";
import Camera from "./Camera.js";
import { PBR_output } from "../../../Data/Geometric/Materials/MaterialGraph.js";
import { f32, mat, sampler, SizeOf, texture_2d, texture_cube, vec } from "../wgsl.js";
import { device } from "../WebGPUInit.js";

const names = { ...Camera.srcVariableNames }

/**
 * @todo: add camera viewpoint / view direction
 * @param {MaterialData<PBR_output>} materialData
 * @param {number} materialGroupId
 * @returns {string}
 */
export function to_wgsl_source(materialData, materialGroupId) {
  let res = "";
  const uniforms = Object.entries(materialData.uniforms).flatMap(([varName, uInfo]) => 
    toUniformInfoWGSL(varName, uInfo)
  )
  let binding = 0;
  for (const uniform of uniforms) {
    const declStart = uniform.resourceType === "buffer" ? "var<uniform>" : "var";
    res+=
      `@group(${materialGroupId})\n` +
      `@binding(${binding++})\n` +
      `${declStart} ${uniform.name} : ${uniform.type};\n\n`;
  }

  res += pbr_src;

  
  res +=
  "@fragment\n" +
  "fn main_fragment(\n";

  
  let attributeLocation = 0; 
  for (const [attributeName, attributeType] of Object.entries(MaterialData.attributes)) {
    const wgslVariables = to_wgsl_source_variable(attributeName, attributeType);
    for (const {varName, type} of wgslVariables) {
      res += `  @location(${attributeLocation++}) ${varName} : ${type},\n`;
    }
  }
  res += `) -> @location(0) vec4<f32> {\n`;

  res += "  let l : vec3<f32> = normalize(vec3<f32>(0.5, 1.0, -.5));\n" /** @todo lights */
  res += `  let v : vec3<f32> = normalize(${names.global.camera}.${names.camera.position} - position);\n`

  for (const statement of materialData.statements) {
    res += to_wgsl_source_statement(statement);
  }
  const outputs = materialData.outputs;
  const expr = to_wgsl_source_expression;
  res += `  let I = ${pbr_names.BRDF}(`;
  res += expr(outputs.normal) + ", ";
  res += "v, l, "
  res += expr(outputs.albedo) + ", ";
  res += expr(outputs.roughness) + ", ";
  res += expr(outputs.metallic);
  res += ") * 5.0;\n";
  res += `return vec4<f32>(${pbr_names.gamma_compresion}(I), 1.0);\n`;
  res += "}";
  return res;
}

/** 
 * @typedef {{
*   resourceType: "buffer",
*   type: import("../wgsl.js").WGSLType,
*   name: string,
*   buffer: GPUBuffer
* } | {
*   resourceType: "texture",
*   type: import("../wgsl.js").WGSLType,
*   name: string
*   textureView: GPUTextureView,
* } | {
*   resourceType: "sampler",
*   type: import("../wgsl.js").WGSLType,
*   name: string
* }} UniformInfoWGSL
*/

/**
 * @param {import("../wgsl.js").WGSLSizedType} wgslType
 * @param {ArrayBuffer} defaultData
 * @param {string} varNameIn
 * @returns {UniformInfoWGSL}
 */
export function tensorUniformInfo(wgslType, defaultData, varNameIn) {
  const buffer = device.createBuffer({
    size: SizeOf(wgslType),
    usage: GPUBufferUsage.COPY_DST | GPUBufferUsage.UNIFORM
  });

  device.queue.writeBuffer(buffer, 0, defaultData);

  return {
    resourceType: "buffer",
    type: wgslType,
    name: varNameIn,
    buffer 
  };
}

/**
 * @param {string} varNameIn
 * @param {eDSL.UniformInformation} uniformInfo 
 * @returns {UniformInfoWGSL[]}
 */
export function toUniformInfoWGSL(varNameIn, uniformInfo) {
  switch (uniformInfo.type) {
    case 'scalar':
      return [tensorUniformInfo(f32, new Float32Array([uniformInfo.defaultValue]), varNameIn)];
    case 'vec2f':
      return [tensorUniformInfo(vec(2, f32), new Float32Array(uniformInfo.defaultValue), varNameIn)];
    case 'vec3f':
      return [tensorUniformInfo(vec(3, f32), new Float32Array(uniformInfo.defaultValue), varNameIn)];
    case 'vec4f':
      return [tensorUniformInfo(vec(4, f32), new Float32Array(uniformInfo.defaultValue), varNameIn)];
    case 'mat2':
      return [tensorUniformInfo(mat(2, 2, f32), new Float32Array(uniformInfo.defaultValue), varNameIn)];
    case 'mat3':
      return [tensorUniformInfo(mat(3,3,f32), new Float32Array(uniformInfo.defaultValue), varNameIn)];
    case 'mat4':
      return [tensorUniformInfo(mat(4,4,f32), new Float32Array(uniformInfo.defaultValue), varNameIn)];
    case 'texture2D': {
      const image = uniformInfo.defaultValue;
      const width = image.width;
      const height = image.height;

      const texture = device.createTexture({
        dimension: "2d",
        format: "rgba8unorm-srgb",
        size: [image.width, image.height, 1],
        usage: GPUTextureUsage.TEXTURE_BINDING | GPUTextureUsage.COPY_DST | GPUTextureUsage.RENDER_ATTACHMENT
      });
      device.queue.copyExternalImageToTexture({source : image}, { texture }, {width, height});
      return [
        { 
          resourceType: "texture", 
          type: texture_2d(f32),
          textureView: texture.createView(), 
          name: `${varNameIn}_texture` 
        },
        { 
          resourceType: "sampler", 
          type: sampler,
          name: `${varNameIn}_sampler`
        }
      ];
    }
    case 'textureCube': {
      const image = uniformInfo.defaultValue;
      const width = image.width;
      const height = image.height;

      const texture = device.createTexture({
        dimension: "2d",
        format: "rgba8unorm",
        size: [image.width, image.height, 1],
        usage: GPUTextureUsage.TEXTURE_BINDING | GPUTextureUsage.COPY_DST | GPUTextureUsage.RENDER_ATTACHMENT
      });

      device.queue.copyExternalImageToTexture({source : image}, { texture }, {width, height});
      return [
        { 
          resourceType: "texture", 
          type: texture_cube(f32),
          textureView: texture.createView(
          {
            dimension: 'cube'
          }),
          name: `${varNameIn}_texture`
        },
        { 
          resourceType: "sampler", 
          type: sampler,
          name: `${varNameIn}_sampler` 
        }
      ];
    }
  }
}

/**
 * @param {UniformInfoWGSL} uniformInfo 
 * @param {number} binding 
 * @returns {GPUBindGroupEntry}
 */
export function createBindGroupEntry(uniformInfo, binding) {
  switch (uniformInfo.resourceType) {
    case "buffer": {
      return {
        binding,
        resource: {
          buffer: uniformInfo.buffer,
          offset: 0
        }
      };
    }
    case "texture": {
      return {
        binding,
        resource: uniformInfo.textureView
      }
    }
    case "sampler": {
      return {
        binding,
        resource: device.createSampler({
          addressModeU: "repeat",
          addressModeV: "repeat"
        })
      }
    }
  }
}

/**
 * @param {UniformInfoWGSL} uniformInfo 
 * @param {number} binding 
 * @returns {GPUBindGroupLayoutEntry}
 */
export function createBindGroupLayoutEntry(uniformInfo, binding) {
  switch (uniformInfo.resourceType) {
    case "buffer": {
      return {
        binding,
        visibility: GPUShaderStage.FRAGMENT,
        buffer: {
          type: "uniform",
        }
      };
    }
    case "texture": {
      return {
        binding,
        visibility: GPUShaderStage.FRAGMENT,
        texture: {
          viewDimension: '2d'
        }
      }
    }
    case "sampler": {
      return {
        binding,
        visibility: GPUShaderStage.FRAGMENT,
        sampler: {}
      }
    }
  }
}

/**
 * @param {string} varName
 * @param {eDSL.Type} materialType
 * @returns {{varName: string, type: import("../wgsl.js").WGSLType}[]}
 */
export function to_wgsl_source_variable(varName, materialType) {
  switch (materialType) {
    case 'scalar':
      return [{varName, type: f32}];
    case 'vec2f':
      return [{varName, type: vec(2, f32)}];
    case 'vec3f':
      return [{varName, type: vec(3, f32)}];
    case 'vec4f':
      return [{varName, type: vec(4, f32)}];
    case 'mat2':
      return [{varName, type: mat(2,2,f32)}];
    case 'mat3':
      return [{varName, type: mat(3,3,f32)}];
    case 'mat4':
      return [{varName, type: mat(4,4,f32)}];
    case 'texture2D':
      return [
        { varName: `${varName}_texture`, type: texture_2d(f32)},
        { varName: `${varName}_sampler`, type: sampler}
      ];
    case 'textureCube':
      return [
        { varName: `${varName}_texture`, type: texture_cube(f32)},
        { varName: `${varName}_sampler`, type: sampler}
      ];
  }
}

/**
 * @param {import("../../../Data/Geometric/Materials/MaterialEDSL.js").Statement} statement 
 * @returns {string}
 */
function to_wgsl_source_statement(statement) {
  switch (statement.tag) {
    case 'assignment':
      return `let ${statement.varName} = ${to_wgsl_source_expression(statement.expression)};\n`;
    case 'conditional':
      return `if (${statement.expression}) {\n` +
        statement.trueCase.map(s => to_wgsl_source_statement(s)).join('') +
        `} else {\n` +
        statement.falseCase.map(s => to_wgsl_source_statement(s)).join('') +
        `}\n`;
  }
}

/**
 * @param {import("../../../Data/Geometric/Materials/MaterialEDSL.js").Expression} expression 
 * @returns {string}
 */
function to_wgsl_source_expression(expression) {
  switch (expression.tag) {
    case 'constant':
      return to_wgsl_source_value(expression.value);
    case 'functionCall':
      {
        const args = expression.arguments.map(to_wgsl_source_expression);
        return `${expression.functionName}(${args.join(',')})`;
      }
    case 'operator':
      {
        const leftSide = expression.leftSide? to_wgsl_source_expression(expression.leftSide) : "";
        const rightSide = to_wgsl_source_expression(expression.rightSide);
        return `(${leftSide}) ${expression.operator} (${rightSide})`;
      }
    case 'selector':
      {
        const subExpression = to_wgsl_source_expression(expression.expression);
        return `(${subExpression}).${expression.selector}`;
      }
    case 'textureSample2D':
      {
        const [textureVar, samplerVar] = to_wgsl_source_variable(expression.textureName, 'texture2D');
        const uv = to_wgsl_source_expression(expression.texCoords);
        return `textureSample(${textureVar.varName}, ${samplerVar.varName}, ${uv})`;
      }
    case 'variable':
      {
        return expression.varName;
      }
  }
}

/**
 * 
 * @param {[number] | [number, number] | [number, number, number] | [number, number, number, number]} value 
 */
function to_wgsl_source_value(value) {
  switch (value.length) {
    case 1:
      return to_wgsl_float_const(value[0]);
    case 2:
      return `vec2<f32>(${to_wgsl_float_const(value[0])}, ${to_wgsl_float_const(value[1])})`;
    case 3:
      return `vec3<f32>(${to_wgsl_float_const(value[0])}, ${to_wgsl_float_const(value[1])}, ${to_wgsl_float_const(value[2])})`;
    case 4:
      return `vec4<f32>(${to_wgsl_float_const(value[0])}, ${to_wgsl_float_const(value[1])}, ${to_wgsl_float_const(value[2])}, ${to_wgsl_float_const(value[3])})`;
  }
}

/**
 * 
 * @param {number} value 
 * @returns {string}
 */
function to_wgsl_float_const(value) {
  if (Number.isInteger(value)){
    return value.toFixed(1);
  } else
    return `${value}`;
}