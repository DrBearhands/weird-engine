// @ts-check
import Camera from "../Camera.js";

const names = { ...Camera.srcVariableNames }

const camera = names.global.camera;
const MVP = names.camera.MVP;
const position = names.camera.position;

export const environment_map_src = /* wgsl */ `

  const v000 = vec3<f32>(-1, -1, -1);
  const v100 = vec3<f32>(1, -1, -1);
  const v010 = vec3<f32>(-1, 1, -1);
  const v110 = vec3<f32>(1, 1, -1);

  const v001 = vec3<f32>(-1, -1, 1);
  const v101 = vec3<f32>(1, -1, 1);
  const v011 = vec3<f32>(-1, 1, 1);
  const v111 = vec3<f32>(1, 1, 1);

  const positions = array<vec3<f32>, 36>(
    //back (-Z)
    v000, v100, v010,
    v100, v110, v010,
    //front (+Z)
    v001, v011, v101,
    v101, v011, v111,
    //left (-X)
    v000, v010, v001,
    v010, v011, v001,
    //right (+X)
    v100, v101, v110,
    v110, v101, v111,
    //bottom  (-Y)
    v000, v101, v100,
    v101, v000, v001,
    //top (+Y)
    v010, v110, v111,
    v111, v011, v010
  );

  struct VertexOutput {
    @builtin(position) position : vec4<f32>,
    @location(0) cube_position : vec3<f32>,
  }

  ${Camera.createShaderSource(0, 0)}

  @vertex
  fn main_vertex(@builtin(vertex_index) vertex_index : u32) -> VertexOutput {
    var out : VertexOutput;
    out.cube_position = positions[vertex_index];
    out.position = (${camera}.${MVP} * vec4(out.cube_position + ${camera}.${position}, 1.0)).xyww;
    return out;
  }

  @group(1)
  @binding(0)
  var environ_texture : texture_cube<f32>;
  @group(1)
  @binding(1)
  var environ_sampler : sampler;

  @fragment
  fn main_fragment(
    @location(0) cube_position : vec3<f32>,
  ) -> @location(0) vec4<f32> {
    
    let c = textureSample(environ_texture, environ_sampler, cube_position.xyz);

    return vec4<f32>(gamma_compresion(c.rgb), c.a);
  }

  const A : f32 = 0.8;
  fn gamma_compresion(Vin : vec3<f32>) -> vec3<f32> {
    return pow(Vin, vec3<f32>(1.0/2.2)) * A;
  }
`;