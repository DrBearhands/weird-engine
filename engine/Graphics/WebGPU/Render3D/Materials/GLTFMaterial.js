// @ts-check

import { GLTFMaterialData } from "../../../../Data/Geometric/Materials/GLTFMaterialData.js";
import Material from "../Material.js";
import MaterialInstance from "../MaterialInstance.js";
import { device } from "../../WebGPUInit.js";
import { textureFromData } from "../../WebGPUTextures.js";
import { f32, SizeOf, struct, vec } from "../../wgsl.js";

import { src } from "./GLTFMaterial.wgsl.js"

const bindings = {
  group_material : -1,
  binding_baseColorTexture : 0,
  binding_baseColorSampler : 1,
  binding_metallicRoughnessTexture : 2,
  binding_metallicRoughnessSampler : 3,
  binding_normalTexture : 4,
  binding_normalSampler : 5,
  binding_occlusionTexture : 6,
  binding_occlusionSampler : 7,
  binding_emissiveTexture : 8,
  binding_emissiveSampler : 9,
  binding_gltfMaterialScalars : 10,
};

const gltfMaterialScalars_type = struct(vec(4, f32), f32, f32, f32, f32, vec(3, f32), f32);
const gltfMaterialScalars_size = SizeOf(gltfMaterialScalars_type);

/**
 * @param {number} group_material 
 */
const make_source = (group_material) => {
  bindings.group_material = group_material;
  return src(bindings);
}

export class GLTFMaterial extends Material {
  /**
   * @readonly @type {GPUBindGroupLayout}
   */
  materialBindGroupLayout;

  constructor() {
    const materialBindGroupLayout = device.createBindGroupLayout({
      label: "GLTFMaterial - bindGroupLayout",
      entries: [
        {
          binding: bindings.binding_baseColorTexture,
          visibility: GPUShaderStage.FRAGMENT,
          texture: {
            multisampled: false,
            sampleType: "float",
          }
        },
        {
          binding: bindings.binding_baseColorSampler,
          visibility: GPUShaderStage.FRAGMENT,
          sampler: {
            type: "filtering"
          }
        },
        {
          binding: bindings.binding_metallicRoughnessTexture,
          visibility: GPUShaderStage.FRAGMENT,
          texture: {
            multisampled: false,
            sampleType: "float"
          }
        },
        {
          binding: bindings.binding_metallicRoughnessSampler,
          visibility: GPUShaderStage.FRAGMENT,
          sampler: {
            type: "filtering"
          }
        },
        {
          binding: bindings.binding_normalTexture,
          visibility: GPUShaderStage.FRAGMENT,
          texture: {
            multisampled: false,
            sampleType: "float"
          }
        },
        {
          binding: bindings.binding_normalSampler,
          visibility: GPUShaderStage.FRAGMENT,
          sampler: {
            type: "filtering"
          }
        },
        {
          binding: bindings.binding_occlusionTexture,
          visibility: GPUShaderStage.FRAGMENT,
          texture: {
            multisampled: false,
            sampleType: "float"
          }
        },
        {
          binding: bindings.binding_occlusionSampler,
          visibility: GPUShaderStage.FRAGMENT,
          sampler: {
            type: "filtering"
          }
        },
        {
          binding: bindings.binding_emissiveTexture,
          visibility: GPUShaderStage.FRAGMENT,
          texture: {
            multisampled: false,
            sampleType: "float"
          }
        },
        {
          binding: bindings.binding_emissiveSampler,
          visibility: GPUShaderStage.FRAGMENT,
          sampler: {
            type: "filtering"
          }
        },
        {
          binding: bindings.binding_gltfMaterialScalars,
          visibility: GPUShaderStage.FRAGMENT,
          buffer: {
            type: "uniform",
            minBindingSize: gltfMaterialScalars_size
          }
        }
      ]
    });

    super(materialBindGroupLayout, make_source, "GLTF Material");
    this.materialBindGroupLayout = materialBindGroupLayout;
  }

  /**
   * @param {GLTFMaterialData} materialData
   * @returns {GLTFMaterialInstance}
   */
  createInstance(materialData ) {

    const baseColorFactor = materialData.baseColorFactor;
    const metallicFactor = materialData.metallicFactor;
    const roughnessFactor = materialData.roughnessFactor;
    const normalScale = materialData.normalScale;
    const occlusionStrength = materialData.occlusionStrength;
    const emissiveFactor = materialData.emissiveFactor;
    const alphaCutoff = materialData.alphaCutoff;
    
    
    const scalars_buffer = device.createBuffer({
      usage: GPUBufferUsage.UNIFORM | GPUBufferUsage.COPY_DST,
      size: gltfMaterialScalars_size
    });
    
    
    device.queue.writeBuffer(scalars_buffer, 0, new Float32Array([
      baseColorFactor[0], baseColorFactor[1], baseColorFactor[2], baseColorFactor[3],
      metallicFactor,
      roughnessFactor,
      normalScale,
      occlusionStrength,
      emissiveFactor[0], emissiveFactor[1], emissiveFactor[2],
      alphaCutoff
    ]));
    
    const baseColorTextureSampler = materialData.baseColorTexture;
    const baseColorTextureView = textureFromData(baseColorTextureSampler.texture).createView();
    const baseColorSampler = device.createSampler(baseColorTextureSampler.sampler);

    const metallicRoughnessTextureSampler = materialData.metallicRoughnessTexture;
    const metallicRoughnessTextureView = textureFromData(metallicRoughnessTextureSampler.texture, true).createView();
    const metallicRoughnessSampler = device.createSampler(metallicRoughnessTextureSampler.sampler);

    const normalTextureSampler = materialData.normalTexture;
    const normalTextureView = textureFromData(normalTextureSampler.texture, true).createView();
    const normalSampler = device.createSampler(normalTextureSampler.sampler);

    const occlusionTextureSampler = materialData.occlusionTexture;
    const occlusionTextureView = textureFromData(occlusionTextureSampler.texture, true).createView();
    const occlusionSampler = device.createSampler(occlusionTextureSampler.sampler);

    const emissiveTextureSampler = materialData.baseColorTexture;
    const emissiveTextureView = textureFromData(emissiveTextureSampler.texture).createView();
    const emissiveSampler = device.createSampler(emissiveTextureSampler.sampler);

    const bindGroup = device.createBindGroup({
      label: "GLTFMaterial Scalars BindGroup",
      layout: this.materialBindGroupLayout,
      entries: [
        {
          binding: bindings.binding_baseColorTexture,
          resource: baseColorTextureView
        },
        {
          binding: bindings.binding_baseColorSampler,
          resource: baseColorSampler
        },
        {
          binding: bindings.binding_metallicRoughnessTexture,
          resource: metallicRoughnessTextureView
        },
        {
          binding: bindings.binding_metallicRoughnessSampler,
          resource: metallicRoughnessSampler
        },
        {
          binding: bindings.binding_normalTexture,
          resource: normalTextureView
        },
        {
          binding: bindings.binding_normalSampler,
          resource: normalSampler
        },
        {
          binding: bindings.binding_occlusionTexture,
          resource: occlusionTextureView
        },
        {
          binding: bindings.binding_occlusionSampler,
          resource: occlusionSampler
        },
        {
          binding: bindings.binding_emissiveTexture,
          resource: emissiveTextureView
        },
        {
          binding: bindings.binding_emissiveSampler,
          resource: emissiveSampler
        },
        {
          binding: bindings.binding_gltfMaterialScalars,
          resource: {
            buffer: scalars_buffer
          }
        }
      ],
    });
    const instance = new GLTFMaterialInstance(this, bindGroup, scalars_buffer);
    return instance;
  }
}

export class GLTFMaterialInstance extends MaterialInstance {
  /** @readonly @type {GPUBuffer} */
  #scalarsBuffer;

  /**
   * @param {GLTFMaterial} material
   * @param {GPUBindGroup} bindGroup 
   * @param {GPUBuffer} scalarsBuffer
   */
  constructor(material, bindGroup, scalarsBuffer) {
    super(material, bindGroup)
    this.device = device;
    this.#scalarsBuffer = scalarsBuffer;
  }

}