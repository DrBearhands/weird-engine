// @ts-check

import Material from "../Material.js";
import { device } from "../../WebGPUInit.js";


export default class UnlitMaterial extends Material {
  constructor() {
    const materialBindGroupLayout = device.createBindGroupLayout({
      label: "Unlit Material - bindGroupLayout",
      entries: []
    });
    super(materialBindGroupLayout, shaderSource, "Unlit Material")
  }
}

const shaderSource = () => /* wgsl */ `

@fragment
fn main_fragment( @builtin(position) Position : vec4<f32>, 
                  @location(0) normal : vec3<f32>,
                  @location(1) color : vec4<f32>
                ) -> @location(0) vec4<f32> {
  return vec4<f32>(1.0, 0.0, 0.0, 1.0);
}
`