// @ts-check

import * as PBR from "../PBR.wgsl.js";
import Camera from "../Camera.js";
import { nameGenerator } from "../../../../Algorithms/UniqueVarNameGenerator.js"

const names = { ...Camera.srcVariableNames }
/**
 * 
 * @param {{
 *  material_group : number
 *  binding_grass_color : number,
 *  binding_rock_color : number
 * }} bindings 
 * @returns 
 */
export function src({material_group, binding_grass_color, binding_rock_color}) {

  const camera = names.global.camera;
  const position = names.camera.position;

  const grass_color = nameGenerator.generate("grass_color");
  const rock_color = nameGenerator.generate("rock_color");
  const up = nameGenerator.generate("up");
  const blend_start = nameGenerator.generate("blend_start");
  const blend_end = nameGenerator.generate("blend_end");
  
  
  return /* wgsl */ `
    ${PBR.src}

    @group(${material_group}) @binding(${binding_grass_color})
    var<uniform> ${grass_color} : vec3<f32>;
    @group(${material_group}) @binding(${binding_rock_color})
    var<uniform> ${rock_color} : vec3<f32>;

    const ${up} = vec3<f32>(0.0, 1.0, 0.0);

    const ${blend_start} : f32 = 0.8;
    const ${blend_end} : f32 = 0.95;

    @fragment
    fn main_fragment( @builtin(position) Position : vec4<f32>, 
                @location(0) world_position : vec3<f32>,
                @location(1) normal : vec3<f32>
              ) -> @location(0) vec4<f32> {
      let n : vec3<f32> = normalize(normal);
      let v : vec3<f32> = normalize(world_position - ${camera}.${position});
      let l : vec3<f32> = normalize(vec3<f32>(0.5, 1.0, -.5));

      let cos_slope = dot(n, ${up});
      
      let blend_factor = clamp((cos_slope - ${blend_start}) / (${blend_end} - ${blend_start}), 0.0, 1.0);
      var c : vec3<f32> = ${rock_color} * (1.0 - blend_factor) + ${grass_color} * blend_factor;

      let I_in : vec3<f32> = vec3<f32>(5.0);

      let I : vec3<f32> = ${PBR.names.BRDF}(n, v, l, c, 0.9, 0.0) * I_in;
      return vec4<f32>(${PBR.names.gamma_compresion}(I), 1.0);
    }
  `;
}