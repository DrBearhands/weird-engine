// @ts-check

import Material from "../Material.js";
import MaterialInstance from "../MaterialInstance.js";
import * as PBR from "../PBR.wgsl.js";

import Camera from "../Camera.js";
import { device } from "../../WebGPUInit.js";

const names = { ...Camera.srcVariableNames }

export default class DefaultMaterial extends Material {
  /** @readonly */
  bindGroupLayout;
  
  constructor() {
    const bindGroupLayout = device.createBindGroupLayout({
      label: "Default Material - bindGroupLayout",
      entries: []
    });
    super(bindGroupLayout, src, "Default");
    this.bindGroupLayout = bindGroupLayout;
  }

  /**
   * @returns {MaterialInstance}
   */
  createInstance() {
    const bindGroup = device.createBindGroup({
      layout: this.bindGroupLayout,
      entries: []
    })
    return new MaterialInstance(this, bindGroup);
  }
}

const src = () => {

  const camera = names.global.camera;
  const position = names.camera.position;
  
  return /* wgsl */ `
    ${PBR.src}

    @fragment
    fn main_fragment( @builtin(position) Position : vec4<f32>, 
                      @location(1) normal : vec3<f32>
                    ) -> @location(0) vec4<f32> {
      let n : vec3<f32> = normalize(normal);
      let v : vec3<f32> = normalize(Position.xyz - ${camera}.${position});
      let l : vec3<f32> = normalize(vec3<f32>(0.5, 1.0, -.5));

      var c : vec3<f32> = vec3<f32>(1.0, 1.0, 1.0);

      let I_in : vec3<f32> = vec3<f32>(5.0);

      let I : vec3<f32> = ${PBR.names.BRDF}(n, v, l, c, 0.9, 0.0) * I_in;
      return vec4<f32>(${PBR.names.gamma_compresion}(I), 1.0);
    }
  `
}