// @ts-check

import { device, preferredCanvasFormat } from "../../WebGPUInit.js";
import { environment_map_src } from "./EnvironmentMap.wgsl.js";
import RenderContext from "../../RenderContext.js";
import CubeMap from "../CubeMap.js";
import SceneGlobals from "../SceneGlobals.js";
import { textureFromData } from "../../WebGPUTextures.js";
import { TextureData } from "../../../../Data/Geometric/TextureData.js";


/** @readonly */
const environmentBindGroupLayout = device.createBindGroupLayout({
  label: "Environment Map",
  entries: [
    {
      binding: 0,
      visibility: GPUShaderStage.FRAGMENT,
      texture: {
        sampleType: "float",
        viewDimension: "cube"
      }
    },
    {
      binding: 1,
      visibility: GPUShaderStage.FRAGMENT,
      sampler: {
        type: "filtering"
      }
    }
  ]
});

/** @readonly */
const sampler = device.createSampler({
  magFilter: "linear",
  minFilter: "linear",
  mipmapFilter: "linear",
  addressModeU : "repeat",
  addressModeV: "repeat"
});

export default class EnvironmentMap {
  /**
   * 
   * @param {GPUTexture} texture 
   */
  static create(texture) {
    const textureView = texture.createView({
      dimension: "cube",
    });
    const bindGroup = device.createBindGroup({
      label: `Environment Map - ${textureView.label}`,
      layout: environmentBindGroupLayout,
      entries: [
        {
          binding: 0,
          resource: textureView
        },
        {
          binding: 1,
          resource: sampler
        }
      ]
    });

    return new CubeMap(textureView, bindGroup);
  }

  static createEmpty() {
    const environmentTexture = device.createTexture({
      dimension: "2d",
      format : EnvironmentMap.textureFormat,
      size: [2, 2, 6],
      usage: GPUTextureUsage.TEXTURE_BINDING | GPUTextureUsage.COPY_DST,
    });

    return EnvironmentMap.create(environmentTexture);
  }

  /**
   * @param {TextureData} textureData 
   * @returns {CubeMap}
   */
  static fromEquirectangularTextureData(textureData) {
    const hdrTextureEquirectangular = textureFromData(textureData, true);
    const cubemapSize = textureData.xRes/4;
    const cubemapDimensions = {width: cubemapSize, height: cubemapSize};
    const cubeTexture = CubeMap.fromEquirectangular(hdrTextureEquirectangular, cubemapDimensions);
    return EnvironmentMap.create(cubeTexture);
  }

  /** @readonly */
  static textureFormat = "rgba16float";

  /** @readonly */
  static pipeline = device.createRenderPipeline({
    layout: device.createPipelineLayout({
      label: "Cube Map pipeline",
      bindGroupLayouts : [SceneGlobals.bindGroupLayout, environmentBindGroupLayout],
    }),
    vertex: {
      module: device.createShaderModule({
        code: environment_map_src,
      }),
      entryPoint: 'main_vertex',
      buffers: []
    },
    fragment: {
      module: device.createShaderModule({
        code: environment_map_src,
      }),
      entryPoint: 'main_fragment',
      targets: [
        {
          format: preferredCanvasFormat,
        },
      ],
    },
    primitive: {
      cullMode: 'back',
      topology: 'triangle-list',
    },
    depthStencil: {
      depthCompare: "less-equal",
      depthWriteEnabled: false,
      format: RenderContext.depthTextureFormat
    }
  });
}