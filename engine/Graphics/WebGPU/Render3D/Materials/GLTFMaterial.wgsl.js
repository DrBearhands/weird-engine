//@ts-check

import { nameGenerator } from "../../../../Algorithms/UniqueVarNameGenerator.js";
import Camera from "../Camera.js";
import * as PBR from "../PBR.wgsl.js";

const names = {
  ...Camera.srcVariableNames,
  baseColorTexture: nameGenerator.generate("baseColorTexture"),
  baseColorSampler: nameGenerator.generate("baseColorSampler"),
  metallicRoughnessTexture: nameGenerator.generate("metallicRoughnessTexture"),
  metallicRoughnessSampler: nameGenerator.generate("metallicRoughnessSampler"),
  normalTexture: nameGenerator.generate("normalTexture"),
  normalSampler: nameGenerator.generate("normalSampler"),
  occlusionTexture: nameGenerator.generate("occlusionTexture"),
  occlusionSampler: nameGenerator.generate("occlusionSampler"),
  emissiveTexture: nameGenerator.generate("emissiveTexture"),
  emissiveSampler: nameGenerator.generate("emissiveSampler"),
  GLTF_Material_Scalars : nameGenerator.generate("GLTF_Material_Scalars"),
  gltfMaterialScalars: nameGenerator.generate("gltfMaterialScalars")
}

/**
 * 
 * @param {{
 *   group_material : number,
 *   binding_baseColorTexture : number,
 *   binding_baseColorSampler : number,
 *   binding_metallicRoughnessTexture : number,
 *   binding_metallicRoughnessSampler : number,
 *   binding_normalTexture : number,
 *   binding_normalSampler : number,
 *   binding_occlusionTexture : number,
 *   binding_occlusionSampler : number,
 *   binding_emissiveTexture : number,
 *   binding_emissiveSampler : number,
 *   binding_gltfMaterialScalars : number
 * }} bindings 
 * @returns 
 */
export function src(bindings) {
  const baseColorTexture = names.baseColorTexture;
  const baseColorSampler = names.baseColorSampler;
  const metallicRoughnessTexture = names.metallicRoughnessTexture;
  const metallicRoughnessSampler = names.metallicRoughnessSampler;
  const normalTexture = names.normalTexture;
  const normalSampler = names.normalSampler;
  const occlusionTexture = names.occlusionTexture;
  const occlusionSampler = names.occlusionSampler;
  const emissiveTexture = names.emissiveTexture;
  const emissiveSampler = names.emissiveSampler;
  const GLTF_Material_Scalars = names.GLTF_Material_Scalars;
  const gltfMaterialScalars = names.gltfMaterialScalars;

  const group_material = bindings.group_material;
  const binding_baseColorTexture = bindings.binding_baseColorTexture;
  const binding_baseColorSampler = bindings.binding_baseColorSampler;
  const binding_metallicRoughnessTexture = bindings.binding_metallicRoughnessTexture;
  const binding_metallicRoughnessSampler = bindings.binding_metallicRoughnessSampler;
  const binding_normalTexture = bindings.binding_normalTexture;
  const binding_normalSampler = bindings.binding_normalSampler;
  const binding_occlusionTexture = bindings.binding_occlusionTexture;
  const binding_occlusionSampler = bindings.binding_occlusionSampler;
  const binding_emissiveTexture = bindings.binding_emissiveTexture;
  const binding_emissiveSampler = bindings.binding_emissiveSampler;
  const binding_gltfMaterialScalars = bindings.binding_gltfMaterialScalars;

  const camera = names.global.camera;
  const position = names.camera.position;
  
  return /* wgsl */ `

    ${PBR.src}

    struct ${GLTF_Material_Scalars} {
      baseColorFactor: vec4<f32>,
      metallicFactor: f32,
      roughnessFactor: f32,
      normalScale: f32,
      occlusionStrength: f32,
      emissiveFactor: vec3<f32>,
      alphaCutoff: f32
    };

    @group(${group_material})
    @binding(${binding_gltfMaterialScalars}) 
    var<uniform> ${gltfMaterialScalars} : ${GLTF_Material_Scalars};

    @group(${group_material})
    @binding(${binding_baseColorTexture})
    var ${baseColorTexture}: texture_2d<f32>;
    @group(${group_material})
    @binding(${binding_baseColorSampler})
    var ${baseColorSampler}: sampler;

    @group(${group_material})
    @binding(${binding_metallicRoughnessTexture})
    var ${metallicRoughnessTexture} : texture_2d<f32>;
    @group(${group_material})
    @binding(${binding_metallicRoughnessSampler})
    var ${metallicRoughnessSampler}: sampler;

    @group(${group_material})
    @binding(${binding_normalTexture})
    var ${normalTexture} : texture_2d<f32>;
    @group(${group_material})
    @binding(${binding_normalSampler})
    var ${normalSampler} : sampler;

    @group(${group_material})
    @binding(${binding_occlusionTexture})
    var ${occlusionTexture} : texture_2d<f32>;
    @group(${group_material})
    @binding(${binding_occlusionSampler})
    var ${occlusionSampler} : sampler;

    @group(${group_material})
    @binding(${binding_emissiveTexture})
    var ${emissiveTexture} : texture_2d<f32>;
    @group(${group_material})
    @binding(${binding_emissiveSampler})
    var ${emissiveSampler} : sampler;

    @fragment
    fn main_fragment(
      @builtin(position) Position : vec4<f32>, 
      @location(0) world_position : vec3<f32>,
      @location(1) normal : vec3<f32>,
      @location(2) tangent : vec4<f32>,
      @location(3) color : vec4<f32>,
      @location(4) tex_coord : vec2<f32>
    ) -> @location(0) vec4<f32> {
      var n_sample = (textureSample(${normalTexture}, ${normalSampler}, tex_coord).rgb - 0.5) * 2.0;
      let normal1 = normalize(normal);
      let tangent1 = normalize(tangent.xyz);
      let bitangent1 = cross(normal1, tangent1) * tangent.w;
      let n = normalize(n_sample.x*tangent1 + n_sample.y*bitangent1 + n_sample.z*normal1);
      let c_pos = ${camera}.${position};
      let v : vec3<f32> = normalize(c_pos - world_position);
      let l : vec3<f32> = normalize(vec3<f32>(0.5, 1.0, -.5));

      let c_alpha = textureSample(${baseColorTexture}, ${baseColorSampler}, tex_coord) * ${gltfMaterialScalars}.baseColorFactor;
      let c : vec3<f32> = c_alpha.rgb * color.rgb;

      let mrRGBA = textureSample(${metallicRoughnessTexture}, ${metallicRoughnessSampler}, tex_coord);
      let metallic = mrRGBA.b * ${gltfMaterialScalars}.metallicFactor;
      let roughness = mrRGBA.g * ${gltfMaterialScalars}.roughnessFactor;


      let I_in : vec3<f32> = vec3<f32>(5.0);

      var I : vec3<f32> = vec3<f32>(0.0);
      //I += max(${PBR.names.BRDF}(n, v, l, c, roughness, metallic), vec3<f32>(0.0)) * I_in;

      let I_IBL = PBR_IBL(
        irradiance_texture, 
        irradiance_sampler, 
        prefiltered_environment_texture, 
        prefiltered_environment_sampler, 
        BRDF_integration_texture,
        BRDF_integration_sampler,
        n, v, c, metallic, roughness, 1.0);

      I += I_IBL;
      return vec4<f32>(${PBR.names.gamma_compresion}(I), 1.0);
    }
  `;
}