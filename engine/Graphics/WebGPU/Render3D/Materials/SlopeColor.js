// @ts-check
import { SizeOf, vec, f32 } from "../../wgsl.js";
import Material from "../Material.js";
import MaterialInstance from "../MaterialInstance.js";
import { device } from "../../WebGPUInit.js";
import { src } from "./SlopeColor.wgsl.js";


const binding_grass_color = 0;
const binding_rock_color = 1;

export class SlopeColorMaterial extends Material {
  /**
   * @readonly @type {GPUBindGroupLayout}
   */
  materialBindGroupLayout;


  constructor() {
    const materialBindGroupLayout = device.createBindGroupLayout({
      label: "Heightmap Material - bindGroupLayout",
      entries: [
        {
          binding: binding_grass_color,
          visibility: GPUShaderStage.FRAGMENT,
          buffer: {
            type: "uniform",
            minBindingSize: grass_color_bytesize
          }
        },
        {
          binding: binding_rock_color,
          visibility: GPUShaderStage.FRAGMENT,
          buffer: {
            type: "uniform",
            minBindingSize: rock_color_bytesize
          }
        }
      ]
    });
    super(materialBindGroupLayout, (material_group) => src({material_group, binding_grass_color, binding_rock_color}), "Slope Color Material");
    this.materialBindGroupLayout = materialBindGroupLayout;
  }

  /**
   * @returns {SlopeColorMaterialInstance}
   */
   createInstance() {
    const grass_color_buffer = device.createBuffer({
      usage: GPUBufferUsage.UNIFORM | GPUBufferUsage.COPY_DST,
      size: grass_color_bytesize
    });

    const rock_color_buffer = device.createBuffer({
      usage: GPUBufferUsage.UNIFORM | GPUBufferUsage.COPY_DST,
      size: rock_color_bytesize
    });

    const bindGroup = device.createBindGroup({
      label: "Slope Color Material bind group",
      layout: this.materialBindGroupLayout,
      entries: [
        {
          resource: {
            buffer: grass_color_buffer
          },
          binding: binding_grass_color
        },
        {
          resource: {
            buffer: rock_color_buffer
          },
          binding: binding_rock_color
        }
      ],
    })
    const instance = new SlopeColorMaterialInstance(this, bindGroup, grass_color_buffer, rock_color_buffer);
    return instance;
  }
}


export class SlopeColorMaterialInstance extends MaterialInstance {
  /** @readonly @type {GPUDevice} */
  device;
  /** @readonly @type {GPUBuffer} */
  #rockColorBuffer;
  /** @readonly @type {GPUBuffer} */
  #grassColorBuffer;

  /**
   * @param {SlopeColorMaterial} material
   * @param {GPUBindGroup} bindGroup 
   * @param {GPUBuffer} grassColorBuffer 
   * @param {GPUBuffer} rockColorBuffer 
   */
  constructor(material, bindGroup, grassColorBuffer,rockColorBuffer) {
    super(material, bindGroup)
    this.device = device;
    this.#rockColorBuffer = rockColorBuffer;
    this.#grassColorBuffer = grassColorBuffer;
  }

  /**
   * @param {Float32Array} a 
   */
  setGrassColor(a) {
    const device = this.device;
    device.queue.writeBuffer(this.#grassColorBuffer, 0, a)
  }

  /**
   * @param {Float32Array} a 
   */
  setRockColor(a) {
    const device = this.device;
    device.queue.writeBuffer(this.#rockColorBuffer, 0, a)
  }
}
/**
 * @typedef {{
 *   material_group : number,
 *   grass_color_binding : number,
 *   rock_color_binding : number
 * }} Bindings
 */

const grass_color_bytesize = SizeOf(vec(3, f32));
const rock_color_bytesize = SizeOf(vec(3, f32));