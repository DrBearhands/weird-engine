// @ts-check
import Camera from "../../../Data/Geometric/Camera.js";
import { F32_BYTES } from "../../../Data/byte-sizes.js"
import { group_global, group_material, group_object, normals_group_global, normals_group_object } from "./config.js";
import Material from "./Material.js";
import MaterialInstance from "./MaterialInstance.js";
import { EachMeshType } from "./Meshes/AllMeshes.js";
import StaticMesh from "./Meshes/StaticMesh.js";
import StaticMeshInstance from "./Meshes/StaticMeshInstance.js";
import HeightmapMesh from "./Meshes/HeightmapMesh.js";
import HeightmapMeshInstance from "./Meshes/HeightmapMeshInstance.js";
import Transform from "./Transform.js";
import DefaultMap from "../../../Data/Datastructures/DefaultMap.js";
import SceneGlobals from "./SceneGlobals.js";

import { device } from "../WebGPUInit.js";
import EnvironmentMap from "./Materials/EnvironmentMap.js";
import RenderContext from "../RenderContext.js";
import NormalsDebugger from "./Debug/Normals.js";
import SceneNode from "../../../Data/Geometric/SceneGraph.js";
  
const cameraArrayBuffer = new ArrayBuffer(F32_BYTES*(16+4));
const C = new Float32Array(cameraArrayBuffer, 0, 16);
const c = new Float32Array(cameraArrayBuffer, F32_BYTES*16, 4);

class RenderingScheduler {
  /** 
   * @typedef { DefaultMap <
   *   MaterialInstance, EachMeshType<
   *     DefaultMap<StaticMesh, StaticMeshInstance[]>,
   *     DefaultMap<HeightmapMesh, HeightmapMeshInstance[]>
   *   >
   * >} MaterialInstanceAssocMap
   */
  /**
   * @readonly @type { DefaultMap<
   *   Material, MaterialInstanceAssocMap
   * >}
   */
  ordered = new DefaultMap(() => new DefaultMap(() => { return {static : new DefaultMap(() => []), heightmap: new DefaultMap(() => [])}}));

  /**
   * @param {StaticMeshInstance} meshInstance 
   */
  scheduleStatic(meshInstance) {
    const mesh = meshInstance.staticMesh; 
    const materialInstance = meshInstance.materialInstance;
    if (!materialInstance) return;
    const material = materialInstance.material;
    this.ordered.get(material).get(materialInstance).static.get(mesh).push(meshInstance);
  }

  /**
   * @param {HeightmapMeshInstance} meshInstance 
   */
  scheduleHeightmap(meshInstance) {
    const mesh = meshInstance.heightmapMesh; 
    const materialInstance = meshInstance.materialInstance;
    if (!materialInstance) return;
    const material = materialInstance.material;
    this.ordered.get(material).get(materialInstance).heightmap.get(mesh)
  }
}

export default class Renderer {

  /**
   * renders to canvas
   * @param {RenderContext} context
   * @param {Camera} camera
   * @param {SceneGlobals} sceneGlobals
   * @param {SceneNode<StaticMeshInstance | HeightmapMeshInstance>} sceneRoot
   */
  render(context, camera, sceneGlobals, sceneRoot, drawNormals = false) {
    sceneRoot.updateGlobalTransforms((instance, globalTransform) => {
      if (instance instanceof StaticMeshInstance)
        Transform.updateGPUBuffers(instance, globalTransform);
    });

    context.resizeToCanvas();
    camera.setAspectRatio(context.canvas.width / context.canvas.height);
    
    camera.getCameraMatrix(C);
    camera.getPosition(c);
    device.queue.writeBuffer(
      sceneGlobals.cameraBuffer,
      0,
      cameraArrayBuffer,
    );

    const scheduler = new RenderingScheduler();

    sceneRoot.traverse((meshInstance) => {
      if (meshInstance instanceof StaticMeshInstance)
        scheduler.scheduleStatic(meshInstance);
      else if (meshInstance instanceof HeightmapMeshInstance)
        scheduler.scheduleHeightmap(meshInstance);
    });

    device.pushErrorScope("validation");

    const commandEncoder = device.createCommandEncoder();
    const clearColor = { r: 0.3, g: 0.7, b: 1.0, a: 1.0 };
    
    const passEncoder = commandEncoder.beginRenderPass({
      colorAttachments: [
        {
          clearValue: clearColor,
          loadOp: "clear",
          storeOp: "store",
          view: context.colorView,
        },
      ],
      depthStencilAttachment : {
        view: context.depthView,
        depthClearValue: 1.0,
        depthLoadOp: 'clear',
        depthStoreOp: 'store',
      }
    });

    passEncoder.setBindGroup(group_global, sceneGlobals.bindGroup);

    for (const [material, instances] of scheduler.ordered) {
      passEncoder.setPipeline(material.pipelines.heightmap);
      for (const [instance, meshes] of instances) {
        passEncoder.setBindGroup(group_material, instance.bindGroup);
        for (const [ mesh, {}] of meshes.heightmap) {
          passEncoder.setBindGroup(group_object, mesh.renderBindGroup);
          passEncoder.setVertexBuffer(0, mesh.vertexBuffer);
          passEncoder.setIndexBuffer(mesh.indexBuffer, "uint32");
          passEncoder.drawIndexed(mesh.drawCount);
        }
      }
      
      passEncoder.setPipeline(material.pipelines.static);
      for (const [ instance, meshes ] of instances) {
        passEncoder.setBindGroup(group_material, instance.bindGroup);
        for (const [ mesh, transforms ] of meshes.static) {
          passEncoder.setVertexBuffer(0, mesh.vertexBuffer);
          passEncoder.setIndexBuffer(mesh.indexBuffer, "uint32");
          for (const transform of transforms) {
            passEncoder.setBindGroup(group_object, transform.bindGroup);
            passEncoder.drawIndexed(mesh.drawCount);
          }
        }
      }

    }

    passEncoder.setPipeline(EnvironmentMap.pipeline);
    const environmentMap = sceneGlobals.environmentMap;
    if (environmentMap) {
      passEncoder.setBindGroup(1, environmentMap.bindGroup);
      passEncoder.draw(36);
    }
    
    if (drawNormals) {
      passEncoder.setBindGroup(normals_group_global, sceneGlobals.bindGroup);
      passEncoder.setPipeline(NormalsDebugger.heightmapMeshPipeline);
      for (const [ _ , instances ] of scheduler.ordered) {
        for (const [ _, meshes ] of instances) {
          for (const [mesh, {}] of meshes.heightmap) {
            passEncoder.setBindGroup(normals_group_object, mesh.renderBindGroup);
            passEncoder.setVertexBuffer(0, mesh.vertexBuffer);
            passEncoder.draw(6, mesh.vertexCount);
          }
        }
      }

      passEncoder.setPipeline(NormalsDebugger.staticMeshPipeline);
      for (const [ _, instances ] of scheduler.ordered) {
        for (const [ _, meshes ] of instances) {
          for (const [ mesh, transforms ] of meshes.static) {
            passEncoder.setVertexBuffer(0, mesh.vertexBuffer);
            for (const transform of transforms) {
              passEncoder.setBindGroup(normals_group_object, transform.bindGroup);
              passEncoder.draw(6, mesh.vertexCount);
            }
          }
        }
      }
    }

    passEncoder.end();
    const finished_encoder = commandEncoder.finish();
    device.queue.submit([finished_encoder]);

    device.popErrorScope().then(err => {
      if (err) {
        console.error("Error when rendering");
        console.error(err);
      }
    });
  }
}