// @ts-check

import { nameGenerator } from "../../../Algorithms/UniqueVarNameGenerator.js";
import { f32, mat, SizeOf, struct, vec } from "../wgsl.js";

export default class Camera {
  /** @readonly */
  static type = struct(mat(4,4,f32), vec(3, f32))
  /** @readonly */
  static byteSize = SizeOf(Camera.type);

  static srcVariableNames = {
    /** @readonly */
    global : {
      /** @readonly */
      Camera : nameGenerator.generate("Camera"),
      /** @readonly */
      camera : nameGenerator.generate("camera")
    },
    /** @readonly */
    camera : {
      /** @readonly */
      MVP : "MVP",
      /** @readonly */
      position : "position"
    }
  }

  static createShaderSource(group, binding) {
    return /* wgsl */ `
      struct ${Camera.srcVariableNames.global.Camera} {
        ${Camera.srcVariableNames.camera.MVP} : mat4x4<f32>,
        ${Camera.srcVariableNames.camera.position} : vec3<f32>
      }
    
      @group(${group}) @binding(${binding}) 
      var<uniform> ${Camera.srcVariableNames.global.camera} : ${Camera.srcVariableNames.global.Camera};
    `
  }
}