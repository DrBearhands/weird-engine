// @ts-check

import Camera from "./Camera.js";
/**
 * @param {{
 *   group_global : number,
 *   binding_camera : number
 * }} bindings
 * @returns {string}
 */
export const globals_src = (bindings) => /* wgsl */ `

${Camera.createShaderSource(bindings.group_global, bindings.binding_camera)}

@group(${bindings.group_global}) @binding(1)
var irradiance_texture : texture_cube<f32>;
@group(${bindings.group_global}) @binding(2)
var irradiance_sampler : sampler;

@group(${bindings.group_global}) @binding(3)
var prefiltered_environment_texture : texture_cube<f32>;
@group(${bindings.group_global}) @binding(4)
var prefiltered_environment_sampler : sampler;

@group(${bindings.group_global}) @binding(5)
var BRDF_integration_texture : texture_2d<f32>;
@group(${bindings.group_global}) @binding(6)
var BRDF_integration_sampler : sampler;
`