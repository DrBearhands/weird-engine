// @ts-check

const apply_exponent_src = /* wgsl */ `
  fn apply_exponent(c : vec4<f32>) -> vec3<f32> {
    return c.rgb * pow(2.0, (c.a-0.5) * 255);
  }
`

const equirectangular_uv_src = /* wgsl */ `
  fn equirectangular_uv(v : vec3<f32>) -> vec2<f32> {
    let dir = normalize(v);
    var uv = vec2<f32>(atan2(dir.z, dir.x), asin(dir.y));
    const invAtan = vec2<f32>(0.1591, -0.3183);
    uv *= invAtan;
    uv += 0.5;
    return uv;
  }
`

export const write_cubemap_vertex_src = /* wgsl */ `
  const v00 = vec2<f32>(-1, -1);
  const v01 = vec2<f32>(-1,  1);
  const v10 = vec2<f32>( 1, -1);
  const v11 = vec2<f32>( 1,  1);

  const positions = array<vec2<f32>, 4>(
    v00, v01, v10, v11
  );

  struct VertexOutput {
    @builtin(position) position : vec4<f32>,
    @location(0) cube_position : vec3<f32>
  }

  @group(1)
  @binding(0)
  var<uniform> R : mat3x3<f32>;

  @vertex
  fn main_vertex(@builtin(vertex_index) vertex_index : u32) -> VertexOutput {
    let clip_pos = vec4<f32>(positions[vertex_index], 1.0, 1.0);
    var out : VertexOutput;
    out.position = clip_pos;
    out.cube_position = R * clip_pos.xyz;
    return out;
  }
`

export const equirectangular_to_cube_src = /* wgsl */ `
  @group(0)
  @binding(0)
  var equirectangular_texture : texture_2d<f32>;
  
  @group(0)
  @binding(1)
  var equirectangular_sampler : sampler;

  ${equirectangular_uv_src}

  @fragment
  fn main_fragment(
    @location(0) cube_position : vec3<f32>,
  ) -> @location(0) vec4<f32> {

    let uv = equirectangular_uv(cube_position);

    let sample = textureSample(equirectangular_texture, equirectangular_sampler, uv);

    return vec4<f32>(sample.rgb, 1.0);
  }
`