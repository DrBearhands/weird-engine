// @ts-check
import "./compatibility-layer.js";

export class WebGPUUnsupporterError extends Error {
  html = `<p>Oh-oh, it appears your browser does not support WebGPU yet. Check browser compatibility on <a href="https://developer.mozilla.org/en-US/docs/Web/API/GPU#browser_compatibility">MDN's browser compatibility table</a>.</p>`;
  constructor() {
    super("WebGPU not supported on this system");
  }
}

if (!navigator.gpu) {
  throw new WebGPUUnsupporterError();
}

async function getAdapter() {
  const adapter = await navigator.gpu.requestAdapter();
  if (!adapter) {
    throw Error("Failed to get WebGPU adapter");
  }
  return adapter;
}

export const adapter = await getAdapter();
export const device = await adapter.requestDevice();

device.lost.then(deviceLostInfo => {
  console.error(deviceLostInfo.reason, deviceLostInfo.message);
  debugger;
});

export const preferredCanvasFormat = navigator.gpu.getPreferredCanvasFormat();