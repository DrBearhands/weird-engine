Not a data format. More hardcoded arguments for specific sequence of WebGL functions.

bufferViews have stride, which depends highly on how they are accessed.
Several datatypes have all kind of optional fields that affect other optionality requirements (camera, image, ...)
Default values sometimes not given, must look into WebGL spec.

Packed as if intended to feed directly to GL command, but implicit parameters require rebuilding geometry

Negative scaling...