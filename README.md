# Weird Engine

This is my personal project to (eventually) experiment with unusual graphics engine features. In particular the dynamic creation of assets rather than pre-creating and baking everything as is common on commercial graphics engines.

While the software source is provided, it is NOT currently licensed, so you are not legally allowed to use or copy it.