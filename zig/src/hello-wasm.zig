const webgpu = @import("./webgpu.zig");

export fn run(deviceId: usize, queueId: usize) void {
  const device = webgpu.GPUDevice{.id = deviceId};
  const queue = webgpu.GPUQueue{.id = queueId};
  const encoder = device.createCommandEncoder();
  const renderPass = encoder.beginRenderPass(1.0, 0.0, 1.0, 1.0);
  renderPass.end();
  const commandBuffer = encoder.finish();
  queue.submit(commandBuffer);
}
