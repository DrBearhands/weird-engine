
const string = *const u8;

pub const GPU = struct {
  id : usize,
  //wgslLanguageFeatures : WGSLLanguageFeatures,
  pub fn getPreferredCanvasFormat(self : @This()) GPUCanvasFormat { 
    return @enumFromInt(GPU_getPreferredCanvasFormat(self.id));
  }
  //pub fn requestAdapter() Promise(GPUAdapter){ }
};

const GPUCanvasFormat = enum(u32) {
  bgra8unorm = 0,
  rgba8unorm = 1,
  rgba16float = 2
};

pub const GPUAdapter = struct {
  id : usize,
  features: GPUSupportedFeatures,
  info: GPUAdapterInfo,
  isFallbackAdapter: bool,
  limits: GPUSupportedLimits,
//  pub fn requestDevice() void {}
};

pub const GPUAdapterInfo = struct {
  architexture: string,
  description: string,
  device: string,
  vendor: string
};

pub const GPUBindGroup = struct {
  id : usize,
//  label: string
};

pub const GPUBindGroupLayout = struct {
  id : usize,
  //label : string
};

pub const GPUBuffer = struct {
  id : usize,
  //label: string,
  //mapState: GPUBuffer_mapState,
  //size: usize,
  //usage: u32,

  pub fn destroy(buffer : GPUBuffer) void {
    GPUBuffer_destroy(buffer.id);
  }

  //pub fn getMappedRange(buffer: GPUBuffer, offset : ?usize, size: ?usize) void {
  //  const arrayBufferId = GPUBuffer_getMappedRange(buffer.id, offset, size);
  //}
};

const GPUBufferUsage = enum(u32) {
  COPY_SRC = 0x0004,
  COPY_DST = 0x0008,
  INDEX = 0x0010,
  INDIRECT = 0x0100,
  MAP_READ = 0x0001,
  MAP_WRITE = 0x0002,
  QUERY_RESOLVE = 0x0200,
  STORAGE = 0x0080,
  UNIFORM = 0x0040,
  VERTEX = 0x0020
};

const GPUBuffer_mapState = enum {
  unmapped,
  pending,
  mapped
};

pub const GPUSupportedFeatures = struct {

};

pub const GPUSupportedLimits = struct {

};


extern fn GPU_getPreferredCanvasFormat(gpuId: usize) usize;
//async extern fn GPU_requestAdapter(gpu: usize) usize;

//async extern fn GPUAdapter_requestDevice(adapter : usize) usize;

extern fn GPUBuffer_destroy(bufferId: usize) void;
extern fn GPUBuffer_getMappedRange(bufferId: usize, offset : ?usize, size: ?usize) usize; //return ArrayBuffer
//async extern fn GPUBuffer_mapAsync(bufferId: usize, mode : u32, offset : ?usize, size : ?usize) void;
extern fn GPUBuffer_unmap(bufferId: usize) void;


//extern fn GPUCanvasContext_configure(configuration) void;
//extern fn GPUCanvasContext_getConfiguration() usize;
extern fn GPUCanvasContext_getCurrentTexture(canvasContextId : usize) usize;
extern fn GPUCanvasContext_unconfigure(canvasContextId : usize) void;

//extern fn GPUCommandEncoder_beginComputePass(encoderId: usize, descriptorPtr: ?usize) usize;
//extern fn GPUCommandEncoder_beginRenderPass(encoderId: usize, descriptorPtr: usize) usize;
extern fn GPUCommandEncoder_clearBuffer(encoderId: usize, bufferId: usize, offset: ?usize, size : ?usize) void;
extern fn GPUCommandEncoder_copyBufferToBuffer(encoderId: usize, sourceId: usize, sourceOffset : usize, destinationId : usize, destinationOffset : usize, size : usize) void;
//extern fn GPUCommandEncoder_copyBufferToTexture(encoderId: usize, source, destination, width: usize, height: usize, depthOrArrayLayer: usize) void;
//extern fn GPUCommandEncoder_copyTextureToBuffer(encoderId: usize, source, destination, width: usize, height: usize, depthOrArrayLayer: usize) void;
//extern fn GPUCommandEncoder_copyTextureToTexture(encoderId: usize, source, destination, width: usize, height: usize, depthOrArrayLayer: usize) void;
//extern fn GPUCommandEncoder_finish(encoderId: usize, label) usize;
//extern fn GPUCommandEncoder_insertDebugMarker(encoderId: usize, markerLabel) void;
extern fn GPUCommandEncoder_popDebugGroup(encoderId: usize) void;
//extern fn GPUCommandEncoder_pushDebugGroup(encoderId: usize, groupLabel) void;
extern fn GPUCommandEncoder_resolveQuerySet(encoderId: usize, querySetId : usize, firstQuery: u32, queryCount: u32, destinationId: usize, destinationOffset : u32) void;


extern fn Canvas_getContext(canvas: usize) usize;


extern fn GPUDevice_createCommandEncoder(deviceId: usize) usize;


extern fn GPURenderPassDescriptor_end(renderPassdescriptorId: usize) void;

extern fn GPUQueue_submit(queueId: usize, commandBufferId: usize) void;
extern fn GPUQueue_writeBuffer(queueId: usize, bufferId: usize, bufferOffset: u64, dataPtr: usize, dataOffset: ?u64, size: ?u64) void;

pub const GPUDevice = struct {
  id : usize,
  pub fn createCommandEncoder(self: @This()) GPUCommandEncoder {
    return GPUCommandEncoder {
      .id = GPUDevice_createCommandEncoder(self.id)
    };
  }
};

pub const GPUCommandEncoder = struct {
  id : usize,

  pub fn beginRenderPass(self : @This(), r: f32, g: f32, b: f32, a: f32) GPURenderPassDescriptor {
    return GPURenderPassDescriptor{
      .id = GPUCommandEncoder_beginRenderPass(self.id, r, g, b, a)
    };
  }

  pub fn finish(commandEncoder: @This()) GPUCommandBuffer {
    return GPUCommandBuffer{
      .id = GPUCommandEncoder_finish(commandEncoder.id)
    };
  }
};

pub const GPURenderPassDescriptor = struct {
  id : usize,

  pub fn end(self : @This()) void {
    GPURenderPassDescriptor_end(self.id);
  }
};

pub const GPUCommandBuffer = struct {
  id : usize
};

pub const GPUQueue = struct {
  id: usize,
  pub fn submit(queue: @This(), commandBuffer: GPUCommandBuffer) void {
    GPUQueue_submit(queue.id, commandBuffer.id );
  }

  pub fn writeBuffer(queue: @This(), buffer : GPUBuffer, bufferOffset: u64, dataPtr: usize, dataOffset: ?u64, size: ?u64) void {
    GPUQueue_writeBuffer(queue.id, buffer.id, bufferOffset, dataPtr, dataOffset, size);
  }
};